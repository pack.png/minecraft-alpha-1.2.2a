// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

package net.minecraft.client;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.EffectRenderer;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerSP;
import net.minecraft.src.EntityRenderer;
import net.minecraft.src.EnumOS2;
import net.minecraft.src.EnumOSMappingHelper;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.GLAllocation;
import net.minecraft.src.GameSettings;
import net.minecraft.src.GameWindowListener;
import net.minecraft.src.GuiChat;
import net.minecraft.src.GuiConflictWarning;
import net.minecraft.src.GuiConnecting;
import net.minecraft.src.GuiGameOver;
import net.minecraft.src.GuiIngame;
import net.minecraft.src.GuiIngameMenu;
import net.minecraft.src.GuiInventory;
import net.minecraft.src.GuiMainMenu;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiUnused;
import net.minecraft.src.ItemRenderer;
import net.minecraft.src.ItemStack;
import net.minecraft.src.LoadingScreenRenderer;
import net.minecraft.src.MathHelper;
import net.minecraft.src.MinecraftError;
import net.minecraft.src.MinecraftException;
import net.minecraft.src.MinecraftImpl;
import net.minecraft.src.ModelBiped;
import net.minecraft.src.MouseHelper;
import net.minecraft.src.MovementInputFromOptions;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.OpenGlCapsChecker;
import net.minecraft.src.PlayerController;
import net.minecraft.src.PlayerControllerTest;
import net.minecraft.src.RenderEngine;
import net.minecraft.src.RenderGlobal;
import net.minecraft.src.RenderManager;
import net.minecraft.src.ScaledResolution;
import net.minecraft.src.ScreenShotHelper;
import net.minecraft.src.Session;
import net.minecraft.src.SoundManager;
import net.minecraft.src.Teleporter;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TextureCompassFX;
import net.minecraft.src.TextureFlamesFX;
import net.minecraft.src.TextureLavaFX;
import net.minecraft.src.TextureLavaFlowFX;
import net.minecraft.src.TexturePackList;
import net.minecraft.src.TexturePortalFX;
import net.minecraft.src.TextureWatchFX;
import net.minecraft.src.TextureWaterFX;
import net.minecraft.src.TexureWaterFlowFX;
import net.minecraft.src.ThreadDownloadResources;
import net.minecraft.src.ThreadSleepForever;
import net.minecraft.src.Timer;
import net.minecraft.src.UnexpectedThrowable;
import net.minecraft.src.Vec3D;
import net.minecraft.src.world.World;
import net.minecraft.src.WorldProvider;
import net.minecraft.src.WorldProviderHell;
import net.minecraft.src.WorldRenderer;
import java.awt.*;
import java.io.*;
import java.util.Random;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.*;
import org.lwjgl.opengl.*;
import org.lwjgl.util.glu.GLU;

// Referenced classes of package net.minecraft.client:
//            MinecraftApplet

public abstract class Minecraft
    implements Runnable
{

    public Minecraft(Component component, Canvas canvas, MinecraftApplet minecraftapplet, int i, int j, boolean flag)
    {
        mainFrame = false;
        timer = new Timer(20F);
        field_6320_i = null;
        field_6317_l = true;
        field_6316_m = false;
        guiScreen = null;
        loadingScreenRenderer = new LoadingScreenRenderer(this);
        entityRenderer = new EntityRenderer(this);
        field_6283_R = 0;
        field_6282_S = 0;
        field_6310_s = null;
        field_6309_t = 0;
        field_6307_v = false;
        field_6306_w = new ModelBiped(0.0F);
        field_6305_x = null;
        soundManager = new SoundManager();
        field_6277_X = new TextureWaterFX();
        field_6276_Y = new TextureLavaFX();
        running = true;
        field_6292_I = "";
        field_6291_J = false;
        field_6290_K = -1L;
        field_6289_L = false;
        field_6302_aa = 0;
        field_6288_M = false;
        field_6287_N = System.currentTimeMillis();
        field_6300_ab = 0;
        field_6281_T = i;
        field_6280_U = j;
        mainFrame = flag;
        field_6303_z = minecraftapplet;
        new ThreadSleepForever(this, "Timer hack thread");
        field_6318_k = canvas;
        field_6326_c = i;
        field_6325_d = j;
        mainFrame = flag;
    }

    public abstract void func_4007_a(UnexpectedThrowable unexpectedthrowable);

    public void func_6258_a(String s, int i)
    {
        field_6279_V = s;
        field_6278_W = i;
    }

    public void func_6271_a() throws LWJGLException
    {
        if(field_6318_k != null)
        {
            Graphics g = field_6318_k.getGraphics();
            if(g != null)
            {
                g.setColor(Color.BLACK);
                g.fillRect(0, 0, field_6326_c, field_6325_d);
                g.dispose();
            }
            Display.setParent(field_6318_k);
        } else
        if(mainFrame)
        {
            Display.setFullscreen(true);
            field_6326_c = Display.getDisplayMode().getWidth();
            field_6325_d = Display.getDisplayMode().getHeight();
            if(field_6326_c <= 0)
            {
                field_6326_c = 1;
            }
            if(field_6325_d <= 0)
            {
                field_6325_d = 1;
            }
        } else
        {
            Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(field_6326_c, field_6325_d));
        }
        Display.setTitle("Minecraft Minecraft Alpha v1.2.2");
        try
        {
            Display.create();
        }
        catch(LWJGLException lwjglexception)
        {
            lwjglexception.printStackTrace();
            try
            {
                Thread.sleep(1000L);
            }
            catch(InterruptedException interruptedexception) { }
            Display.create();
        }
        RenderManager.field_1233_a.field_4236_f = new ItemRenderer(this);
        field_6297_D = func_6240_b();
        gameSettings = new GameSettings(this, field_6297_D);
        texturePackList = new TexturePackList(this, field_6297_D);
        renderEngine = new RenderEngine(texturePackList, gameSettings);
        fontRenderer = new FontRenderer(gameSettings, "/font/default.png", renderEngine);
        func_6257_q();
        Keyboard.create();
        Mouse.create();
        mouseHelper = new MouseHelper(field_6318_k);
        try
        {
            Controllers.create();
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        func_6250_c("Pre startup");
        GL11.glEnable(3553);
        GL11.glShadeModel(7425);
        GL11.glClearDepth(1.0D);
        GL11.glEnable(2929);
        GL11.glDepthFunc(515);
        GL11.glEnable(3008);
        GL11.glAlphaFunc(516, 0.1F);
        GL11.glCullFace(1029);
        GL11.glMatrixMode(5889);
        GL11.glLoadIdentity();
        GL11.glMatrixMode(5888);
        func_6250_c("Startup");
        openGlCapsChecker = new OpenGlCapsChecker();
        soundManager.func_340_a(gameSettings);
        renderEngine.func_1066_a(field_6276_Y);
        renderEngine.func_1066_a(field_6277_X);
        renderEngine.func_1066_a(new TexturePortalFX());
        renderEngine.func_1066_a(new TextureCompassFX(this));
        renderEngine.func_1066_a(new TextureWatchFX(this));
        renderEngine.func_1066_a(new TexureWaterFlowFX());
        renderEngine.func_1066_a(new TextureLavaFlowFX());
        renderEngine.func_1066_a(new TextureFlamesFX(0));
        renderEngine.func_1066_a(new TextureFlamesFX(1));
        field_6323_f = new RenderGlobal(this, renderEngine);
        GL11.glViewport(0, 0, field_6326_c, field_6325_d);
        effectRenderer = new EffectRenderer(world, renderEngine);
        try
        {
            field_6284_Q = new ThreadDownloadResources(field_6297_D, this);
            field_6284_Q.start();
        }
        catch(Exception exception1) { }
        func_6250_c("Post startup");
        guiIngame = new GuiIngame(this);
        if(field_6279_V != null)
        {
            displayGuiScreen(new GuiConnecting(this, field_6279_V, field_6278_W));
        } else
        {
            displayGuiScreen(new GuiMainMenu());
        }
    }

    private void func_6257_q() throws LWJGLException
    {
        ScaledResolution scaledresolution = new ScaledResolution(field_6326_c, field_6325_d);
        int i = scaledresolution.getScaledWidth();
        int j = scaledresolution.getScaledHeight();
        GL11.glClear(16640);
        GL11.glMatrixMode(5889);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, i, j, 0.0D, 1000D, 3000D);
        GL11.glMatrixMode(5888);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000F);
        GL11.glViewport(0, 0, field_6326_c, field_6325_d);
        GL11.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
        Tessellator tessellator = Tessellator.instance;
        GL11.glDisable(2896);
        GL11.glEnable(3553);
        GL11.glDisable(2912);
        GL11.glBindTexture(3553, renderEngine.getTexture("/title/mojang.png"));
        tessellator.startDrawingQuads();
        tessellator.setColorOpaque_I(0xffffff);
        tessellator.addVertexWithUV(0.0D, field_6325_d, 0.0D, 0.0D, 0.0D);
        tessellator.addVertexWithUV(field_6326_c, field_6325_d, 0.0D, 0.0D, 0.0D);
        tessellator.addVertexWithUV(field_6326_c, 0.0D, 0.0D, 0.0D, 0.0D);
        tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
        tessellator.draw();
        char c = '\u0100';
        char c1 = '\u0100';
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        tessellator.setColorOpaque_I(0xffffff);
        func_6274_a((field_6326_c / 2 - c) / 2, (field_6325_d / 2 - c1) / 2, 0, 0, c, c1);
        GL11.glDisable(2896);
        GL11.glDisable(2912);
        GL11.glEnable(3008);
        GL11.glAlphaFunc(516, 0.1F);
        Display.swapBuffers();
    }

    public void func_6274_a(int i, int j, int k, int l, int i1, int j1)
    {
        float f = 0.00390625F;
        float f1 = 0.00390625F;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(i + 0, j + j1, 0.0D, (float)(k + 0) * f, (float)(l + j1) * f1);
        tessellator.addVertexWithUV(i + i1, j + j1, 0.0D, (float)(k + i1) * f, (float)(l + j1) * f1);
        tessellator.addVertexWithUV(i + i1, j + 0, 0.0D, (float)(k + i1) * f, (float)(l + 0) * f1);
        tessellator.addVertexWithUV(i + 0, j + 0, 0.0D, (float)(k + 0) * f, (float)(l + 0) * f1);
        tessellator.draw();
    }

    public static File func_6240_b()
    {
        if(field_6275_Z == null)
        {
            field_6275_Z = func_6264_a("minecraft");
        }
        return field_6275_Z;
    }

    public static File func_6264_a(String s)
    {
        String s1 = System.getProperty("user.home", ".");
        File file;
        switch(EnumOSMappingHelper.field_1585_a[func_6267_r().ordinal()])
        {
        case 1: // '\001'
        case 2: // '\002'
            file = new File(s1, (new StringBuilder()).append('.').append(s).append('/').toString());
            break;

        case 3: // '\003'
            String s2 = System.getenv("APPDATA");
            if(s2 != null)
            {
                file = new File(s2, (new StringBuilder()).append(".").append(s).append('/').toString());
            } else
            {
                file = new File(s1, (new StringBuilder()).append('.').append(s).append('/').toString());
            }
            break;

        case 4: // '\004'
            file = new File(s1, (new StringBuilder()).append("Library/Application Support/").append(s).toString());
            break;

        default:
            file = new File(s1, (new StringBuilder()).append(s).append('/').toString());
            break;
        }
        if(!file.exists() && !file.mkdirs())
        {
            throw new RuntimeException((new StringBuilder()).append("The working directory could not be created: ").append(file).toString());
        } else
        {
            return file;
        }
    }

    private static EnumOS2 func_6267_r()
    {
        String s = System.getProperty("os.name").toLowerCase();
        if(s.contains("win"))
        {
            return EnumOS2.windows;
        }
        if(s.contains("mac"))
        {
            return EnumOS2.macos;
        }
        if(s.contains("solaris"))
        {
            return EnumOS2.solaris;
        }
        if(s.contains("sunos"))
        {
            return EnumOS2.solaris;
        }
        if(s.contains("linux"))
        {
            return EnumOS2.linux;
        }
        if(s.contains("unix"))
        {
            return EnumOS2.linux;
        } else
        {
            return EnumOS2.unknown;
        }
    }

    public void displayGuiScreen(GuiScreen guiscreen)
    {
        if(guiScreen instanceof GuiUnused)
        {
            return;
        }
        if(guiScreen != null)
        {
            guiScreen.func_6449_h();
        }
        if(guiscreen == null && world == null)
        {
            guiscreen = new GuiMainMenu();
        } else
        if(guiscreen == null && entityPlayerSP.health_00 <= 0)
        {
            guiscreen = new GuiGameOver();
        }
        guiScreen = guiscreen;
        if(guiscreen != null)
        {
            func_6273_f();
            ScaledResolution scaledresolution = new ScaledResolution(field_6326_c, field_6325_d);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            guiscreen.func_6447_a(this, i, j);
            field_6307_v = false;
        } else
        {
            func_6259_e();
        }
    }

    private void func_6250_c(String s)
    {
        int i = GL11.glGetError();
        if(i != 0)
        {
            String s1 = GLU.gluErrorString(i);
            System.out.println("########## GL ERROR ##########");
            System.out.println((new StringBuilder()).append("@ ").append(s).toString());
            System.out.println((new StringBuilder()).append(i).append(": ").append(s1).toString());
            System.exit(0);
        }
    }

    public void func_6266_c()
    {
        if(field_6303_z != null)
        {
            field_6303_z.func_6231_c();
        }
        try
        {
            if(field_6284_Q != null)
            {
                field_6284_Q.closeMinecraft();
            }
        }
        catch(Exception exception) { }
        try
        {
            System.out.println("Stopping!");
            setWorld(null);
            try
            {
                GLAllocation.deleteTexturesAndDisplayLists();
            }
            catch(Exception exception1) { }
            soundManager.closeMinecraft_00();
            Mouse.destroy();
            Keyboard.destroy();
        }
        finally
        {
            Display.destroy();
        }
        System.gc();
    }

    public void run()
    {
        running = true;
        try
        {
            func_6271_a();
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            func_4007_a(new UnexpectedThrowable("Failed to start game", exception));
            return;
        }
        try
        {
            long l = System.currentTimeMillis();
            int i = 0;
            while(running && (field_6303_z == null || field_6303_z.isActive()))
            {
                AxisAlignedBB.clearBoundingBoxPool();
                Vec3D.initialize();
                if(field_6318_k == null && Display.isCloseRequested())
                {
                    func_6244_d();
                }
                if(field_6316_m && world != null)
                {
                    float f = timer.field_1378_c;
                    timer.func_904_a();
                    timer.field_1378_c = f;
                } else
                {
                    timer.func_904_a();
                }
                long l1 = System.nanoTime();
                for(int j = 0; j < timer.field_1379_b; j++)
                {
                    field_6283_R++;
                    try
                    {
                        func_6246_i();
                        continue;
                    }
                    catch(MinecraftException minecraftexception)
                    {
                        world = null;
                    }
                    setWorld(null);
                    displayGuiScreen(new GuiConflictWarning());
                }

                long l2 = System.nanoTime() - l1;
                func_6250_c("Pre render");
                soundManager.func_338_a(entityPlayerSP, timer.field_1378_c);
                GL11.glEnable(3553);
                if(world != null && !world.multiplayerWorld)
                {
                    while(world.func_6465_g()) ;
                }
                if(world != null && world.multiplayerWorld)
                {
                    world.func_6465_g();
                }
                if(gameSettings.limitFramerate)
                {
                    Thread.sleep(5L);
                }
                if(!Keyboard.isKeyDown(65))
                {
                    Display.update();
                }
                if(!field_6307_v)
                {
                    if(playerController != null)
                    {
                        playerController.func_6467_a(timer.field_1378_c);
                    }
                    entityRenderer.func_4136_b(timer.field_1378_c);
                }
                if(!Display.isActive())
                {
                    if(mainFrame)
                    {
                        func_6270_h();
                    }
                    Thread.sleep(10L);
                }
                if(Keyboard.isKeyDown(61))
                {
                    func_6238_a(l2);
                } else
                {
                    field_6290_K = System.nanoTime();
                }
                Thread.yield();
                if(Keyboard.isKeyDown(65))
                {
                    Display.update();
                }
                func_6248_s();
                if(field_6318_k != null && !mainFrame && (field_6318_k.getWidth() != field_6326_c || field_6318_k.getHeight() != field_6325_d))
                {
                    field_6326_c = field_6318_k.getWidth();
                    field_6325_d = field_6318_k.getHeight();
                    if(field_6326_c <= 0)
                    {
                        field_6326_c = 1;
                    }
                    if(field_6325_d <= 0)
                    {
                        field_6325_d = 1;
                    }
                    func_6249_a(field_6326_c, field_6325_d);
                }
                func_6250_c("Post render");
                i++;
                field_6316_m = !func_6260_j() && guiScreen != null && guiScreen.func_6450_b();
                while(System.currentTimeMillis() >= l + 1000L) 
                {
                    field_6292_I = (new StringBuilder()).append(i).append(" fps, ").append(WorldRenderer.field_1762_b).append(" chunk updates").toString();
                    WorldRenderer.field_1762_b = 0;
                    l += 1000L;
                    i = 0;
                }
            }
        }
        catch(MinecraftError minecrafterror) { }
        catch(Throwable throwable)
        {
            world = null;
            throwable.printStackTrace();
            func_4007_a(new UnexpectedThrowable("Unexpected error", throwable));
        }
        finally { }
    }

    private void func_6248_s()
    {
        if(Keyboard.isKeyDown(60))
        {
            if(!field_6291_J)
            {
                if(Keyboard.isKeyDown(59))
                {
                    guiIngame.func_552_a(ScreenShotHelper.func_4148_a(field_6275_Z, field_6326_c, field_6325_d));
                }
                field_6291_J = true;
            }
        } else
        {
            field_6291_J = false;
        }
    }

    private void func_6238_a(long l)
    {
        long l1 = 0xfe502aL;
        if(field_6290_K == -1L)
        {
            field_6290_K = System.nanoTime();
        }
        long l2 = System.nanoTime();
        field_6295_F[field_6294_G & field_6296_E.length - 1] = l;
        field_6296_E[field_6294_G++ & field_6296_E.length - 1] = l2 - field_6290_K;
        field_6290_K = l2;
        GL11.glClear(256);
        GL11.glMatrixMode(5889);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, field_6326_c, field_6325_d, 0.0D, 1000D, 3000D);
        GL11.glMatrixMode(5888);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -2000F);
        GL11.glLineWidth(1.0F);
        GL11.glDisable(3553);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(7);
        int i = (int)(l1 / 0x30d40L);
        tessellator.setColorOpaque_I(0x20000000);
        tessellator.addVertex(0.0D, field_6325_d - i, 0.0D);
        tessellator.addVertex(0.0D, field_6325_d, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d - i, 0.0D);
        tessellator.setColorOpaque_I(0x20200000);
        tessellator.addVertex(0.0D, field_6325_d - i * 2, 0.0D);
        tessellator.addVertex(0.0D, field_6325_d - i, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d - i, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d - i * 2, 0.0D);
        tessellator.draw();
        long l3 = 0L;
        for(int j = 0; j < field_6296_E.length; j++)
        {
            l3 += field_6296_E[j];
        }

        int k = (int)(l3 / 0x30d40L / (long)field_6296_E.length);
        tessellator.startDrawing(7);
        tessellator.setColorOpaque_I(0x20400000);
        tessellator.addVertex(0.0D, field_6325_d - k, 0.0D);
        tessellator.addVertex(0.0D, field_6325_d, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d, 0.0D);
        tessellator.addVertex(field_6296_E.length, field_6325_d - k, 0.0D);
        tessellator.draw();
        tessellator.startDrawing(1);
        for(int i1 = 0; i1 < field_6296_E.length; i1++)
        {
            int j1 = ((i1 - field_6294_G & field_6296_E.length - 1) * 255) / field_6296_E.length;
            int k1 = (j1 * j1) / 255;
            k1 = (k1 * k1) / 255;
            int i2 = (k1 * k1) / 255;
            i2 = (i2 * i2) / 255;
            if(field_6296_E[i1] > l1)
            {
                tessellator.setColorOpaque_I(0xff000000 + k1 * 0x10000);
            } else
            {
                tessellator.setColorOpaque_I(0xff000000 + k1 * 256);
            }
            long l4 = field_6296_E[i1] / 0x30d40L;
            long l5 = field_6295_F[i1] / 0x30d40L;
            tessellator.addVertex((float)i1 + 0.5F, (float)((long)field_6325_d - l4) + 0.5F, 0.0D);
            tessellator.addVertex((float)i1 + 0.5F, (float)field_6325_d + 0.5F, 0.0D);
            tessellator.setColorOpaque_I(0xff000000 + k1 * 0x10000 + k1 * 256 + k1 * 1);
            tessellator.addVertex((float)i1 + 0.5F, (float)((long)field_6325_d - l4) + 0.5F, 0.0D);
            tessellator.addVertex((float)i1 + 0.5F, (float)((long)field_6325_d - (l4 - l5)) + 0.5F, 0.0D);
        }

        tessellator.draw();
        GL11.glEnable(3553);
    }

    public void func_6244_d()
    {
        running = false;
    }

    public void func_6259_e()
    {
        if(!Display.isActive())
        {
            return;
        }
        if(field_6289_L)
        {
            return;
        } else
        {
            field_6289_L = true;
            mouseHelper.func_774_a();
            displayGuiScreen(null);
            field_6302_aa = field_6283_R + 10000;
            return;
        }
    }

    public void func_6273_f()
    {
        if(!field_6289_L)
        {
            return;
        }
        if(entityPlayerSP != null)
        {
            entityPlayerSP.func_458_k();
        }
        field_6289_L = false;
        mouseHelper.func_773_b();
    }

    public void func_6252_g()
    {
        if(guiScreen != null)
        {
            return;
        } else
        {
            displayGuiScreen(new GuiIngameMenu());
            return;
        }
    }

    private void func_6254_a(int i, boolean flag)
    {
        if(playerController.field_1064_b)
        {
            return;
        }
        if(i == 0 && field_6282_S > 0)
        {
            return;
        }
        if(flag && field_6305_x != null && field_6305_x.typeOfHit == 0 && i == 0)
        {
            int j = field_6305_x.blockX;
            int k = field_6305_x.blockY;
            int l = field_6305_x.blockZ;
            playerController.func_6470_c(j, k, l, field_6305_x.sideHit);
            effectRenderer.func_1191_a(j, k, l, field_6305_x.sideHit);
        } else
        {
            playerController.func_6468_a();
        }
    }

    private void func_6243_a(int i)
    {
        if(i == 0 && field_6282_S > 0)
        {
            return;
        }
        if(i == 0)
        {
            entityPlayerSP.func_457_w();
        }
        if(field_6305_x == null)
        {
            if(i == 0 && !(playerController instanceof PlayerControllerTest))
            {
                field_6282_S = 10;
            }
        } else
        if(field_6305_x.typeOfHit == 1)
        {
            if(i == 0)
            {
                playerController.func_6472_b(entityPlayerSP, field_6305_x.entityHit);
            }
            if(i == 1)
            {
                playerController.func_6475_a(entityPlayerSP, field_6305_x.entityHit);
            }
        } else
        if(field_6305_x.typeOfHit == 0)
        {
            int j = field_6305_x.blockX;
            int k = field_6305_x.blockY;
            int l = field_6305_x.blockZ;
            int i1 = field_6305_x.sideHit;
            Block block = Block.blocksList[world.getBlockId(j, k, l)];
            if(i == 0)
            {
                world.func_612_i(j, k, l, field_6305_x.sideHit);
                if(block != Block.bedrock || entityPlayerSP.field_6418_f >= 100)
                {
                    playerController.func_719_a(j, k, l, field_6305_x.sideHit);
                }
            } else
            {
                ItemStack itemstack1 = entityPlayerSP.inventory.getCurrentItem();
                int j1 = itemstack1 == null ? 0 : itemstack1.stackSize;
                if(playerController.func_722_a(entityPlayerSP, world, itemstack1, j, k, l, i1))
                {
                    entityPlayerSP.func_457_w();
                }
                if(itemstack1 == null)
                {
                    return;
                }
                if(itemstack1.stackSize == 0)
                {
                    entityPlayerSP.inventory.mainInventory[entityPlayerSP.inventory.currentItem_00] = null;
                } else
                if(itemstack1.stackSize != j1)
                {
                    entityRenderer.field_1395_a.func_6505_b();
                }
            }
        }
        if(i == 1)
        {
            ItemStack itemstack = entityPlayerSP.inventory.getCurrentItem();
            if(itemstack != null && playerController.func_6471_a(entityPlayerSP, world, itemstack))
            {
                entityRenderer.field_1395_a.func_6506_c();
            }
        }
    }

    public void func_6270_h()
    {
        try
        {
            mainFrame = !mainFrame;
            System.out.println("Toggle fullscreen!");
            if(mainFrame)
            {
                Display.setDisplayMode(Display.getDesktopDisplayMode());
                field_6326_c = Display.getDisplayMode().getWidth();
                field_6325_d = Display.getDisplayMode().getHeight();
                if(field_6326_c <= 0)
                {
                    field_6326_c = 1;
                }
                if(field_6325_d <= 0)
                {
                    field_6325_d = 1;
                }
            } else
            {
                if(field_6318_k != null)
                {
                    field_6326_c = field_6318_k.getWidth();
                    field_6325_d = field_6318_k.getHeight();
                } else
                {
                    field_6326_c = field_6281_T;
                    field_6325_d = field_6280_U;
                }
                if(field_6326_c <= 0)
                {
                    field_6326_c = 1;
                }
                if(field_6325_d <= 0)
                {
                    field_6325_d = 1;
                }
                Display.setDisplayMode(new org.lwjgl.opengl.DisplayMode(field_6281_T, field_6280_U));
            }
            func_6273_f();
            Display.setFullscreen(mainFrame);
            Display.update();
            Thread.sleep(1000L);
            if(mainFrame)
            {
                func_6259_e();
            }
            if(guiScreen != null)
            {
                func_6273_f();
                func_6249_a(field_6326_c, field_6325_d);
            }
            System.out.println((new StringBuilder()).append("Size: ").append(field_6326_c).append(", ").append(field_6325_d).toString());
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void func_6249_a(int i, int j)
    {
        if(i <= 0)
        {
            i = 1;
        }
        if(j <= 0)
        {
            j = 1;
        }
        field_6326_c = i;
        field_6325_d = j;
        if(guiScreen != null)
        {
            ScaledResolution scaledresolution = new ScaledResolution(i, j);
            int k = scaledresolution.getScaledWidth();
            int l = scaledresolution.getScaledHeight();
            guiScreen.func_6447_a(this, k, l);
        }
    }

    private void func_6265_t()
    {
        if(field_6305_x != null)
        {
            int i = world.getBlockId(field_6305_x.blockX, field_6305_x.blockY, field_6305_x.blockZ);
            if(i == Block.grass.blockID_00)
            {
                i = Block.dirt.blockID_00;
            }
            if(i == Block.stairDouble.blockID_00)
            {
                i = Block.stairSingle.blockID_00;
            }
            if(i == Block.bedrock.blockID_00)
            {
                i = Block.stone.blockID_00;
            }
            entityPlayerSP.inventory.func_496_a(i, playerController instanceof PlayerControllerTest);
        }
    }

    public void func_6246_i()
    {
        guiIngame.func_555_a();
        entityRenderer.func_910_a(1.0F);
        if(entityPlayerSP != null)
        {
            entityPlayerSP.func_6420_o();
        }
        if(!field_6316_m && world != null)
        {
            playerController.func_6474_c();
        }
        GL11.glBindTexture(3553, renderEngine.getTexture("/terrain.png"));
        if(!field_6316_m)
        {
            renderEngine.func_1067_a();
        }
        if(guiScreen == null && entityPlayerSP != null && entityPlayerSP.health_00 <= 0)
        {
            displayGuiScreen(null);
        }
        if(guiScreen != null)
        {
            field_6302_aa = field_6283_R + 10000;
        }
        if(guiScreen != null)
        {
            guiScreen.handleInput();
            if(guiScreen != null)
            {
                guiScreen.updateScreen();
            }
        }
        if(guiScreen == null || guiScreen.field_948_f)
        {
            do
            {
                if(!Mouse.next())
                {
                    break;
                }
                long l = System.currentTimeMillis() - field_6287_N;
                if(l <= 200L)
                {
                    int j = Mouse.getEventDWheel();
                    if(j != 0)
                    {
                        entityPlayerSP.inventory.func_498_a(j);
                    }
                    if(guiScreen == null)
                    {
                        if(!field_6289_L && Mouse.getEventButtonState())
                        {
                            func_6259_e();
                        } else
                        {
                            if(Mouse.getEventButton() == 0 && Mouse.getEventButtonState())
                            {
                                func_6243_a(0);
                                field_6302_aa = field_6283_R;
                            }
                            if(Mouse.getEventButton() == 1 && Mouse.getEventButtonState())
                            {
                                func_6243_a(1);
                                field_6302_aa = field_6283_R;
                            }
                            if(Mouse.getEventButton() == 2 && Mouse.getEventButtonState())
                            {
                                func_6265_t();
                            }
                        }
                    } else
                    if(guiScreen != null)
                    {
                        guiScreen.handleMouseInput();
                    }
                }
            } while(true);
            if(field_6282_S > 0)
            {
                field_6282_S--;
            }
            do
            {
                if(!Keyboard.next())
                {
                    break;
                }
                entityPlayerSP.func_460_a(Keyboard.getEventKey(), Keyboard.getEventKeyState());
                if(Keyboard.getEventKeyState())
                {
                    if(Keyboard.getEventKey() == 87)
                    {
                        func_6270_h();
                    } else
                    if(Keyboard.getEventKey() == 62)
                    {
                        (new Teleporter()).func_4108_c(world, entityPlayerSP);
                    } else
                    {
                        if(guiScreen != null)
                        {
                            guiScreen.handleKeyboardInput();
                        } else
                        {
                            if(Keyboard.getEventKey() == 1)
                            {
                                func_6252_g();
                            }
                            if(Keyboard.getEventKey() == 31 && Keyboard.isKeyDown(61))
                            {
                                func_6242_u();
                            }
                            if(Keyboard.getEventKey() == 63)
                            {
                                gameSettings.thirdPersonView = !gameSettings.thirdPersonView;
                            }
                            if(Keyboard.getEventKey() == gameSettings.keyBindInventory.keyCode)
                            {
                                displayGuiScreen(new GuiInventory(entityPlayerSP.inventory, entityPlayerSP.inventory.craftingInventory));
                            }
                            if(Keyboard.getEventKey() == gameSettings.field_6523_q.keyCode)
                            {
                                entityPlayerSP.func_444_a(entityPlayerSP.inventory.decrStackSize(entityPlayerSP.inventory.currentItem_00, 1), false);
                            }
                            if(func_6260_j() && Keyboard.getEventKey() == gameSettings.field_6521_r.keyCode)
                            {
                                displayGuiScreen(new GuiChat());
                            }
                        }
                        for(int i = 0; i < 9; i++)
                        {
                            if(Keyboard.getEventKey() == 2 + i)
                            {
                                entityPlayerSP.inventory.currentItem_00 = i;
                            }
                        }

                        if(Keyboard.getEventKey() == gameSettings.field_6520_s.keyCode)
                        {
                            gameSettings.setOptionValue(4, !Keyboard.isKeyDown(42) && !Keyboard.isKeyDown(54) ? 1 : -1);
                        }
                    }
                }
            } while(true);
            if(guiScreen == null)
            {
                if(Mouse.isButtonDown(0) && (float)(field_6283_R - field_6302_aa) >= timer.field_1380_a / 4F && field_6289_L)
                {
                    func_6243_a(0);
                    field_6302_aa = field_6283_R;
                }
                if(Mouse.isButtonDown(1) && (float)(field_6283_R - field_6302_aa) >= timer.field_1380_a / 4F && field_6289_L)
                {
                    func_6243_a(1);
                    field_6302_aa = field_6283_R;
                }
            }
            func_6254_a(0, guiScreen == null && Mouse.isButtonDown(0) && field_6289_L);
        }
        if(world != null)
        {
            if(entityPlayerSP != null)
            {
                field_6300_ab++;
                if(field_6300_ab == 30)
                {
                    field_6300_ab = 0;
                    world.func_705_f(entityPlayerSP);
                }
            }
            world.field_1039_l = gameSettings.difficulty;
            if(!field_6316_m)
            {
                entityRenderer.func_911_a();
            }
            if(!field_6316_m)
            {
                field_6323_f.func_945_d();
            }
            if(!field_6316_m)
            {
                world.func_633_c();
            }
            if(!field_6316_m || func_6260_j())
            {
                world.tick();
            }
            if(!field_6316_m && world != null)
            {
                world.randomDisplayUpdates(MathHelper.convertToBlockCoord_00(entityPlayerSP.posX), MathHelper.convertToBlockCoord_00(entityPlayerSP.posY), MathHelper.convertToBlockCoord_00(entityPlayerSP.posZ));
            }
            if(!field_6316_m)
            {
                effectRenderer.func_1193_a();
            }
        }
        field_6287_N = System.currentTimeMillis();
    }

    private void func_6242_u()
    {
        System.out.println("FORCING RELOAD!");
        soundManager = new SoundManager();
        soundManager.func_340_a(gameSettings);
        field_6284_Q.reloadResources();
    }

    public boolean func_6260_j()
    {
        return world != null && world.multiplayerWorld;
    }

    public void loadWorld(String s) {
        loadWorld(s, new Random().nextLong());
    }

    public void loadWorld(String s, long seed)
    {
        setWorld(null);
        System.gc();
        World world = new World(new File(func_6240_b(), "saves"), s, seed);
        if(world.newWorld)
        {
            setWorld(world, "Generating level");
        } else
        {
            setWorld(world, "Loading level");
        }
    }

    public void func_6237_k()
    {
        if(entityPlayerSP.field_4129_m == -1)
        {
            entityPlayerSP.field_4129_m = 0;
        } else
        {
            entityPlayerSP.field_4129_m = -1;
        }
        world.func_607_d(entityPlayerSP);
        entityPlayerSP.field_646_aA = false;
        double d = entityPlayerSP.posX;
        double d1 = entityPlayerSP.posZ;
        double d2 = 8D;
        if(entityPlayerSP.field_4129_m == -1)
        {
            d /= d2;
            d1 /= d2;
            entityPlayerSP.func_365_c(d, entityPlayerSP.posY, d1, entityPlayerSP.rotationYaw, entityPlayerSP.rotationPitch);
            world.func_4084_a(entityPlayerSP, false);
            World world = new World(this.world, new WorldProviderHell());
            setWorld(world, "Entering the Nether", entityPlayerSP);
        } else
        {
            d *= d2;
            d1 *= d2;
            entityPlayerSP.func_365_c(d, entityPlayerSP.posY, d1, entityPlayerSP.rotationYaw, entityPlayerSP.rotationPitch);
            world.func_4084_a(entityPlayerSP, false);
            World world1 = new World(world, new WorldProvider());
            setWorld(world1, "Leaving the Nether", entityPlayerSP);
        }
        entityPlayerSP.worldObj_09 = world;
        entityPlayerSP.func_365_c(d, entityPlayerSP.posY, d1, entityPlayerSP.rotationYaw, entityPlayerSP.rotationPitch);
        world.func_4084_a(entityPlayerSP, false);
        (new Teleporter()).func_4107_a(world, entityPlayerSP);
    }

    public void setWorld(World world)
    {
        setWorld(world, "");
    }

    public void setWorld(World world, String s)
    {
        setWorld(world, s, null);
    }

    public void setWorld(World world, String loadingText, EntityPlayer entityplayer)
    {
        loadingScreenRenderer.func_596_a(loadingText);
        loadingScreenRenderer.func_595_d("");
        soundManager.func_331_a(null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
        if(this.world != null)
        {
            this.world.func_651_a(loadingScreenRenderer);
        }
        this.world = world;
        System.out.println((new StringBuilder()).append("Player is ").append(entityPlayerSP).toString());
        if(world != null)
        {
            playerController.func_717_a(world);
            if(!func_6260_j())
            {
                if(entityplayer == null)
                {
                    entityPlayerSP = (EntityPlayerSP)world.func_4085_a(EntityPlayerSP.class);
                }
            } else
            if(entityPlayerSP != null)
            {
                entityPlayerSP.func_374_q();
                if(world != null)
                {
                    world.entityJoinedWorld(entityPlayerSP);
                }
            }
            if(!world.multiplayerWorld)
            {
                func_6255_d(loadingText);
            }
            System.out.println((new StringBuilder()).append("Player is now ").append(entityPlayerSP).toString());
            if(entityPlayerSP == null)
            {
                entityPlayerSP = (EntityPlayerSP) playerController.func_4087_b(world);
                entityPlayerSP.func_374_q();
                playerController.func_6476_a(entityPlayerSP);
            }
            entityPlayerSP.field_787_a = new MovementInputFromOptions(gameSettings);
            if(field_6323_f != null)
            {
                field_6323_f.func_946_a(world);
            }
            if(effectRenderer != null)
            {
                effectRenderer.func_1188_a(world);
            }
            playerController.func_6473_b(entityPlayerSP);
            if(entityplayer != null)
            {
                world.func_6464_c();
            }
            world.func_608_a(entityPlayerSP);
            if(world.newWorld)
            {
                world.func_651_a(loadingScreenRenderer);
            }
        } else
        {
            entityPlayerSP = null;
        }
        System.gc();
        field_6287_N = 0L;
    }

    private void func_6255_d(String s)
    {
        loadingScreenRenderer.func_596_a(s);
        loadingScreenRenderer.func_595_d("Building terrain");
        char c = '\200';
        int i = 0;
        int j = (c * 2) / 16 + 1;
        j *= j;
        for(int k = -c; k <= c; k += 16)
        {
            int l = world.spawnX;
            int i1 = world.spawnZ;
            if(entityPlayerSP != null)
            {
                l = (int) entityPlayerSP.posX;
                i1 = (int) entityPlayerSP.posZ;
            }
            for(int j1 = -c; j1 <= c; j1 += 16)
            {
                loadingScreenRenderer.func_593_a((i++ * 100) / j);
                world.getBlockId(l + k, 64, i1 + j1);
                while(world.func_6465_g()) ;
            }

        }

        loadingScreenRenderer.func_595_d("Simulating world for a bit");
        j = 2000;
        world.func_656_j();
    }

    public void func_6268_a(String s, File file)
    {
        int i = s.indexOf("/");
        String s1 = s.substring(0, i);
        s = s.substring(i + 1);
        if(s1.equalsIgnoreCase("sound"))
        {
            soundManager.func_6372_a(s, file);
        } else
        if(s1.equalsIgnoreCase("newsound"))
        {
            soundManager.func_6372_a(s, file);
        } else
        if(s1.equalsIgnoreCase("streaming"))
        {
            soundManager.func_6373_b(s, file);
        } else
        if(s1.equalsIgnoreCase("music"))
        {
            soundManager.func_6374_c(s, file);
        } else
        if(s1.equalsIgnoreCase("newmusic"))
        {
            soundManager.func_6374_c(s, file);
        }
    }

    public OpenGlCapsChecker func_6251_l()
    {
        return openGlCapsChecker;
    }

    public String func_6241_m()
    {
        return field_6323_f.func_953_b();
    }

    public String func_6262_n()
    {
        return field_6323_f.func_957_c();
    }

    public String func_6245_o()
    {
        return (new StringBuilder()).append("P: ").append(effectRenderer.func_1190_b()).append(". T: ").append(world.func_687_d()).toString();
    }

    public String getDebugCoordinates() {
        return String.format("XYZ: %.3f / %.5f / %.3f", entityPlayerSP.posX, entityPlayerSP.posY, entityPlayerSP.posZ);
    }

    public void func_6239_p()
    {
        if(!world.field_4209_q.func_6477_d())
        {
            func_6237_k();
        }
        world.func_4076_b();
        if(entityPlayerSP != null)
        {
            world.func_607_d(entityPlayerSP);
        }
        entityPlayerSP = (EntityPlayerSP) playerController.func_4087_b(world);
        entityPlayerSP.func_374_q();
        playerController.func_6476_a(entityPlayerSP);
        world.func_608_a(entityPlayerSP);
        entityPlayerSP.field_787_a = new MovementInputFromOptions(gameSettings);
        playerController.func_6473_b(entityPlayerSP);
        func_6255_d("Respawning");
    }

    public static void func_6269_a(String s, String s1)
    {
        func_6253_a(s, s1, null);
    }

    public static void func_6253_a(String s, String s1, String s2)
    {
        boolean flag = false;
        String s3 = s;
        Frame frame = new Frame("Minecraft");
        Canvas canvas = new Canvas();
        frame.setLayout(new BorderLayout());
        frame.add(canvas, "Center");
        canvas.setPreferredSize(new Dimension(854, 480));
        frame.pack();
        frame.setLocationRelativeTo(null);
        MinecraftImpl minecraftimpl = new MinecraftImpl(frame, canvas, null, 854, 480, flag, frame);
        Thread thread = new Thread(minecraftimpl, "Minecraft main thread");
        thread.setPriority(10);
        minecraftimpl.field_6317_l = false;
        minecraftimpl.field_6319_j = "www.minecraft.net";
        if(s3 != null && s1 != null)
        {
            minecraftimpl.field_6320_i = new Session(s3, s1);
        } else
        {
            minecraftimpl.field_6320_i = new Session((new StringBuilder()).append("Player").append(System.currentTimeMillis() % 1000L).toString(), "");
        }
        if(s2 != null)
        {
            String as[] = s2.split(":");
            minecraftimpl.func_6258_a(as[0], Integer.parseInt(as[1]));
        }
        frame.setVisible(true);
        frame.addWindowListener(new GameWindowListener(minecraftimpl, thread));
        thread.start();
    }

    public static void main(String args[])
    {
        String s = (new StringBuilder()).append("Player").append(System.currentTimeMillis() % 1000L).toString();
        if(args.length > 0)
        {
            s = args[0];
        }
        String s1 = "-";
        if(args.length > 1)
        {
            s1 = args[1];
        }
        s = (new StringBuilder()).append("Player").append(System.currentTimeMillis() % 1000L).toString();
        func_6269_a(s, s1);
    }

    public PlayerController playerController;
    private boolean mainFrame;
    public int field_6326_c;
    public int field_6325_d;
    private OpenGlCapsChecker openGlCapsChecker;
    private Timer timer; // TODO: check what for
    public World world;
    public RenderGlobal field_6323_f;
    public EntityPlayerSP entityPlayerSP;
    public EffectRenderer effectRenderer;
    public Session field_6320_i;
    public String field_6319_j;
    public Canvas field_6318_k;
    public boolean field_6317_l;
    public volatile boolean field_6316_m;
    public RenderEngine renderEngine;
    public FontRenderer fontRenderer;
    public GuiScreen guiScreen;
    public LoadingScreenRenderer loadingScreenRenderer;
    public EntityRenderer entityRenderer;
    private ThreadDownloadResources field_6284_Q;
    private int field_6283_R;
    private int field_6282_S;
    private int field_6281_T;
    private int field_6280_U;
    public String field_6310_s;
    public int field_6309_t;
    public GuiIngame guiIngame;
    public boolean field_6307_v;
    public ModelBiped field_6306_w;
    public MovingObjectPosition field_6305_x;
    public GameSettings gameSettings;
    protected MinecraftApplet field_6303_z;
    public SoundManager soundManager;
    public MouseHelper mouseHelper;
    public TexturePackList texturePackList;
    public File field_6297_D;
    public static long field_6296_E[] = new long[512];
    public static long field_6295_F[] = new long[512];
    public static int field_6294_G = 0;
    private String field_6279_V;
    private int field_6278_W;
    private TextureWaterFX field_6277_X;
    private TextureLavaFX field_6276_Y;
    private static File field_6275_Z = null;
    public volatile boolean running;
    public String field_6292_I;
    boolean field_6291_J;
    long field_6290_K;
    public boolean field_6289_L;
    private int field_6302_aa;
    public boolean field_6288_M;
    long field_6287_N;
    private int field_6300_ab;

}
