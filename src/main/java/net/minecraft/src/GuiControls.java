package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiControls extends GuiScreen
{

    public GuiControls(GuiScreen guiscreen, GameSettings gamesettings)
    {
        screenTitle_01 = "Controls";
        buttonId = -1;
        parentScreen_00 = guiscreen;
        options_01 = gamesettings;
    }

    public void initGui()
    {
        for(int i = 0; i < options_01.keyBindings.length; i++)
        {
            controlList.add(new GuiSmallButton(i, (width_02 / 2 - 155) + (i % 2) * 160, height_02 / 6 + 24 * (i >> 1), options_01.getKeyBinding(i)));
        }

        controlList.add(new GuiButton(200, width_02 / 2 - 100, height_02 / 6 + 168, "Done"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        for(int i = 0; i < options_01.keyBindings.length; i++)
        {
            ((GuiButton)controlList.get(i)).displayString = options_01.getKeyBinding(i);
        }

        if(guibutton.id_02 == 200)
        {
            mc_06.displayGuiScreen(parentScreen_00);
        } else
        {
            buttonId = guibutton.id_02;
            guibutton.displayString = (new StringBuilder()).append("> ").append(options_01.getKeyBinding(guibutton.id_02)).append(" <").toString();
        }
    }

    protected void keyTyped(char c, int i)
    {
        if(buttonId >= 0)
        {
            options_01.setKeyBinding(buttonId, i);
            ((GuiButton)controlList.get(buttonId)).displayString = options_01.getKeyBinding(buttonId);
            buttonId = -1;
        } else
        {
            super.keyTyped(c, i);
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, screenTitle_01, width_02 / 2, 20, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private GuiScreen parentScreen_00;
    protected String screenTitle_01;
    private GameSettings options_01;
    private int buttonId;
}
