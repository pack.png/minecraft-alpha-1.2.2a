package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.Item;
import net.minecraft.src.MathHelper;
import net.minecraft.src.world.World;

public class EntityZombie extends EntityMobs
{

    public EntityZombie(World world)
    {
        super(world);
        field_6409_z = "/mob/zombie.png";
        field_6401_al = 0.5F;
        field_762_e = 5;
    }

    public void onLivingUpdate()
    {
        if(worldObj_09.isDaytime())
        {
            float f = getEntityBrightness(1.0F);
            if(f > 0.5F && worldObj_09.canBlockSeeTheSky(MathHelper.convertToBlockCoord_00(posX), MathHelper.convertToBlockCoord_00(posY), MathHelper.convertToBlockCoord_00(posZ)) && rand_05.nextFloat() * 30F < (f - 0.4F) * 2.0F)
            {
                fire_00 = 300;
            }
        }
        super.onLivingUpdate();
    }

    protected String func_6389_d()
    {
        return "mob.zombie";
    }

    protected String func_6394_f_()
    {
        return "mob.zombiehurt";
    }

    protected String func_6390_f()
    {
        return "mob.zombiedeath";
    }

    protected int getDropItemId()
    {
        return Item.feather.swiftedIndex;
    }
}
