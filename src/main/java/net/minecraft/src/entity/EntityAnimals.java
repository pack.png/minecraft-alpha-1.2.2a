package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.MathHelper;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.Block;
import net.minecraft.src.world.World;

public abstract class EntityAnimals extends EntityCreature
{

    public EntityAnimals(World world)
    {
        super(world);
    }

    protected float func_439_a(int i, int j, int k)
    {
        if(worldObj_09.getBlockId(i, j - 1, k) == Block.grass.blockID_00)
        {
            return 10F;
        } else
        {
            return worldObj_09.getLightBrightness(i, j, k) - 0.5F;
        }
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    public boolean getCanSpawnHere()
    {
        int i = MathHelper.convertToBlockCoord_00(posX);
        int j = MathHelper.convertToBlockCoord_00(boundingBox.minY);
        int k = MathHelper.convertToBlockCoord_00(posZ);
        return worldObj_09.getBlockId(i, j - 1, k) == Block.grass.blockID_00 && worldObj_09.getBlockLightValue_00(i, j, k) > 8 && super.getCanSpawnHere();
    }

    public int func_421_b()
    {
        return 120;
    }
}
