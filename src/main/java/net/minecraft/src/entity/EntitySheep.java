package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.Block;
import net.minecraft.src.world.World;

public class EntitySheep extends EntityAnimals
{

    public EntitySheep(World world)
    {
        super(world);
        sheared = false;
        field_6409_z = "/mob/sheep.png";
        setSize(0.9F, 1.3F);
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(!sheared && (entity instanceof EntityLiving))
        {
            sheared = true;
            int j = 1 + rand_05.nextInt(3);
            for(int k = 0; k < j; k++)
            {
                EntityItem entityitem = dropItemWithOffset(Block.cloth.blockID_00, 1, 1.0F);
                entityitem.motionY += rand_05.nextFloat() * 0.05F;
                entityitem.motionX += (rand_05.nextFloat() - rand_05.nextFloat()) * 0.1F;
                entityitem.motionZ += (rand_05.nextFloat() - rand_05.nextFloat()) * 0.1F;
            }

        }
        return super.attackEntity(entity, i);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setBoolean("Sheared", sheared);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
        sheared = nbttagcompound.getBoolean("Sheared");
    }

    protected String func_6389_d()
    {
        return "mob.sheep";
    }

    protected String func_6394_f_()
    {
        return "mob.sheep";
    }

    protected String func_6390_f()
    {
        return "mob.sheep";
    }

    public boolean sheared;
}
