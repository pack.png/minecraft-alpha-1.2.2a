package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.world.World;

public class EntitySkeleton extends EntityMobs
{

    public EntitySkeleton(World world)
    {
        super(world);
        field_6409_z = "/mob/skeleton.png";
    }

    protected String func_6389_d()
    {
        return "mob.skeleton";
    }

    protected String func_6394_f_()
    {
        return "mob.skeletonhurt";
    }

    protected String func_6390_f()
    {
        return "mob.skeletonhurt";
    }

    public void onLivingUpdate()
    {
        if(worldObj_09.isDaytime())
        {
            float f = getEntityBrightness(1.0F);
            if(f > 0.5F && worldObj_09.canBlockSeeTheSky(MathHelper.convertToBlockCoord_00(posX), MathHelper.convertToBlockCoord_00(posY), MathHelper.convertToBlockCoord_00(posZ)) && rand_05.nextFloat() * 30F < (f - 0.4F) * 2.0F)
            {
                fire_00 = 300;
            }
        }
        super.onLivingUpdate();
    }

    protected void func_437_a(Entity entity, float f)
    {
        if(f < 10F)
        {
            double d = entity.posX - posX;
            double d1 = entity.posZ - posZ;
            if(attackTime == 0)
            {
                EntityArrow entityarrow = new EntityArrow(worldObj_09, this);
                entityarrow.posY += 1.3999999761581421D;
                double d2 = entity.posY - 0.20000000298023224D - entityarrow.posY;
                float f1 = MathHelper.sqrt_double(d * d + d1 * d1) * 0.2F;
                worldObj_09.playSoundAtEntity(this, "random.bow", 1.0F, 1.0F / (rand_05.nextFloat() * 0.4F + 0.8F));
                worldObj_09.entityJoinedWorld(entityarrow);
                entityarrow.func_408_a(d, d2 + (double)f1, d1, 0.6F, 12F);
                attackTime = 30;
            }
            rotationYaw = (float)((Math.atan2(d1, d) * 180D) / 3.1415927410125732D) - 90F;
            field_750_g = true;
        }
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    protected int getDropItemId()
    {
        return Item.arrow.swiftedIndex;
    }

    public ItemStack func_4045_l()
    {
        return field_4119_a;
    }

    private static final ItemStack field_4119_a;

    static 
    {
        field_4119_a = new ItemStack(Item.bow, 1);
    }
}
