package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.Item;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.world.World;

public class EntityCreeper extends EntityMobs
{

    public EntityCreeper(World world)
    {
        super(world);
        field_766_c = 30;
        field_765_d = -1;
        field_6409_z = "/mob/creeper.png";
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    protected void func_418_b_()
    {
        field_763_b = field_764_a;
        if(field_764_a > 0 && field_765_d < 0)
        {
            field_764_a--;
        }
        if(field_765_d >= 0)
        {
            field_765_d = 2;
        }
        super.func_418_b_();
        if(field_765_d != 1)
        {
            field_765_d = -1;
        }
    }

    protected String func_6394_f_()
    {
        return "mob.creeper";
    }

    protected String func_6390_f()
    {
        return "mob.creeperdeath";
    }

    public void onDeath(Entity entity)
    {
        super.onDeath(entity);
        if(entity instanceof EntitySkeleton)
        {
            dropItem(Item.record13.swiftedIndex + rand_05.nextInt(2), 1);
        }
    }

    protected void func_437_a(Entity entity, float f)
    {
        if(field_765_d <= 0 && f < 3F || field_765_d > 0 && f < 7F)
        {
            if(field_764_a == 0)
            {
                worldObj_09.playSoundAtEntity(this, "random.fuse", 1.0F, 0.5F);
            }
            field_765_d = 1;
            field_764_a++;
            if(field_764_a == field_766_c)
            {
                worldObj_09.createExplosion(this, posX, posY, posZ, 3F);
                setEntityDead();
            }
            field_750_g = true;
        }
    }

    public float func_440_b(float f)
    {
        return ((float)field_763_b + (float)(field_764_a - field_763_b) * f) / (float)(field_766_c - 2);
    }

    protected int getDropItemId()
    {
        return Item.gunpowder.swiftedIndex;
    }

    int field_764_a;
    int field_763_b;
    int field_766_c;
    int field_765_d;
}
