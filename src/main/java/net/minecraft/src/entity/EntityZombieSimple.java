package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.world.World;

public class EntityZombieSimple extends EntityMobs
{

    public EntityZombieSimple(World world)
    {
        super(world);
        field_6409_z = "/mob/zombie.png";
        field_6401_al = 0.5F;
        field_762_e = 50;
        health_00 *= 10;
        yOffset_00 *= 6F;
        setSize(width_00 * 6F, height_01 * 6F);
    }

    protected float func_439_a(int i, int j, int k)
    {
        return worldObj_09.getLightBrightness(i, j, k) - 0.5F;
    }
}
