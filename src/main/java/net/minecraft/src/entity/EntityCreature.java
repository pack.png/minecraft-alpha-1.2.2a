package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.MathHelper;
import net.minecraft.src.PathEntity;
import net.minecraft.src.Vec3D;
import net.minecraft.src.world.World;

public class EntityCreature extends EntityLiving
{

    public EntityCreature(World world)
    {
        super(world);
        field_750_g = false;
    }

    protected void func_418_b_()
    {
        field_750_g = false;
        float f = 16F;
        if(field_751_f == null)
        {
            field_751_f = func_438_i();
            if(field_751_f != null)
            {
                field_749_a = worldObj_09.func_702_a(this, field_751_f, f);
            }
        } else
        if(!field_751_f.func_354_B())
        {
            field_751_f = null;
        } else
        {
            float f1 = field_751_f.getDistanceToEntity(this);
            if(func_420_c(field_751_f))
            {
                func_437_a(field_751_f, f1);
            }
        }
        if(!field_750_g && field_751_f != null && (field_749_a == null || rand_05.nextInt(20) == 0))
        {
            field_749_a = worldObj_09.func_702_a(this, field_751_f, f);
        } else
        if(field_749_a == null && rand_05.nextInt(80) == 0 || rand_05.nextInt(80) == 0)
        {
            boolean flag = false;
            int j = -1;
            int k = -1;
            int l = -1;
            float f2 = -99999F;
            for(int i1 = 0; i1 < 10; i1++)
            {
                int j1 = MathHelper.convertToBlockCoord_00((posX + (double)rand_05.nextInt(13)) - 6D);
                int k1 = MathHelper.convertToBlockCoord_00((posY + (double)rand_05.nextInt(7)) - 3D);
                int l1 = MathHelper.convertToBlockCoord_00((posZ + (double)rand_05.nextInt(13)) - 6D);
                float f3 = func_439_a(j1, k1, l1);
                if(f3 > f2)
                {
                    f2 = f3;
                    j = j1;
                    k = k1;
                    l = l1;
                    flag = true;
                }
            }

            if(flag)
            {
                field_749_a = worldObj_09.func_637_a(this, j, k, l, 10F);
            }
        }
        int i = MathHelper.convertToBlockCoord_00(boundingBox.minY);
        boolean flag1 = handleWaterMovement();
        boolean flag2 = func_359_G();
        rotationPitch = 0.0F;
        if(field_749_a == null || rand_05.nextInt(100) == 0)
        {
            super.func_418_b_();
            field_749_a = null;
            return;
        }
        Vec3D vec3d = field_749_a.getPosition(this);
        for(double d = width_00 * 2.0F; vec3d != null && vec3d.squareDistanceTo(posX, vec3d.yCoord_00, posZ) < d * d;)
        {
            field_749_a.incrementPathIndex();
            if(field_749_a.isFinished())
            {
                vec3d = null;
                field_749_a = null;
            } else
            {
                vec3d = field_749_a.getPosition(this);
            }
        }

        field_697_Y = false;
        if(vec3d != null)
        {
            double d1 = vec3d.xCoord_00 - posX;
            double d2 = vec3d.zCoord_00 - posZ;
            double d3 = vec3d.yCoord_00 - (double)i;
            float f4 = (float)((Math.atan2(d2, d1) * 180D) / 3.1415927410125732D) - 90F;
            float f5 = f4 - rotationYaw;
            field_699_W = field_6401_al;
            for(; f5 < -180F; f5 += 360F) { }
            for(; f5 >= 180F; f5 -= 360F) { }
            if(f5 > 30F)
            {
                f5 = 30F;
            }
            if(f5 < -30F)
            {
                f5 = -30F;
            }
            rotationYaw += f5;
            if(field_750_g && field_751_f != null)
            {
                double d4 = field_751_f.posX - posX;
                double d5 = field_751_f.posZ - posZ;
                float f7 = rotationYaw;
                rotationYaw = (float)((Math.atan2(d5, d4) * 180D) / 3.1415927410125732D) - 90F;
                float f6 = (((f7 - rotationYaw) + 90F) * 3.141593F) / 180F;
                field_700_V = -MathHelper.sin(f6) * field_699_W * 1.0F;
                field_699_W = MathHelper.cos(f6) * field_699_W * 1.0F;
            }
            if(d3 > 0.0D)
            {
                field_697_Y = true;
            }
        }
        if(field_751_f != null)
        {
            func_426_b(field_751_f, 30F);
        }
        if(field_599_aw)
        {
            field_697_Y = true;
        }
        if(rand_05.nextFloat() < 0.8F && (flag1 || flag2))
        {
            field_697_Y = true;
        }
    }

    protected void func_437_a(Entity entity, float f)
    {
    }

    protected float func_439_a(int i, int j, int k)
    {
        return 0.0F;
    }

    protected Entity func_438_i()
    {
        return null;
    }

    public boolean getCanSpawnHere()
    {
        int i = MathHelper.convertToBlockCoord_00(posX);
        int j = MathHelper.convertToBlockCoord_00(boundingBox.minY);
        int k = MathHelper.convertToBlockCoord_00(posZ);
        return super.getCanSpawnHere() && func_439_a(i, j, k) >= 0.0F;
    }

    private PathEntity field_749_a;
    protected Entity field_751_f;
    protected boolean field_750_g;
}
