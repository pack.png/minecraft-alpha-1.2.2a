package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.MathHelper;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.world.World;

public class EntityTNTPrimed extends Entity
{

    public EntityTNTPrimed(World world)
    {
        super(world);
        fuse = 0;
        field_618_ad = true;
        setSize(0.98F, 0.98F);
        yOffset_00 = height_01 / 2.0F;
    }

    public EntityTNTPrimed(World world, float f, float f1, float f2)
    {
        this(world);
        setPosition(f, f1, f2);
        float f3 = (float)(Math.random() * 3.1415927410125732D * 2D);
        motionX = -MathHelper.sin((f3 * 3.141593F) / 180F) * 0.02F;
        motionY = 0.20000000298023224D;
        motionZ = -MathHelper.cos((f3 * 3.141593F) / 180F) * 0.02F;
        entityWalks = false;
        fuse = 80;
        prevPosX = f;
        prevPosY = f1;
        prevPosZ = f2;
    }

    public boolean func_401_c_()
    {
        return !field_646_aA;
    }

    public void onUpdate()
    {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        motionY -= 0.039999999105930328D;
        moveEntity(motionX, motionY, motionZ);
        motionX *= 0.98000001907348633D;
        motionY *= 0.98000001907348633D;
        motionZ *= 0.98000001907348633D;
        if(onGround_00)
        {
            motionX *= 0.69999998807907104D;
            motionZ *= 0.69999998807907104D;
            motionY *= -0.5D;
        }
        if(fuse-- <= 0)
        {
            setEntityDead();
            explode();
        } else
        {
            worldObj_09.spawnParticle_00("smoke", posX, posY + 0.5D, posZ, 0.0D, 0.0D, 0.0D);
        }
    }

    private void explode()
    {
        float f = 4F;
        worldObj_09.createExplosion(null, posX, posY, posZ, f);
    }

    protected void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setByte("Fuse", (byte)fuse);
    }

    protected void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        fuse = nbttagcompound.getByte("Fuse");
    }

    public float func_392_h_()
    {
        return 0.0F;
    }

    public int fuse;
}
