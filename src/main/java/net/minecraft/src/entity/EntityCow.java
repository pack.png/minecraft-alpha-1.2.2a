package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.*;
import net.minecraft.src.world.World;

public class EntityCow extends EntityAnimals
{

    public EntityCow(World world)
    {
        super(world);
        unusedBoolean = false;
        field_6409_z = "/mob/cow.png";
        setSize(0.9F, 1.3F);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    protected String func_6389_d()
    {
        return "mob.cow";
    }

    protected String func_6394_f_()
    {
        return "mob.cowhurt";
    }

    protected String func_6390_f()
    {
        return "mob.cowhurt";
    }

    protected float func_6393_h()
    {
        return 0.4F;
    }

    protected int getDropItemId()
    {
        return Item.leather.swiftedIndex;
    }

    public boolean interact(EntityPlayer entityplayer)
    {
        ItemStack itemstack = entityplayer.inventory.getCurrentItem();
        if(itemstack != null && itemstack.itemID == Item.bucketEmpty.swiftedIndex)
        {
            entityplayer.inventory.setInventorySlotContents(entityplayer.inventory.currentItem_00, new ItemStack(Item.bucketMilk));
            return true;
        } else
        {
            return false;
        }
    }

    public boolean unusedBoolean;
}
