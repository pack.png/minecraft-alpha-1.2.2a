package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.world.World;

import java.util.List;

public class EntityFireball extends Entity
{

    public EntityFireball(World world)
    {
        super(world);
        field_4152_e = -1;
        field_4151_f = -1;
        field_4150_g = -1;
        field_4149_h = 0;
        field_4148_i = false;
        field_4156_a = 0;
        field_4145_l = 0;
        setSize(1.0F, 1.0F);
    }

    public boolean func_384_a(double d)
    {
        double d1 = boundingBox.getAverageEdgeLength() * 4D;
        d1 *= 64D;
        return d < d1 * d1;
    }

    public EntityFireball(World world, EntityLiving entityliving, double d, double d1, double d2)
    {
        super(world);
        field_4152_e = -1;
        field_4151_f = -1;
        field_4150_g = -1;
        field_4149_h = 0;
        field_4148_i = false;
        field_4156_a = 0;
        field_4145_l = 0;
        field_4147_j = entityliving;
        setSize(1.0F, 1.0F);
        func_365_c(entityliving.posX, entityliving.posY, entityliving.posZ, entityliving.rotationYaw, entityliving.rotationPitch);
        setPosition(posX, posY, posZ);
        yOffset_00 = 0.0F;
        motionX = motionY = motionZ = 0.0D;
        d += rand_05.nextGaussian() * 0.40000000000000002D;
        d1 += rand_05.nextGaussian() * 0.40000000000000002D;
        d2 += rand_05.nextGaussian() * 0.40000000000000002D;
        double d3 = MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2);
        field_4155_b = (d / d3) * 0.10000000000000001D;
        field_4154_c = (d1 / d3) * 0.10000000000000001D;
        field_4153_d = (d2 / d3) * 0.10000000000000001D;
    }

    public void onUpdate()
    {
        super.onUpdate();
        fire_00 = 10;
        if(field_4156_a > 0)
        {
            field_4156_a--;
        }
        if(field_4148_i)
        {
            int i = worldObj_09.getBlockId(field_4152_e, field_4151_f, field_4150_g);
            if(i != field_4149_h)
            {
                field_4148_i = false;
                motionX *= rand_05.nextFloat() * 0.2F;
                motionY *= rand_05.nextFloat() * 0.2F;
                motionZ *= rand_05.nextFloat() * 0.2F;
                field_6436_k = 0;
                field_4145_l = 0;
            } else
            {
                field_6436_k++;
                if(field_6436_k == 1200)
                {
                    setEntityDead();
                }
                return;
            }
        } else
        {
            field_4145_l++;
        }
        Vec3D vec3d = Vec3D.createVector(posX, posY, posZ);
        Vec3D vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        MovingObjectPosition movingobjectposition = worldObj_09.func_645_a(vec3d, vec3d1);
        vec3d = Vec3D.createVector(posX, posY, posZ);
        vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        if(movingobjectposition != null)
        {
            vec3d1 = Vec3D.createVector(movingobjectposition.hitVec.xCoord_00, movingobjectposition.hitVec.yCoord_00, movingobjectposition.hitVec.zCoord_00);
        }
        Entity entity = null;
        List list = worldObj_09.getEntitiesWithinAABBExcludingEntity(this, boundingBox.addCoord(motionX, motionY, motionZ).expands(1.0D, 1.0D, 1.0D));
        double d = 0.0D;
        for(int j = 0; j < list.size(); j++)
        {
            Entity entity1 = (Entity)list.get(j);
            if(!entity1.func_401_c_() || entity1 == field_4147_j && field_4145_l < 25)
            {
                continue;
            }
            float f2 = 0.3F;
            AxisAlignedBB axisalignedbb = entity1.boundingBox.expands(f2, f2, f2);
            MovingObjectPosition movingobjectposition1 = axisalignedbb.func_1169_a(vec3d, vec3d1);
            if(movingobjectposition1 == null)
            {
                continue;
            }
            double d1 = vec3d.distanceTo_00(movingobjectposition1.hitVec);
            if(d1 < d || d == 0.0D)
            {
                entity = entity1;
                d = d1;
            }
        }

        if(entity != null)
        {
            movingobjectposition = new MovingObjectPosition(entity);
        }
        if(movingobjectposition != null)
        {
            if(movingobjectposition.entityHit != null)
            {
                if(!movingobjectposition.entityHit.attackEntity(field_4147_j, 0));
            }
            Explosion explosion = new Explosion();
            explosion.field_4267_a = true;
            explosion.func_901_a(worldObj_09, this, posX, posY, posZ, 1.0F);
            setEntityDead();
        }
        posX += motionX;
        posY += motionY;
        posZ += motionZ;
        float f = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
        rotationYaw = (float)((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
        for(rotationPitch = (float)((Math.atan2(motionY, f) * 180D) / 3.1415927410125732D); rotationPitch - field_602_at < -180F; field_602_at -= 360F) { }
        for(; rotationPitch - field_602_at >= 180F; field_602_at += 360F) { }
        for(; rotationYaw - field_603_as < -180F; field_603_as -= 360F) { }
        for(; rotationYaw - field_603_as >= 180F; field_603_as += 360F) { }
        rotationPitch = field_602_at + (rotationPitch - field_602_at) * 0.2F;
        rotationYaw = field_603_as + (rotationYaw - field_603_as) * 0.2F;
        float f1 = 0.95F;
        if(handleWaterMovement())
        {
            for(int k = 0; k < 4; k++)
            {
                float f3 = 0.25F;
                worldObj_09.spawnParticle_00("bubble", posX - motionX * (double)f3, posY - motionY * (double)f3, posZ - motionZ * (double)f3, motionX, motionY, motionZ);
            }

            f1 = 0.8F;
        }
        motionX += field_4155_b;
        motionY += field_4154_c;
        motionZ += field_4153_d;
        motionX *= f1;
        motionY *= f1;
        motionZ *= f1;
        worldObj_09.spawnParticle_00("smoke", posX, posY + 0.5D, posZ, 0.0D, 0.0D, 0.0D);
        setPosition(posX, posY, posZ);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setShort("xTile", (short)field_4152_e);
        nbttagcompound.setShort("yTile", (short)field_4151_f);
        nbttagcompound.setShort("zTile", (short)field_4150_g);
        nbttagcompound.setByte("inTile", (byte)field_4149_h);
        nbttagcompound.setByte("shake", (byte)field_4156_a);
        nbttagcompound.setByte("inGround", (byte)(field_4148_i ? 1 : 0));
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        field_4152_e = nbttagcompound.getShort("xTile");
        field_4151_f = nbttagcompound.getShort("yTile");
        field_4150_g = nbttagcompound.getShort("zTile");
        field_4149_h = nbttagcompound.getByte("inTile") & 0xff;
        field_4156_a = nbttagcompound.getByte("shake") & 0xff;
        field_4148_i = nbttagcompound.getByte("inGround") == 1;
    }

    public boolean func_401_c_()
    {
        return true;
    }

    public float func_4035_j_()
    {
        return 1.0F;
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(entity != null)
        {
            Vec3D vec3d = entity.func_4037_H();
            if(vec3d != null)
            {
                motionX = vec3d.xCoord_00;
                motionY = vec3d.yCoord_00;
                motionZ = vec3d.zCoord_00;
                field_4155_b = motionX * 0.10000000000000001D;
                field_4154_c = motionY * 0.10000000000000001D;
                field_4153_d = motionZ * 0.10000000000000001D;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public float func_392_h_()
    {
        return 0.0F;
    }

    private int field_4152_e;
    private int field_4151_f;
    private int field_4150_g;
    private int field_4149_h;
    private boolean field_4148_i;
    public int field_4156_a;
    private EntityLiving field_4147_j;
    private int field_6436_k;
    private int field_4145_l;
    public double field_4155_b;
    public double field_4154_c;
    public double field_4153_d;
}
