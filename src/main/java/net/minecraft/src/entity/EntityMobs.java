package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityCreature;
import net.minecraft.src.world.World;

public class EntityMobs extends EntityCreature
    implements IMobs
{

    public EntityMobs(World world)
    {
        super(world);
        field_762_e = 2;
        health_00 = 20;
    }

    public void onLivingUpdate()
    {
        float f = getEntityBrightness(1.0F);
        if(f > 0.5F)
        {
            field_701_U += 2;
        }
        super.onLivingUpdate();
    }

    public void onUpdate()
    {
        super.onUpdate();
        if(worldObj_09.field_1039_l == 0)
        {
            setEntityDead();
        }
    }

    protected Entity func_438_i()
    {
        EntityPlayer entityplayer = worldObj_09.getClosestPlayerToEntity(this, 16D);
        if(entityplayer != null && func_420_c(entityplayer))
        {
            return entityplayer;
        } else
        {
            return null;
        }
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(super.attackEntity(entity, i))
        {
            if(field_617_ae == entity || field_616_af == entity)
            {
                return true;
            }
            if(entity != this)
            {
                field_751_f = entity;
            }
            return true;
        } else
        {
            return false;
        }
    }

    protected void func_437_a(Entity entity, float f)
    {
        if((double)f < 2.5D && entity.boundingBox.maxY > boundingBox.minY && entity.boundingBox.minY < boundingBox.maxY)
        {
            attackTime = 20;
            entity.attackEntity(this, field_762_e);
        }
    }

    protected float func_439_a(int i, int j, int k)
    {
        return 0.5F - worldObj_09.getLightBrightness(i, j, k);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    public boolean getCanSpawnHere()
    {
        int i = MathHelper.convertToBlockCoord_00(posX);
        int j = MathHelper.convertToBlockCoord_00(boundingBox.minY);
        int k = MathHelper.convertToBlockCoord_00(posZ);
        if(worldObj_09.getSavedLightValue_00(EnumSkyBlock.Sky, i, j, k) > rand_05.nextInt(32))
        {
            return false;
        } else
        {
            int l = worldObj_09.getBlockLightValue_00(i, j, k);
            return l <= rand_05.nextInt(8) && super.getCanSpawnHere();
        }
    }

    protected int field_762_e;
}
