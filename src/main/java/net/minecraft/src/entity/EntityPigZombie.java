package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.world.World;

import java.util.List;

public class EntityPigZombie extends EntityZombie
{

    public EntityPigZombie(World world)
    {
        super(world);
        field_4117_a = 0;
        field_4116_b = 0;
        field_6409_z = "/mob/pigzombie.png";
        field_6401_al = 0.5F;
        field_762_e = 5;
        field_4079_be = true;
    }

    public void onUpdate()
    {
        field_6401_al = field_751_f == null ? 0.5F : 0.95F;
        if(field_4116_b > 0 && --field_4116_b == 0)
        {
            worldObj_09.playSoundAtEntity(this, "mob.zombiepig.zpigangry", func_6393_h() * 2.0F, ((rand_05.nextFloat() - rand_05.nextFloat()) * 0.2F + 1.0F) * 1.8F);
        }
        super.onUpdate();
    }

    public boolean getCanSpawnHere()
    {
        return worldObj_09.field_1039_l > 0 && worldObj_09.func_604_a(boundingBox) && worldObj_09.getCollidingBoundingBoxes_00(this, boundingBox).size() == 0 && !worldObj_09.getIsAnyLiquid(boundingBox);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setShort("Anger", (short)field_4117_a);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
        field_4117_a = nbttagcompound.getShort("Anger");
    }

    protected Entity func_438_i()
    {
        if(field_4117_a == 0)
        {
            return null;
        } else
        {
            return super.func_438_i();
        }
    }

    public void onLivingUpdate()
    {
        super.onLivingUpdate();
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(entity instanceof EntityPlayer)
        {
            List list = worldObj_09.getEntitiesWithinAABBExcludingEntity(this, boundingBox.expands(32D, 32D, 32D));
            for(int j = 0; j < list.size(); j++)
            {
                Entity entity1 = (Entity)list.get(j);
                if(entity1 instanceof EntityPigZombie)
                {
                    EntityPigZombie entitypigzombie = (EntityPigZombie)entity1;
                    entitypigzombie.func_4049_h(entity);
                }
            }

            func_4049_h(entity);
        }
        return super.attackEntity(entity, i);
    }

    private void func_4049_h(Entity entity)
    {
        field_751_f = entity;
        field_4117_a = 400 + rand_05.nextInt(400);
        field_4116_b = rand_05.nextInt(40);
    }

    protected String func_6389_d()
    {
        return "mob.zombiepig.zpig";
    }

    protected String func_6394_f_()
    {
        return "mob.zombiepig.zpighurt";
    }

    protected String func_6390_f()
    {
        return "mob.zombiepig.zpigdeath";
    }

    protected int getDropItemId()
    {
        return Item.field_4017_ap.swiftedIndex;
    }

    public ItemStack func_4045_l()
    {
        return field_4118_c;
    }

    private int field_4117_a;
    private int field_4116_b;
    private static final ItemStack field_4118_c;

    static 
    {
        field_4118_c = new ItemStack(Item.swordGold, 1);
    }
}
