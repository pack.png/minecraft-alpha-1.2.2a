package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.Block;
import net.minecraft.src.world.World;

import java.util.List;

public class EntityBoat extends Entity
{

    public EntityBoat(World world)
    {
        super(world);
        field_807_a = 0;
        field_806_b = 0;
        field_808_c = 1;
        field_618_ad = true;
        setSize(1.5F, 0.6F);
        yOffset_00 = height_01 / 2.0F;
        entityWalks = false;
    }

    public AxisAlignedBB func_383_b_(Entity entity)
    {
        return entity.boundingBox;
    }

    public AxisAlignedBB func_372_f_()
    {
        return boundingBox;
    }

    public boolean func_385_d_()
    {
        return true;
    }

    public EntityBoat(World world, double d, double d1, double d2)
    {
        this(world);
        setPosition(d, d1 + (double)yOffset_00, d2);
        motionX = 0.0D;
        motionY = 0.0D;
        motionZ = 0.0D;
        prevPosX = d;
        prevPosY = d1;
        prevPosZ = d2;
    }

    public double func_402_h()
    {
        return (double)height_01 * 0.0D - 0.30000001192092896D;
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(worldObj_09.multiplayerWorld)
        {
            return true;
        }
        field_808_c = -field_808_c;
        field_806_b = 10;
        field_807_a += i * 10;
        if(field_807_a > 40)
        {
            for(int j = 0; j < 3; j++)
            {
                dropItemWithOffset(Block.planks.blockID_00, 1, 0.0F);
            }

            for(int k = 0; k < 2; k++)
            {
                dropItemWithOffset(Item.stick.swiftedIndex, 1, 0.0F);
            }

            setEntityDead();
        }
        return true;
    }

    public boolean func_401_c_()
    {
        return !field_646_aA;
    }

    public void setPositionAndRotation2(double d, double d1, double d2, float f, 
            float f1, int i)
    {
        field_6434_e = d;
        field_6433_f = d1;
        field_6432_g = d2;
        field_6431_h = f;
        field_6430_i = f1;
        field_6435_d = i + 4;
        motionX = field_6429_j;
        motionY = field_6428_k;
        motionZ = field_6427_l;
    }

    public void setVelocity(double d, double d1, double d2)
    {
        field_6429_j = motionX = d;
        field_6428_k = motionY = d1;
        field_6427_l = motionZ = d2;
    }

    public void onUpdate()
    {
        super.onUpdate();
        if(field_806_b > 0)
        {
            field_806_b--;
        }
        if(field_807_a > 0)
        {
            field_807_a--;
        }
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        int i = 5;
        double d = 0.0D;
        for(int j = 0; j < i; j++)
        {
            double d4 = (boundingBox.minY + ((boundingBox.maxY - boundingBox.minY) * (double)(j + 0)) / (double)i) - 0.125D;
            double d8 = (boundingBox.minY + ((boundingBox.maxY - boundingBox.minY) * (double)(j + 1)) / (double)i) - 0.125D;
            AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBoxFromPool(boundingBox.minX_00, d4, boundingBox.minZ, boundingBox.maxX, d8, boundingBox.maxZ);
            if(worldObj_09.func_707_b(axisalignedbb, Material.water))
            {
                d += 1.0D / (double)i;
            }
        }

        if(worldObj_09.multiplayerWorld)
        {
            if(field_6435_d > 0)
            {
                double d1 = posX + (field_6434_e - posX) / (double)field_6435_d;
                double d5 = posY + (field_6433_f - posY) / (double)field_6435_d;
                double d9 = posZ + (field_6432_g - posZ) / (double)field_6435_d;
                double d12;
                for(d12 = field_6431_h - (double)rotationYaw; d12 < -180D; d12 += 360D) { }
                for(; d12 >= 180D; d12 -= 360D) { }
                rotationYaw += d12 / (double)field_6435_d;
                rotationPitch += (field_6430_i - (double)rotationPitch) / (double)field_6435_d;
                field_6435_d--;
                setPosition(d1, d5, d9);
                setRotation(rotationYaw, rotationPitch);
            } else
            {
                double d2 = posX + motionX;
                double d6 = posY + motionY;
                double d10 = posZ + motionZ;
                setPosition(d2, d6, d10);
                if(onGround_00)
                {
                    motionX *= 0.5D;
                    motionY *= 0.5D;
                    motionZ *= 0.5D;
                }
                motionX *= 0.99000000953674316D;
                motionY *= 0.94999998807907104D;
                motionZ *= 0.99000000953674316D;
            }
            return;
        }
        double d3 = d * 2D - 1.0D;
        motionY += 0.039999999105930328D * d3;
        if(field_617_ae != null)
        {
            motionX += field_617_ae.motionX * 0.20000000000000001D;
            motionZ += field_617_ae.motionZ * 0.20000000000000001D;
        }
        double d7 = 0.40000000000000002D;
        if(motionX < -d7)
        {
            motionX = -d7;
        }
        if(motionX > d7)
        {
            motionX = d7;
        }
        if(motionZ < -d7)
        {
            motionZ = -d7;
        }
        if(motionZ > d7)
        {
            motionZ = d7;
        }
        if(onGround_00)
        {
            motionX *= 0.5D;
            motionY *= 0.5D;
            motionZ *= 0.5D;
        }
        moveEntity(motionX, motionY, motionZ);
        double d11 = Math.sqrt(motionX * motionX + motionZ * motionZ);
        if(d11 > 0.14999999999999999D)
        {
            double d13 = Math.cos(((double)rotationYaw * 3.1415926535897931D) / 180D);
            double d15 = Math.sin(((double)rotationYaw * 3.1415926535897931D) / 180D);
            for(int i1 = 0; (double)i1 < 1.0D + d11 * 60D; i1++)
            {
                double d18 = rand_05.nextFloat() * 2.0F - 1.0F;
                double d20 = (double)(rand_05.nextInt(2) * 2 - 1) * 0.69999999999999996D;
                if(rand_05.nextBoolean())
                {
                    double d21 = (posX - d13 * d18 * 0.80000000000000004D) + d15 * d20;
                    double d23 = posZ - d15 * d18 * 0.80000000000000004D - d13 * d20;
                    worldObj_09.spawnParticle_00("splash", d21, posY - 0.125D, d23, motionX, motionY, motionZ);
                } else
                {
                    double d22 = posX + d13 + d15 * d18 * 0.69999999999999996D;
                    double d24 = (posZ + d15) - d13 * d18 * 0.69999999999999996D;
                    worldObj_09.spawnParticle_00("splash", d22, posY - 0.125D, d24, motionX, motionY, motionZ);
                }
            }

        }
        if(field_599_aw && d11 > 0.14999999999999999D)
        {
            if(!worldObj_09.multiplayerWorld)
            {
                setEntityDead();
                for(int k = 0; k < 3; k++)
                {
                    dropItemWithOffset(Block.planks.blockID_00, 1, 0.0F);
                }

                for(int l = 0; l < 2; l++)
                {
                    dropItemWithOffset(Item.stick.swiftedIndex, 1, 0.0F);
                }

            }
        } else
        {
            motionX *= 0.99000000953674316D;
            motionY *= 0.94999998807907104D;
            motionZ *= 0.99000000953674316D;
        }
        rotationPitch = 0.0F;
        double d14 = rotationYaw;
        double d16 = prevPosX - posX;
        double d17 = prevPosZ - posZ;
        if(d16 * d16 + d17 * d17 > 0.001D)
        {
            d14 = (float)((Math.atan2(d17, d16) * 180D) / 3.1415926535897931D);
        }
        double d19;
        for(d19 = d14 - (double)rotationYaw; d19 >= 180D; d19 -= 360D) { }
        for(; d19 < -180D; d19 += 360D) { }
        if(d19 > 20D)
        {
            d19 = 20D;
        }
        if(d19 < -20D)
        {
            d19 = -20D;
        }
        rotationYaw += d19;
        setRotation(rotationYaw, rotationPitch);
        List list = worldObj_09.getEntitiesWithinAABBExcludingEntity(this, boundingBox.expands(0.20000000298023224D, 0.0D, 0.20000000298023224D));
        if(list != null && list.size() > 0)
        {
            for(int j1 = 0; j1 < list.size(); j1++)
            {
                Entity entity = (Entity)list.get(j1);
                if(entity != field_617_ae && entity.func_385_d_() && (entity instanceof EntityBoat))
                {
                    entity.applyEntityCollision(this);
                }
            }

        }
        if(field_617_ae != null && field_617_ae.field_646_aA)
        {
            field_617_ae = null;
        }
    }

    public void func_366_i_()
    {
        double d = Math.cos(((double)rotationYaw * 3.1415926535897931D) / 180D) * 0.40000000000000002D;
        double d1 = Math.sin(((double)rotationYaw * 3.1415926535897931D) / 180D) * 0.40000000000000002D;
        field_617_ae.setPosition(posX + d, posY + func_402_h() + field_617_ae.func_388_v(), posZ + d1);
    }

    protected void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
    }

    protected void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
    }

    public float func_392_h_()
    {
        return 0.0F;
    }

    public boolean interact(EntityPlayer entityplayer)
    {
        if(field_617_ae != null && (field_617_ae instanceof EntityPlayer) && field_617_ae != entityplayer)
        {
            return true;
        }
        if(!worldObj_09.multiplayerWorld)
        {
            entityplayer.func_6377_h(this);
        }
        return true;
    }

    public int field_807_a;
    public int field_806_b;
    public int field_808_c;
    private int field_6435_d;
    private double field_6434_e;
    private double field_6433_f;
    private double field_6432_g;
    private double field_6431_h;
    private double field_6430_i;
    private double field_6429_j;
    private double field_6428_k;
    private double field_6427_l;
}
