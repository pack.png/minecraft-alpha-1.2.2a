package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.Item;
import net.minecraft.src.MathHelper;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.world.World;

public class EntitySpider extends EntityMobs
{

    public EntitySpider(World world)
    {
        super(world);
        field_6409_z = "/mob/spider.png";
        setSize(1.4F, 0.9F);
        field_6401_al = 0.8F;
    }

    public double func_402_h()
    {
        return (double)height_01 * 0.75D - 0.5D;
    }

    protected Entity func_438_i()
    {
        float f = getEntityBrightness(1.0F);
        if(f < 0.5F)
        {
            double d = 16D;
            return worldObj_09.getClosestPlayerToEntity(this, d);
        } else
        {
            return null;
        }
    }

    protected String func_6389_d()
    {
        return "mob.spider";
    }

    protected String func_6394_f_()
    {
        return "mob.spider";
    }

    protected String func_6390_f()
    {
        return "mob.spiderdeath";
    }

    protected void func_437_a(Entity entity, float f)
    {
        float f1 = getEntityBrightness(1.0F);
        if(f1 > 0.5F && rand_05.nextInt(100) == 0)
        {
            field_751_f = null;
            return;
        }
        if(f > 2.0F && f < 6F && rand_05.nextInt(10) == 0)
        {
            if(onGround_00)
            {
                double d = entity.posX - posX;
                double d1 = entity.posZ - posZ;
                float f2 = MathHelper.sqrt_double(d * d + d1 * d1);
                motionX = (d / (double)f2) * 0.5D * 0.80000001192092896D + motionX * 0.20000000298023224D;
                motionZ = (d1 / (double)f2) * 0.5D * 0.80000001192092896D + motionZ * 0.20000000298023224D;
                motionY = 0.40000000596046448D;
            }
        } else
        {
            super.func_437_a(entity, f);
        }
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
    }

    protected int getDropItemId()
    {
        return Item.silk.swiftedIndex;
    }
}
