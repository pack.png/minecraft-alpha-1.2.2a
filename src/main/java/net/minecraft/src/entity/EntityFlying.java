package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.MathHelper;
import net.minecraft.src.Block;
import net.minecraft.src.world.World;

public class EntityFlying extends EntityLiving
{

    public EntityFlying(World world)
    {
        super(world);
    }

    protected void fall(float f)
    {
    }

    public void func_435_b(float f, float f1)
    {
        if(handleWaterMovement())
        {
            func_351_a(f, f1, 0.02F);
            moveEntity(motionX, motionY, motionZ);
            motionX *= 0.80000001192092896D;
            motionY *= 0.80000001192092896D;
            motionZ *= 0.80000001192092896D;
        } else
        if(func_359_G())
        {
            func_351_a(f, f1, 0.02F);
            moveEntity(motionX, motionY, motionZ);
            motionX *= 0.5D;
            motionY *= 0.5D;
            motionZ *= 0.5D;
        } else
        {
            float f2 = 0.91F;
            if(onGround_00)
            {
                f2 = 0.5460001F;
                int i = worldObj_09.getBlockId(MathHelper.convertToBlockCoord_00(posX), MathHelper.convertToBlockCoord_00(boundingBox.minY) - 1, MathHelper.convertToBlockCoord_00(posZ));
                if(i > 0)
                {
                    f2 = Block.blocksList[i].slipperiness * 0.91F;
                }
            }
            float f3 = 0.1627714F / (f2 * f2 * f2);
            func_351_a(f, f1, onGround_00 ? 0.1F * f3 : 0.02F);
            f2 = 0.91F;
            if(onGround_00)
            {
                f2 = 0.5460001F;
                int j = worldObj_09.getBlockId(MathHelper.convertToBlockCoord_00(posX), MathHelper.convertToBlockCoord_00(boundingBox.minY) - 1, MathHelper.convertToBlockCoord_00(posZ));
                if(j > 0)
                {
                    f2 = Block.blocksList[j].slipperiness * 0.91F;
                }
            }
            moveEntity(motionX, motionY, motionZ);
            motionX *= f2;
            motionY *= f2;
            motionZ *= f2;
        }
        field_705_Q = field_704_R;
        double d = posX - prevPosX;
        double d1 = posZ - prevPosZ;
        float f4 = MathHelper.sqrt_double(d * d + d1 * d1) * 4F;
        if(f4 > 1.0F)
        {
            f4 = 1.0F;
        }
        field_704_R += (f4 - field_704_R) * 0.4F;
        field_703_S += field_704_R;
    }

    public boolean func_429_A()
    {
        return false;
    }
}
