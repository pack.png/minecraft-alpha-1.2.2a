package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityLiving;
import net.minecraft.src.world.World;

import java.util.List;

public class EntitySnowball extends Entity
{

    public EntitySnowball(World world)
    {
        super(world);
        field_816_b = -1;
        field_815_c = -1;
        field_814_d = -1;
        field_813_e = 0;
        field_812_f = false;
        field_817_a = 0;
        field_809_i = 0;
        setSize(0.25F, 0.25F);
    }

    public boolean func_384_a(double d)
    {
        double d1 = boundingBox.getAverageEdgeLength() * 4D;
        d1 *= 64D;
        return d < d1 * d1;
    }

    public EntitySnowball(World world, EntityLiving entityliving)
    {
        super(world);
        field_816_b = -1;
        field_815_c = -1;
        field_814_d = -1;
        field_813_e = 0;
        field_812_f = false;
        field_817_a = 0;
        field_809_i = 0;
        field_811_g = entityliving;
        setSize(0.25F, 0.25F);
        func_365_c(entityliving.posX, entityliving.posY, entityliving.posZ, entityliving.rotationYaw, entityliving.rotationPitch);
        posX -= MathHelper.cos((rotationYaw / 180F) * 3.141593F) * 0.16F;
        posY -= 0.10000000149011612D;
        posZ -= MathHelper.sin((rotationYaw / 180F) * 3.141593F) * 0.16F;
        setPosition(posX, posY, posZ);
        yOffset_00 = 0.0F;
        float f = 0.4F;
        motionX = -MathHelper.sin((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F) * f;
        motionZ = MathHelper.cos((rotationYaw / 180F) * 3.141593F) * MathHelper.cos((rotationPitch / 180F) * 3.141593F) * f;
        motionY = -MathHelper.sin((rotationPitch / 180F) * 3.141593F) * f;
        func_467_a(motionX, motionY, motionZ, 1.5F, 1.0F);
    }

    public void func_467_a(double d, double d1, double d2, float f, 
            float f1)
    {
        float f2 = MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2);
        d /= f2;
        d1 /= f2;
        d2 /= f2;
        d += rand_05.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d1 += rand_05.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d2 += rand_05.nextGaussian() * 0.0074999998323619366D * (double)f1;
        d *= f;
        d1 *= f;
        d2 *= f;
        motionX = d;
        motionY = d1;
        motionZ = d2;
        float f3 = MathHelper.sqrt_double(d * d + d2 * d2);
        field_603_as = rotationYaw = (float)((Math.atan2(d, d2) * 180D) / 3.1415927410125732D);
        field_602_at = rotationPitch = (float)((Math.atan2(d1, f3) * 180D) / 3.1415927410125732D);
        field_810_h = 0;
    }

    public void onUpdate()
    {
        super.onUpdate();
        if(field_817_a > 0)
        {
            field_817_a--;
        }
        if(field_812_f)
        {
            int i = worldObj_09.getBlockId(field_816_b, field_815_c, field_814_d);
            if(i != field_813_e)
            {
                field_812_f = false;
                motionX *= rand_05.nextFloat() * 0.2F;
                motionY *= rand_05.nextFloat() * 0.2F;
                motionZ *= rand_05.nextFloat() * 0.2F;
                field_810_h = 0;
                field_809_i = 0;
            } else
            {
                field_810_h++;
                if(field_810_h == 1200)
                {
                    setEntityDead();
                }
                return;
            }
        } else
        {
            field_809_i++;
        }
        Vec3D vec3d = Vec3D.createVector(posX, posY, posZ);
        Vec3D vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        MovingObjectPosition movingobjectposition = worldObj_09.func_645_a(vec3d, vec3d1);
        vec3d = Vec3D.createVector(posX, posY, posZ);
        vec3d1 = Vec3D.createVector(posX + motionX, posY + motionY, posZ + motionZ);
        if(movingobjectposition != null)
        {
            vec3d1 = Vec3D.createVector(movingobjectposition.hitVec.xCoord_00, movingobjectposition.hitVec.yCoord_00, movingobjectposition.hitVec.zCoord_00);
        }
        Entity entity = null;
        List list = worldObj_09.getEntitiesWithinAABBExcludingEntity(this, boundingBox.addCoord(motionX, motionY, motionZ).expands(1.0D, 1.0D, 1.0D));
        double d = 0.0D;
        for(int j = 0; j < list.size(); j++)
        {
            Entity entity1 = (Entity)list.get(j);
            if(!entity1.func_401_c_() || entity1 == field_811_g && field_809_i < 5)
            {
                continue;
            }
            float f2 = 0.3F;
            AxisAlignedBB axisalignedbb = entity1.boundingBox.expands(f2, f2, f2);
            MovingObjectPosition movingobjectposition1 = axisalignedbb.func_1169_a(vec3d, vec3d1);
            if(movingobjectposition1 == null)
            {
                continue;
            }
            double d1 = vec3d.distanceTo_00(movingobjectposition1.hitVec);
            if(d1 < d || d == 0.0D)
            {
                entity = entity1;
                d = d1;
            }
        }

        if(entity != null)
        {
            movingobjectposition = new MovingObjectPosition(entity);
        }
        if(movingobjectposition != null)
        {
            if(movingobjectposition.entityHit != null)
            {
                if(!movingobjectposition.entityHit.attackEntity(field_811_g, 0));
            }
            for(int k = 0; k < 8; k++)
            {
                worldObj_09.spawnParticle_00("snowballpoof", posX, posY, posZ, 0.0D, 0.0D, 0.0D);
            }

            setEntityDead();
        }
        posX += motionX;
        posY += motionY;
        posZ += motionZ;
        float f = MathHelper.sqrt_double(motionX * motionX + motionZ * motionZ);
        rotationYaw = (float)((Math.atan2(motionX, motionZ) * 180D) / 3.1415927410125732D);
        for(rotationPitch = (float)((Math.atan2(motionY, f) * 180D) / 3.1415927410125732D); rotationPitch - field_602_at < -180F; field_602_at -= 360F) { }
        for(; rotationPitch - field_602_at >= 180F; field_602_at += 360F) { }
        for(; rotationYaw - field_603_as < -180F; field_603_as -= 360F) { }
        for(; rotationYaw - field_603_as >= 180F; field_603_as += 360F) { }
        rotationPitch = field_602_at + (rotationPitch - field_602_at) * 0.2F;
        rotationYaw = field_603_as + (rotationYaw - field_603_as) * 0.2F;
        float f1 = 0.99F;
        float f3 = 0.03F;
        if(handleWaterMovement())
        {
            for(int l = 0; l < 4; l++)
            {
                float f4 = 0.25F;
                worldObj_09.spawnParticle_00("bubble", posX - motionX * (double)f4, posY - motionY * (double)f4, posZ - motionZ * (double)f4, motionX, motionY, motionZ);
            }

            f1 = 0.8F;
        }
        motionX *= f1;
        motionY *= f1;
        motionZ *= f1;
        motionY -= f3;
        setPosition(posX, posY, posZ);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setShort("xTile", (short)field_816_b);
        nbttagcompound.setShort("yTile", (short)field_815_c);
        nbttagcompound.setShort("zTile", (short)field_814_d);
        nbttagcompound.setByte("inTile", (byte)field_813_e);
        nbttagcompound.setByte("shake", (byte)field_817_a);
        nbttagcompound.setByte("inGround", (byte)(field_812_f ? 1 : 0));
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        field_816_b = nbttagcompound.getShort("xTile");
        field_815_c = nbttagcompound.getShort("yTile");
        field_814_d = nbttagcompound.getShort("zTile");
        field_813_e = nbttagcompound.getByte("inTile") & 0xff;
        field_817_a = nbttagcompound.getByte("shake") & 0xff;
        field_812_f = nbttagcompound.getByte("inGround") == 1;
    }

    public void onCollideWithPlayer(EntityPlayer entityplayer)
    {
        if(field_812_f && field_811_g == entityplayer && field_817_a <= 0 && entityplayer.inventory.addItemStackToInventory(new ItemStack(Item.arrow.swiftedIndex, 1)))
        {
            worldObj_09.playSoundAtEntity(this, "random.pop", 0.2F, ((rand_05.nextFloat() - rand_05.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            entityplayer.func_443_a_(this, 1);
            setEntityDead();
        }
    }

    public float func_392_h_()
    {
        return 0.0F;
    }

    private int field_816_b;
    private int field_815_c;
    private int field_814_d;
    private int field_813_e;
    private boolean field_812_f;
    public int field_817_a;
    private EntityLiving field_811_g;
    private int field_810_h;
    private int field_809_i;
}
