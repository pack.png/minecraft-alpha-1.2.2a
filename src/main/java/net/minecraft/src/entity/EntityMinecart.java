package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.Block;
import net.minecraft.src.world.World;

import java.util.List;

public class EntityMinecart extends Entity
    implements IInventory
{

    public EntityMinecart(World world)
    {
        super(world);
        cargoItems = new ItemStack[36];
        field_864_a = 0;
        field_863_b = 0;
        field_862_c = 1;
        field_856_i = false;
        field_618_ad = true;
        setSize(0.98F, 0.7F);
        yOffset_00 = height_01 / 2.0F;
        entityWalks = false;
    }

    public AxisAlignedBB func_383_b_(Entity entity)
    {
        return entity.boundingBox;
    }

    public AxisAlignedBB func_372_f_()
    {
        return null;
    }

    public boolean func_385_d_()
    {
        return true;
    }

    public EntityMinecart(World world, double d, double d1, double d2, 
            int i)
    {
        this(world);
        setPosition(d, d1 + (double)yOffset_00, d2);
        motionX = 0.0D;
        motionY = 0.0D;
        motionZ = 0.0D;
        prevPosX = d;
        prevPosY = d1;
        prevPosZ = d2;
        minecartType = i;
    }

    public double func_402_h()
    {
        return (double)height_01 * 0.0D - 0.30000001192092896D;
    }

    public boolean attackEntity(Entity entity, int i)
    {
        if(worldObj_09.multiplayerWorld)
        {
            return true;
        }
        field_862_c = -field_862_c;
        field_863_b = 10;
        field_864_a += i * 10;
        if(field_864_a > 40)
        {
            dropItemWithOffset(Item.minecartEmpty.swiftedIndex, 1, 0.0F);
            if(minecartType == 1)
            {
                dropItemWithOffset(Block.crate.blockID_00, 1, 0.0F);
            } else
            if(minecartType == 2)
            {
                dropItemWithOffset(Block.stoneOvenIdle.blockID_00, 1, 0.0F);
            }
            setEntityDead();
        }
        return true;
    }

    public boolean func_401_c_()
    {
        return !field_646_aA;
    }

    public void setEntityDead()
    {
label0:
        for(int i = 0; i < getSizeInventory(); i++)
        {
            ItemStack itemstack = getStackInSlot(i);
            if(itemstack == null)
            {
                continue;
            }
            float f = rand_05.nextFloat() * 0.8F + 0.1F;
            float f1 = rand_05.nextFloat() * 0.8F + 0.1F;
            float f2 = rand_05.nextFloat() * 0.8F + 0.1F;
            do
            {
                if(itemstack.stackSize <= 0)
                {
                    continue label0;
                }
                int j = rand_05.nextInt(21) + 10;
                if(j > itemstack.stackSize)
                {
                    j = itemstack.stackSize;
                }
                itemstack.stackSize -= j;
                EntityItem entityitem = new EntityItem(worldObj_09, posX + (double)f, posY + (double)f1, posZ + (double)f2, new ItemStack(itemstack.itemID, j, itemstack.itemDamage));
                float f3 = 0.05F;
                entityitem.motionX = (float)rand_05.nextGaussian() * f3;
                entityitem.motionY = (float)rand_05.nextGaussian() * f3 + 0.2F;
                entityitem.motionZ = (float)rand_05.nextGaussian() * f3;
                worldObj_09.entityJoinedWorld(entityitem);
            } while(true);
        }

        super.setEntityDead();
    }

    public void onUpdate()
    {
        if(worldObj_09.multiplayerWorld && field_6445_k > 0)
        {
            if(field_6445_k > 0)
            {
                double d = posX + (field_6444_l - posX) / (double)field_6445_k;
                double d1 = posY + (field_6443_m - posY) / (double)field_6445_k;
                double d3 = posZ + (field_6442_n - posZ) / (double)field_6445_k;
                double d4;
                for(d4 = field_6441_o - (double)rotationYaw; d4 < -180D; d4 += 360D) { }
                for(; d4 >= 180D; d4 -= 360D) { }
                rotationYaw += d4 / (double)field_6445_k;
                rotationPitch += (field_6440_p - (double)rotationPitch) / (double)field_6445_k;
                field_6445_k--;
                setPosition(d, d1, d3);
                setRotation(rotationYaw, rotationPitch);
            } else
            {
                setPosition(posX, posY, posZ);
                setRotation(rotationYaw, rotationPitch);
            }
            return;
        }
        if(field_863_b > 0)
        {
            field_863_b--;
        }
        if(field_864_a > 0)
        {
            field_864_a--;
        }
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        motionY -= 0.039999999105930328D;
        int i = MathHelper.convertToBlockCoord_00(posX);
        int j = MathHelper.convertToBlockCoord_00(posY);
        int k = MathHelper.convertToBlockCoord_00(posZ);
        if(worldObj_09.getBlockId(i, j - 1, k) == Block.minecartTrack.blockID_00)
        {
            j--;
        }
        double d2 = 0.40000000000000002D;
        boolean flag = false;
        double d5 = 0.0078125D;
        if(worldObj_09.getBlockId(i, j, k) == Block.minecartTrack.blockID_00)
        {
            Vec3D vec3d = func_514_g(posX, posY, posZ);
            int l = worldObj_09.getBlockMetadata_01(i, j, k);
            posY = j;
            if(l >= 2 && l <= 5)
            {
                posY = j + 1;
            }
            if(l == 2)
            {
                motionX -= d5;
            }
            if(l == 3)
            {
                motionX += d5;
            }
            if(l == 4)
            {
                motionZ += d5;
            }
            if(l == 5)
            {
                motionZ -= d5;
            }
            int ai[][] = field_855_j[l];
            double d8 = ai[1][0] - ai[0][0];
            double d10 = ai[1][2] - ai[0][2];
            double d11 = Math.sqrt(d8 * d8 + d10 * d10);
            double d12 = motionX * d8 + motionZ * d10;
            if(d12 < 0.0D)
            {
                d8 = -d8;
                d10 = -d10;
            }
            double d13 = Math.sqrt(motionX * motionX + motionZ * motionZ);
            motionX = (d13 * d8) / d11;
            motionZ = (d13 * d10) / d11;
            double d16 = 0.0D;
            double d17 = (double)i + 0.5D + (double)ai[0][0] * 0.5D;
            double d18 = (double)k + 0.5D + (double)ai[0][2] * 0.5D;
            double d19 = (double)i + 0.5D + (double)ai[1][0] * 0.5D;
            double d20 = (double)k + 0.5D + (double)ai[1][2] * 0.5D;
            d8 = d19 - d17;
            d10 = d20 - d18;
            if(d8 == 0.0D)
            {
                posX = (double)i + 0.5D;
                d16 = posZ - (double)k;
            } else
            if(d10 == 0.0D)
            {
                posZ = (double)k + 0.5D;
                d16 = posX - (double)i;
            } else
            {
                double d21 = posX - d17;
                double d23 = posZ - d18;
                double d25 = (d21 * d8 + d23 * d10) * 2D;
                d16 = d25;
            }
            posX = d17 + d8 * d16;
            posZ = d18 + d10 * d16;
            setPosition(posX, posY + (double)yOffset_00, posZ);
            double d22 = motionX;
            double d24 = motionZ;
            if(field_617_ae != null)
            {
                d22 *= 0.75D;
                d24 *= 0.75D;
            }
            if(d22 < -d2)
            {
                d22 = -d2;
            }
            if(d22 > d2)
            {
                d22 = d2;
            }
            if(d24 < -d2)
            {
                d24 = -d2;
            }
            if(d24 > d2)
            {
                d24 = d2;
            }
            moveEntity(d22, 0.0D, d24);
            if(ai[0][1] != 0 && MathHelper.convertToBlockCoord_00(posX) - i == ai[0][0] && MathHelper.convertToBlockCoord_00(posZ) - k == ai[0][2])
            {
                setPosition(posX, posY + (double)ai[0][1], posZ);
            } else
            if(ai[1][1] != 0 && MathHelper.convertToBlockCoord_00(posX) - i == ai[1][0] && MathHelper.convertToBlockCoord_00(posZ) - k == ai[1][2])
            {
                setPosition(posX, posY + (double)ai[1][1], posZ);
            }
            if(field_617_ae != null)
            {
                motionX *= 0.99699997901916504D;
                motionY *= 0.0D;
                motionZ *= 0.99699997901916504D;
            } else
            {
                if(minecartType == 2)
                {
                    double d26 = MathHelper.sqrt_double(pushX * pushX + pushZ * pushZ);
                    if(d26 > 0.01D)
                    {
                        flag = true;
                        pushX /= d26;
                        pushZ /= d26;
                        double d28 = 0.040000000000000001D;
                        motionX *= 0.80000001192092896D;
                        motionY *= 0.0D;
                        motionZ *= 0.80000001192092896D;
                        motionX += pushX * d28;
                        motionZ += pushZ * d28;
                    } else
                    {
                        motionX *= 0.89999997615814209D;
                        motionY *= 0.0D;
                        motionZ *= 0.89999997615814209D;
                    }
                }
                motionX *= 0.95999997854232788D;
                motionY *= 0.0D;
                motionZ *= 0.95999997854232788D;
            }
            Vec3D vec3d1 = func_514_g(posX, posY, posZ);
            if(vec3d1 != null && vec3d != null)
            {
                double d27 = (vec3d.yCoord_00 - vec3d1.yCoord_00) * 0.050000000000000003D;
                double d14 = Math.sqrt(motionX * motionX + motionZ * motionZ);
                if(d14 > 0.0D)
                {
                    motionX = (motionX / d14) * (d14 + d27);
                    motionZ = (motionZ / d14) * (d14 + d27);
                }
                setPosition(posX, vec3d1.yCoord_00, posZ);
            }
            int j1 = MathHelper.convertToBlockCoord_00(posX);
            int k1 = MathHelper.convertToBlockCoord_00(posZ);
            if(j1 != i || k1 != k)
            {
                double d15 = Math.sqrt(motionX * motionX + motionZ * motionZ);
                motionX = d15 * (double)(j1 - i);
                motionZ = d15 * (double)(k1 - k);
            }
            if(minecartType == 2)
            {
                double d29 = MathHelper.sqrt_double(pushX * pushX + pushZ * pushZ);
                if(d29 > 0.01D && motionX * motionX + motionZ * motionZ > 0.001D)
                {
                    pushX /= d29;
                    pushZ /= d29;
                    if(pushX * motionX + pushZ * motionZ < 0.0D)
                    {
                        pushX = 0.0D;
                        pushZ = 0.0D;
                    } else
                    {
                        pushX = motionX;
                        pushZ = motionZ;
                    }
                }
            }
        } else
        {
            if(motionX < -d2)
            {
                motionX = -d2;
            }
            if(motionX > d2)
            {
                motionX = d2;
            }
            if(motionZ < -d2)
            {
                motionZ = -d2;
            }
            if(motionZ > d2)
            {
                motionZ = d2;
            }
            if(onGround_00)
            {
                motionX *= 0.5D;
                motionY *= 0.5D;
                motionZ *= 0.5D;
            }
            moveEntity(motionX, motionY, motionZ);
            if(!onGround_00)
            {
                motionX *= 0.94999998807907104D;
                motionY *= 0.94999998807907104D;
                motionZ *= 0.94999998807907104D;
            }
        }
        rotationPitch = 0.0F;
        double d6 = prevPosX - posX;
        double d7 = prevPosZ - posZ;
        if(d6 * d6 + d7 * d7 > 0.001D)
        {
            rotationYaw = (float)((Math.atan2(d7, d6) * 180D) / 3.1415926535897931D);
            if(field_856_i)
            {
                rotationYaw += 180F;
            }
        }
        double d9;
        for(d9 = rotationYaw - field_603_as; d9 >= 180D; d9 -= 360D) { }
        for(; d9 < -180D; d9 += 360D) { }
        if(d9 < -170D || d9 >= 170D)
        {
            rotationYaw += 180F;
            field_856_i = !field_856_i;
        }
        setRotation(rotationYaw, rotationPitch);
        List list = worldObj_09.getEntitiesWithinAABBExcludingEntity(this, boundingBox.expands(0.20000000298023224D, 0.0D, 0.20000000298023224D));
        if(list != null && list.size() > 0)
        {
            for(int i1 = 0; i1 < list.size(); i1++)
            {
                Entity entity = (Entity)list.get(i1);
                if(entity != field_617_ae && entity.func_385_d_() && (entity instanceof EntityMinecart))
                {
                    entity.applyEntityCollision(this);
                }
            }

        }
        if(field_617_ae != null && field_617_ae.field_646_aA)
        {
            field_617_ae = null;
        }
        if(flag && rand_05.nextInt(4) == 0)
        {
            fuel--;
            if(fuel < 0)
            {
                pushX = pushZ = 0.0D;
            }
            worldObj_09.spawnParticle_00("largesmoke", posX, posY + 0.80000000000000004D, posZ, 0.0D, 0.0D, 0.0D);
        }
    }

    public Vec3D func_515_a(double d, double d1, double d2, double d3)
    {
        int i = MathHelper.convertToBlockCoord_00(d);
        int j = MathHelper.convertToBlockCoord_00(d1);
        int k = MathHelper.convertToBlockCoord_00(d2);
        if(worldObj_09.getBlockId(i, j - 1, k) == Block.minecartTrack.blockID_00)
        {
            j--;
        }
        if(worldObj_09.getBlockId(i, j, k) == Block.minecartTrack.blockID_00)
        {
            int l = worldObj_09.getBlockMetadata_01(i, j, k);
            d1 = j;
            if(l >= 2 && l <= 5)
            {
                d1 = j + 1;
            }
            int ai[][] = field_855_j[l];
            double d4 = ai[1][0] - ai[0][0];
            double d5 = ai[1][2] - ai[0][2];
            double d6 = Math.sqrt(d4 * d4 + d5 * d5);
            d4 /= d6;
            d5 /= d6;
            d += d4 * d3;
            d2 += d5 * d3;
            if(ai[0][1] != 0 && MathHelper.convertToBlockCoord_00(d) - i == ai[0][0] && MathHelper.convertToBlockCoord_00(d2) - k == ai[0][2])
            {
                d1 += ai[0][1];
            } else
            if(ai[1][1] != 0 && MathHelper.convertToBlockCoord_00(d) - i == ai[1][0] && MathHelper.convertToBlockCoord_00(d2) - k == ai[1][2])
            {
                d1 += ai[1][1];
            }
            return func_514_g(d, d1, d2);
        } else
        {
            return null;
        }
    }

    public Vec3D func_514_g(double d, double d1, double d2)
    {
        int i = MathHelper.convertToBlockCoord_00(d);
        int j = MathHelper.convertToBlockCoord_00(d1);
        int k = MathHelper.convertToBlockCoord_00(d2);
        if(worldObj_09.getBlockId(i, j - 1, k) == Block.minecartTrack.blockID_00)
        {
            j--;
        }
        if(worldObj_09.getBlockId(i, j, k) == Block.minecartTrack.blockID_00)
        {
            int l = worldObj_09.getBlockMetadata_01(i, j, k);
            d1 = j;
            if(l >= 2 && l <= 5)
            {
                d1 = j + 1;
            }
            int ai[][] = field_855_j[l];
            double d3 = 0.0D;
            double d4 = (double)i + 0.5D + (double)ai[0][0] * 0.5D;
            double d5 = (double)j + 0.5D + (double)ai[0][1] * 0.5D;
            double d6 = (double)k + 0.5D + (double)ai[0][2] * 0.5D;
            double d7 = (double)i + 0.5D + (double)ai[1][0] * 0.5D;
            double d8 = (double)j + 0.5D + (double)ai[1][1] * 0.5D;
            double d9 = (double)k + 0.5D + (double)ai[1][2] * 0.5D;
            double d10 = d7 - d4;
            double d11 = (d8 - d5) * 2D;
            double d12 = d9 - d6;
            if(d10 == 0.0D)
            {
                d = (double)i + 0.5D;
                d3 = d2 - (double)k;
            } else
            if(d12 == 0.0D)
            {
                d2 = (double)k + 0.5D;
                d3 = d - (double)i;
            } else
            {
                double d13 = d - d4;
                double d14 = d2 - d6;
                double d15 = (d13 * d10 + d14 * d12) * 2D;
                d3 = d15;
            }
            d = d4 + d10 * d3;
            d1 = d5 + d11 * d3;
            d2 = d6 + d12 * d3;
            if(d11 < 0.0D)
            {
                d1++;
            }
            if(d11 > 0.0D)
            {
                d1 += 0.5D;
            }
            return Vec3D.createVector(d, d1, d2);
        } else
        {
            return null;
        }
    }

    protected void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setInteger("Type", minecartType);
        if(minecartType == 2)
        {
            nbttagcompound.setDouble("PushX", pushX);
            nbttagcompound.setDouble("PushZ", pushZ);
            nbttagcompound.setShort("Fuel", (short)fuel);
        } else
        if(minecartType == 1)
        {
            NBTTagList nbttaglist = new NBTTagList();
            for(int i = 0; i < cargoItems.length; i++)
            {
                if(cargoItems[i] != null)
                {
                    NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                    nbttagcompound1.setByte("Slot", (byte)i);
                    cargoItems[i].writeToNBT_02(nbttagcompound1);
                    nbttaglist.setTag_00(nbttagcompound1);
                }
            }

            nbttagcompound.setTag("Items", nbttaglist);
        }
    }

    protected void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        minecartType = nbttagcompound.getInteger("Type");
        if(minecartType == 2)
        {
            pushX = nbttagcompound.getDouble("PushX");
            pushZ = nbttagcompound.getDouble("PushZ");
            fuel = nbttagcompound.getShort("Fuel");
        } else
        if(minecartType == 1)
        {
            NBTTagList nbttaglist = nbttagcompound.getTagList("Items");
            cargoItems = new ItemStack[getSizeInventory()];
            for(int i = 0; i < nbttaglist.tagCount(); i++)
            {
                NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
                int j = nbttagcompound1.getByte("Slot") & 0xff;
                if(j >= 0 && j < cargoItems.length)
                {
                    cargoItems[j] = new ItemStack(nbttagcompound1);
                }
            }

        }
    }

    public float func_392_h_()
    {
        return 0.0F;
    }

    public void applyEntityCollision(Entity entity)
    {
        if(worldObj_09.multiplayerWorld)
        {
            return;
        }
        if(entity == field_617_ae)
        {
            return;
        }
        if((entity instanceof EntityLiving) && !(entity instanceof EntityPlayer) && minecartType == 0 && motionX * motionX + motionZ * motionZ > 0.01D && field_617_ae == null && entity.field_616_af == null)
        {
            entity.func_6377_h(this);
        }
        double d = entity.posX - posX;
        double d1 = entity.posZ - posZ;
        double d2 = d * d + d1 * d1;
        if(d2 >= 9.9999997473787516E-005D)
        {
            d2 = MathHelper.sqrt_double(d2);
            d /= d2;
            d1 /= d2;
            double d3 = 1.0D / d2;
            if(d3 > 1.0D)
            {
                d3 = 1.0D;
            }
            d *= d3;
            d1 *= d3;
            d *= 0.10000000149011612D;
            d1 *= 0.10000000149011612D;
            d *= 1.0F - field_632_aO;
            d1 *= 1.0F - field_632_aO;
            d *= 0.5D;
            d1 *= 0.5D;
            if(entity instanceof EntityMinecart)
            {
                double d4 = entity.motionX + motionX;
                double d5 = entity.motionZ + motionZ;
                if(((EntityMinecart)entity).minecartType == 2 && minecartType != 2)
                {
                    motionX *= 0.20000000298023224D;
                    motionZ *= 0.20000000298023224D;
                    addVelocity(entity.motionX - d, 0.0D, entity.motionZ - d1);
                    entity.motionX *= 0.69999998807907104D;
                    entity.motionZ *= 0.69999998807907104D;
                } else
                if(((EntityMinecart)entity).minecartType != 2 && minecartType == 2)
                {
                    entity.motionX *= 0.20000000298023224D;
                    entity.motionZ *= 0.20000000298023224D;
                    entity.addVelocity(motionX + d, 0.0D, motionZ + d1);
                    motionX *= 0.69999998807907104D;
                    motionZ *= 0.69999998807907104D;
                } else
                {
                    d4 /= 2D;
                    d5 /= 2D;
                    motionX *= 0.20000000298023224D;
                    motionZ *= 0.20000000298023224D;
                    addVelocity(d4 - d, 0.0D, d5 - d1);
                    entity.motionX *= 0.20000000298023224D;
                    entity.motionZ *= 0.20000000298023224D;
                    entity.addVelocity(d4 + d, 0.0D, d5 + d1);
                }
            } else
            {
                addVelocity(-d, 0.0D, -d1);
                entity.addVelocity(d / 4D, 0.0D, d1 / 4D);
            }
        }
    }

    public int getSizeInventory()
    {
        return 27;
    }

    public ItemStack getStackInSlot(int i)
    {
        return cargoItems[i];
    }

    public ItemStack decrStackSize(int i, int j)
    {
        if(cargoItems[i] != null)
        {
            if(cargoItems[i].stackSize <= j)
            {
                ItemStack itemstack = cargoItems[i];
                cargoItems[i] = null;
                return itemstack;
            }
            ItemStack itemstack1 = cargoItems[i].splitStack(j);
            if(cargoItems[i].stackSize == 0)
            {
                cargoItems[i] = null;
            }
            return itemstack1;
        } else
        {
            return null;
        }
    }

    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        cargoItems[i] = itemstack;
        if(itemstack != null && itemstack.stackSize > getInventoryStackLimit())
        {
            itemstack.stackSize = getInventoryStackLimit();
        }
    }

    public String getInvName()
    {
        return "Minecart";
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public void func_474_j_()
    {
    }

    public boolean interact(EntityPlayer entityplayer)
    {
        if(minecartType == 0)
        {
            if(field_617_ae != null && (field_617_ae instanceof EntityPlayer) && field_617_ae != entityplayer)
            {
                return true;
            }
            if(!worldObj_09.multiplayerWorld)
            {
                entityplayer.func_6377_h(this);
            }
        } else
        if(minecartType == 1)
        {
            entityplayer.func_452_a(this);
        } else
        if(minecartType == 2)
        {
            ItemStack itemstack = entityplayer.inventory.getCurrentItem();
            if(itemstack != null && itemstack.itemID == Item.coal.swiftedIndex)
            {
                if(--itemstack.stackSize == 0)
                {
                    entityplayer.inventory.setInventorySlotContents(entityplayer.inventory.currentItem_00, null);
                }
                fuel += 1200;
            }
            pushX = posX - entityplayer.posX;
            pushZ = posZ - entityplayer.posZ;
        }
        return true;
    }

    public void setPositionAndRotation2(double d, double d1, double d2, float f, 
            float f1, int i)
    {
        field_6444_l = d;
        field_6443_m = d1;
        field_6442_n = d2;
        field_6441_o = f;
        field_6440_p = f1;
        field_6445_k = i + 2;
        motionX = field_6439_q;
        motionY = field_6438_r;
        motionZ = field_6437_s;
    }

    public void setVelocity(double d, double d1, double d2)
    {
        field_6439_q = motionX = d;
        field_6438_r = motionY = d1;
        field_6437_s = motionZ = d2;
    }

    private ItemStack cargoItems[];
    public int field_864_a;
    public int field_863_b;
    public int field_862_c;
    private boolean field_856_i;
    public int minecartType;
    public int fuel;
    public double pushX;
    public double pushZ;
    private static final int field_855_j[][][] = {
        {
            {
                0, 0, -1
            }, {
                0, 0, 1
            }
        }, {
            {
                -1, 0, 0
            }, {
                1, 0, 0
            }
        }, {
            {
                -1, -1, 0
            }, {
                1, 0, 0
            }
        }, {
            {
                -1, 0, 0
            }, {
                1, -1, 0
            }
        }, {
            {
                0, 0, -1
            }, {
                0, -1, 1
            }
        }, {
            {
                0, -1, -1
            }, {
                0, 0, 1
            }
        }, {
            {
                0, 0, 1
            }, {
                1, 0, 0
            }
        }, {
            {
                0, 0, 1
            }, {
                -1, 0, 0
            }
        }, {
            {
                0, 0, -1
            }, {
                -1, 0, 0
            }
        }, {
            {
                0, 0, -1
            }, {
                1, 0, 0
            }
        }
    };
    private int field_6445_k;
    private double field_6444_l;
    private double field_6443_m;
    private double field_6442_n;
    private double field_6441_o;
    private double field_6440_p;
    private double field_6439_q;
    private double field_6438_r;
    private double field_6437_s;

}
