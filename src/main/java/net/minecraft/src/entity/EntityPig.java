package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Item;
import net.minecraft.src.NBTTagCompound;
import net.minecraft.src.world.World;

public class EntityPig extends EntityAnimals
{

    public EntityPig(World world)
    {
        super(world);
        rideable = false;
        field_6409_z = "/mob/pig.png";
        setSize(0.9F, 0.9F);
        rideable = false;
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setBoolean("Saddle", rideable);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
        rideable = nbttagcompound.getBoolean("Saddle");
    }

    protected String func_6389_d()
    {
        return "mob.pig";
    }

    protected String func_6394_f_()
    {
        return "mob.pig";
    }

    protected String func_6390_f()
    {
        return "mob.pigdeath";
    }

    public boolean interact(EntityPlayer entityplayer)
    {
        if(rideable)
        {
            entityplayer.func_6377_h(this);
            return true;
        } else
        {
            return false;
        }
    }

    protected int getDropItemId()
    {
        return Item.porkRaw.swiftedIndex;
    }

    public boolean rideable;
}
