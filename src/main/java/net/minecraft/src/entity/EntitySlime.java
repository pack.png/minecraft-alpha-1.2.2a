package net.minecraft.src.entity;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.world.World;
import net.minecraft.src.world.chunk.Chunk;

public class EntitySlime extends EntityLiving
    implements IMobs
{

    public EntitySlime(World world)
    {
        super(world);
        field_769_d = 0;
        field_770_c = 1;
        field_6409_z = "/mob/slime.png";
        field_770_c = 1 << rand_05.nextInt(3);
        yOffset_00 = 0.0F;
        field_769_d = rand_05.nextInt(20) + 10;
        func_441_c(field_770_c);
    }

    public void func_441_c(int i)
    {
        field_770_c = i;
        setSize(0.6F * (float)i, 0.6F * (float)i);
        health_00 = i * i;
        setPosition(posX, posY, posZ);
    }

    public void writeEntityToNBT(NBTTagCompound nbttagcompound)
    {
        super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setInteger("Size", field_770_c - 1);
    }

    public void readEntityFromNBT(NBTTagCompound nbttagcompound)
    {
        super.readEntityFromNBT(nbttagcompound);
        field_770_c = nbttagcompound.getInteger("Size") + 1;
    }

    public void onUpdate()
    {
        field_767_b = field_768_a;
        boolean flag = onGround_00;
        super.onUpdate();
        if(onGround_00 && !flag)
        {
            for(int i = 0; i < field_770_c * 8; i++)
            {
                float f = rand_05.nextFloat() * 3.141593F * 2.0F;
                float f1 = rand_05.nextFloat() * 0.5F + 0.5F;
                float f2 = MathHelper.sin(f) * (float)field_770_c * 0.5F * f1;
                float f3 = MathHelper.cos(f) * (float)field_770_c * 0.5F * f1;
                worldObj_09.spawnParticle_00("slime", posX + (double)f2, boundingBox.minY, posZ + (double)f3, 0.0D, 0.0D, 0.0D);
            }

            if(field_770_c > 2)
            {
                worldObj_09.playSoundAtEntity(this, "mob.slime", func_6393_h(), ((rand_05.nextFloat() - rand_05.nextFloat()) * 0.2F + 1.0F) / 0.8F);
            }
            field_768_a = -0.5F;
        }
        field_768_a = field_768_a * 0.6F;
    }

    protected void func_418_b_()
    {
        EntityPlayer entityplayer = worldObj_09.getClosestPlayerToEntity(this, 16D);
        if(entityplayer != null)
        {
            func_426_b(entityplayer, 10F);
        }
        if(onGround_00 && field_769_d-- <= 0)
        {
            field_769_d = rand_05.nextInt(20) + 10;
            if(entityplayer != null)
            {
                field_769_d /= 3;
            }
            field_697_Y = true;
            if(field_770_c > 1)
            {
                worldObj_09.playSoundAtEntity(this, "mob.slime", func_6393_h(), ((rand_05.nextFloat() - rand_05.nextFloat()) * 0.2F + 1.0F) * 0.8F);
            }
            field_768_a = 1.0F;
            field_700_V = 1.0F - rand_05.nextFloat() * 2.0F;
            field_699_W = 1 * field_770_c;
        } else
        {
            field_697_Y = false;
            if(onGround_00)
            {
                field_700_V = field_699_W = 0.0F;
            }
        }
    }

    public void setEntityDead()
    {
        if(field_770_c > 1 && health_00 == 0)
        {
            for(int i = 0; i < 4; i++)
            {
                float f = (((float)(i % 2) - 0.5F) * (float)field_770_c) / 4F;
                float f1 = (((float)(i / 2) - 0.5F) * (float)field_770_c) / 4F;
                EntitySlime entityslime = new EntitySlime(worldObj_09);
                entityslime.func_441_c(field_770_c / 2);
                entityslime.func_365_c(posX + (double)f, posY + 0.5D, posZ + (double)f1, rand_05.nextFloat() * 360F, 0.0F);
                worldObj_09.entityJoinedWorld(entityslime);
            }

        }
        super.setEntityDead();
    }

    public void onCollideWithPlayer(EntityPlayer entityplayer)
    {
        if(field_770_c > 1 && func_420_c(entityplayer) && (double)getDistanceToEntity(entityplayer) < 0.59999999999999998D * (double)field_770_c && entityplayer.attackEntity(this, field_770_c))
        {
            worldObj_09.playSoundAtEntity(this, "mob.slimeattack", 1.0F, (rand_05.nextFloat() - rand_05.nextFloat()) * 0.2F + 1.0F);
        }
    }

    protected String func_6394_f_()
    {
        return "mob.slime";
    }

    protected String func_6390_f()
    {
        return "mob.slime";
    }

    protected int getDropItemId()
    {
        if(field_770_c == 1)
        {
            return Item.slimeBall.swiftedIndex;
        } else
        {
            return 0;
        }
    }

    public boolean getCanSpawnHere()
    {
        Chunk chunk = worldObj_09.getChunkFromBlockCoords(MathHelper.convertToBlockCoord_00(posX), MathHelper.convertToBlockCoord_00(posZ));
        return (field_770_c == 1 || worldObj_09.field_1039_l > 0) && rand_05.nextInt(10) == 0 && chunk.func_997_a(0x3ad8025fL).nextInt(10) == 0 && posY < 16D;
    }

    protected float func_6393_h()
    {
        return 0.6F;
    }

    public float field_768_a;
    public float field_767_b;
    private int field_769_d;
    public int field_770_c;
}
