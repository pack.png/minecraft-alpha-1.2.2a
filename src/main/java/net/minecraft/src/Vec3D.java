package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.util.ArrayList;
import java.util.List;

public class Vec3D
{

    public static Vec3D createVectorHelper(double d, double d1, double d2)
    {
        return new Vec3D(d, d1, d2);
    }

    public static void initialize()
    {
        nextVector = 0;
    }

    public static Vec3D createVector(double d, double d1, double d2)
    {
        if(nextVector >= vectorList.size())
        {
            vectorList.add(createVectorHelper(0.0D, 0.0D, 0.0D));
        }
        return ((Vec3D)vectorList.get(nextVector++)).setComponents(d, d1, d2);
    }

    private Vec3D(double d, double d1, double d2)
    {
        if(d == -0D)
        {
            d = 0.0D;
        }
        if(d1 == -0D)
        {
            d1 = 0.0D;
        }
        if(d2 == -0D)
        {
            d2 = 0.0D;
        }
        xCoord_00 = d;
        yCoord_00 = d1;
        zCoord_00 = d2;
    }

    private Vec3D setComponents(double d, double d1, double d2)
    {
        xCoord_00 = d;
        yCoord_00 = d1;
        zCoord_00 = d2;
        return this;
    }

    public Vec3D subtract(Vec3D vec3d)
    {
        return createVector(vec3d.xCoord_00 - xCoord_00, vec3d.yCoord_00 - yCoord_00, vec3d.zCoord_00 - zCoord_00);
    }

    public Vec3D normalize()
    {
        double d = MathHelper.sqrt_double(xCoord_00 * xCoord_00 + yCoord_00 * yCoord_00 + zCoord_00 * zCoord_00);
        if(d < 0.0001D)
        {
            return createVector(0.0D, 0.0D, 0.0D);
        } else
        {
            return createVector(xCoord_00 / d, yCoord_00 / d, zCoord_00 / d);
        }
    }

    public Vec3D crossProduct(Vec3D vec3d)
    {
        return createVector(yCoord_00 * vec3d.zCoord_00 - zCoord_00 * vec3d.yCoord_00, zCoord_00 * vec3d.xCoord_00 - xCoord_00 * vec3d.zCoord_00, xCoord_00 * vec3d.yCoord_00 - yCoord_00 * vec3d.xCoord_00);
    }

    public Vec3D addVector(double d, double d1, double d2)
    {
        return createVector(xCoord_00 + d, yCoord_00 + d1, zCoord_00 + d2);
    }

    public double distanceTo_00(Vec3D vec3d)
    {
        double d = vec3d.xCoord_00 - xCoord_00;
        double d1 = vec3d.yCoord_00 - yCoord_00;
        double d2 = vec3d.zCoord_00 - zCoord_00;
        return (double)MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2);
    }

    public double squareDistanceTo_00(Vec3D vec3d)
    {
        double d = vec3d.xCoord_00 - xCoord_00;
        double d1 = vec3d.yCoord_00 - yCoord_00;
        double d2 = vec3d.zCoord_00 - zCoord_00;
        return d * d + d1 * d1 + d2 * d2;
    }

    public double squareDistanceTo(double d, double d1, double d2)
    {
        double d3 = d - xCoord_00;
        double d4 = d1 - yCoord_00;
        double d5 = d2 - zCoord_00;
        return d3 * d3 + d4 * d4 + d5 * d5;
    }

    public double lengthVector()
    {
        return (double)MathHelper.sqrt_double(xCoord_00 * xCoord_00 + yCoord_00 * yCoord_00 + zCoord_00 * zCoord_00);
    }

    public Vec3D getIntermediateWithXValue(Vec3D vec3d, double d)
    {
        double d1 = vec3d.xCoord_00 - xCoord_00;
        double d2 = vec3d.yCoord_00 - yCoord_00;
        double d3 = vec3d.zCoord_00 - zCoord_00;
        if(d1 * d1 < 1.0000000116860974E-007D)
        {
            return null;
        }
        double d4 = (d - xCoord_00) / d1;
        if(d4 < 0.0D || d4 > 1.0D)
        {
            return null;
        } else
        {
            return createVector(xCoord_00 + d1 * d4, yCoord_00 + d2 * d4, zCoord_00 + d3 * d4);
        }
    }

    public Vec3D getIntermediateWithYValue(Vec3D vec3d, double d)
    {
        double d1 = vec3d.xCoord_00 - xCoord_00;
        double d2 = vec3d.yCoord_00 - yCoord_00;
        double d3 = vec3d.zCoord_00 - zCoord_00;
        if(d2 * d2 < 1.0000000116860974E-007D)
        {
            return null;
        }
        double d4 = (d - yCoord_00) / d2;
        if(d4 < 0.0D || d4 > 1.0D)
        {
            return null;
        } else
        {
            return createVector(xCoord_00 + d1 * d4, yCoord_00 + d2 * d4, zCoord_00 + d3 * d4);
        }
    }

    public Vec3D getIntermediateWithZValue(Vec3D vec3d, double d)
    {
        double d1 = vec3d.xCoord_00 - xCoord_00;
        double d2 = vec3d.yCoord_00 - yCoord_00;
        double d3 = vec3d.zCoord_00 - zCoord_00;
        if(d3 * d3 < 1.0000000116860974E-007D)
        {
            return null;
        }
        double d4 = (d - zCoord_00) / d3;
        if(d4 < 0.0D || d4 > 1.0D)
        {
            return null;
        } else
        {
            return createVector(xCoord_00 + d1 * d4, yCoord_00 + d2 * d4, zCoord_00 + d3 * d4);
        }
    }

    public String toString()
    {
        return (new StringBuilder()).append("(").append(xCoord_00).append(", ").append(yCoord_00).append(", ").append(zCoord_00).append(")").toString();
    }

    public void rotateAroundX(float f)
    {
        float f1 = MathHelper.cos(f);
        float f2 = MathHelper.sin(f);
        double d = xCoord_00;
        double d1 = yCoord_00 * (double)f1 + zCoord_00 * (double)f2;
        double d2 = zCoord_00 * (double)f1 - yCoord_00 * (double)f2;
        xCoord_00 = d;
        yCoord_00 = d1;
        zCoord_00 = d2;
    }

    public void rotateAroundY(float f)
    {
        float f1 = MathHelper.cos(f);
        float f2 = MathHelper.sin(f);
        double d = xCoord_00 * (double)f1 + zCoord_00 * (double)f2;
        double d1 = yCoord_00;
        double d2 = zCoord_00 * (double)f1 - xCoord_00 * (double)f2;
        xCoord_00 = d;
        yCoord_00 = d1;
        zCoord_00 = d2;
    }

    private static List vectorList = new ArrayList();
    private static int nextVector = 0;
    public double xCoord_00;
    public double yCoord_00;
    public double zCoord_00;

}
