package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.world.World;

import java.util.Random;

public class WorldGenLiquids extends WorldGenerator
{

    public WorldGenLiquids(int i)
    {
        field_4157_a = i;
    }

    public boolean generate(World world, Random random, int i, int j, int k)
    {
        if(world.getBlockId(i, j + 1, k) != Block.stone.blockID_00)
        {
            return false;
        }
        if(world.getBlockId(i, j - 1, k) != Block.stone.blockID_00)
        {
            return false;
        }
        if(world.getBlockId(i, j, k) != 0 && world.getBlockId(i, j, k) != Block.stone.blockID_00)
        {
            return false;
        }
        int l = 0;
        if(world.getBlockId(i - 1, j, k) == Block.stone.blockID_00)
        {
            l++;
        }
        if(world.getBlockId(i + 1, j, k) == Block.stone.blockID_00)
        {
            l++;
        }
        if(world.getBlockId(i, j, k - 1) == Block.stone.blockID_00)
        {
            l++;
        }
        if(world.getBlockId(i, j, k + 1) == Block.stone.blockID_00)
        {
            l++;
        }
        int i1 = 0;
        if(world.getBlockId(i - 1, j, k) == 0)
        {
            i1++;
        }
        if(world.getBlockId(i + 1, j, k) == 0)
        {
            i1++;
        }
        if(world.getBlockId(i, j, k - 1) == 0)
        {
            i1++;
        }
        if(world.getBlockId(i, j, k + 1) == 0)
        {
            i1++;
        }
        if(l == 3 && i1 == 1)
        {
            world.setBlockWithNotify(i, j, k, field_4157_a);
            world.field_4214_a = true;
            Block.blocksList[field_4157_a].updateTick(world, i, j, k, random);
            world.field_4214_a = false;
        }
        return true;
    }

    private int field_4157_a;
}
