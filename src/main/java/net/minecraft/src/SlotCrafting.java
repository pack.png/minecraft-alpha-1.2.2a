package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class SlotCrafting extends SlotInventory
{

    public SlotCrafting(GuiContainer guicontainer, IInventory iinventory, IInventory iinventory1, int i, int j, int k)
    {
        super(guicontainer, iinventory1, i, j, k);
        craftMatrix_01 = iinventory;
    }

    public boolean func_4105_a(ItemStack itemstack)
    {
        return false;
    }

    public void func_4103_a()
    {
        for(int i = 0; i < craftMatrix_01.getSizeInventory(); i++)
        {
            if(craftMatrix_01.getStackInSlot(i) != null)
            {
                craftMatrix_01.decrStackSize(i, 1);
            }
        }

    }

    private final IInventory craftMatrix_01;
}
