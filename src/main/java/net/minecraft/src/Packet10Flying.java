package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet10Flying extends Packet
{

    public Packet10Flying()
    {
    }

    public Packet10Flying(boolean flag)
    {
        onGround = flag;
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleFlying(this);
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        onGround = datainputstream.read() != 0;
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.write(onGround ? 1 : 0);
    }

    public int getPacketSize()
    {
        return 1;
    }

    public double xPosition_00;
    public double yPosition;
    public double zPosition_10;
    public double stance;
    public float yaw_01;
    public float pitch_02;
    public boolean onGround;
    public boolean moving;
    public boolean rotating_00;
}
