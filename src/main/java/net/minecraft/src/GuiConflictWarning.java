package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiConflictWarning extends GuiScreen
{

    public GuiConflictWarning()
    {
        updateCounter_03 = 0;
    }

    public void updateScreen()
    {
        updateCounter_03++;
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Back to title screen"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(!guibutton.enabled)
        {
            return;
        }
        if(guibutton.id_02 == 0)
        {
            mc_06.displayGuiScreen(new GuiMainMenu());
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, "Level save conflict", width_02 / 2, (height_02 / 4 - 60) + 20, 0xffffff);
        drawString(field_6451_g, "Minecraft detected a conflict in the level save data.", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 0, 0xa0a0a0);
        drawString(field_6451_g, "This could be caused by two copies of the game", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 18, 0xa0a0a0);
        drawString(field_6451_g, "accessing the same level.", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 27, 0xa0a0a0);
        drawString(field_6451_g, "To prevent level corruption, the current game has quit.", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 45, 0xa0a0a0);
        super.drawScreen(i, j, f);
    }

    private int updateCounter_03;
}
