package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet31RelEntityMove extends Packet30Entity
{

    public Packet31RelEntityMove()
    {
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        super.readPacketData(datainputstream);
        xPosition_08 = datainputstream.readByte();
        yPosition_05 = datainputstream.readByte();
        zPosition_07 = datainputstream.readByte();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        super.writePacketData(dataoutputstream);
        dataoutputstream.writeByte(xPosition_08);
        dataoutputstream.writeByte(yPosition_05);
        dataoutputstream.writeByte(zPosition_07);
    }

    public int getPacketSize()
    {
        return 7;
    }
}
