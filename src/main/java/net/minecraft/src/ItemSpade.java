package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class ItemSpade extends ItemTool
{

    public ItemSpade(int i, int j)
    {
        super(i, 1, j, field_326_aX);
    }

    public boolean func_4018_a(Block block)
    {
        if(block == Block.snow_00)
        {
            return true;
        }
        return block == Block.blockSnow;
    }

    private static Block field_326_aX[];

    static 
    {
        field_326_aX = (new Block[] {
            Block.grass, Block.dirt, Block.sand_00, Block.gravel, Block.snow_00, Block.blockSnow, Block.blockClay
        });
    }
}
