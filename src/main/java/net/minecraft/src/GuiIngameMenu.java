package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiIngameMenu extends GuiScreen
{

    public GuiIngameMenu()
    {
        updateCounter2 = 0;
        updateCounter_02 = 0;
    }

    public void initGui()
    {
        updateCounter2 = 0;
        controlList.clear();
        controlList.add(new GuiButton(1, width_02 / 2 - 100, height_02 / 4 + 48, "Save and quit to title"));
        if(mc_06.func_6260_j())
        {
            ((GuiButton)controlList.get(0)).displayString = "Disconnect";
        }
        controlList.add(new GuiButton(4, width_02 / 2 - 100, height_02 / 4 + 24, "Back to game"));
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 96, "Options..."));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(guibutton.id_02 == 0)
        {
            mc_06.displayGuiScreen(new GuiOptions(this, mc_06.gameSettings));
        }
        if(guibutton.id_02 == 1)
        {
            if(mc_06.func_6260_j())
            {
                mc_06.world.func_660_k();
            }
            mc_06.setWorld(null);
            mc_06.displayGuiScreen(new GuiMainMenu());
        }
        if(guibutton.id_02 == 4)
        {
            mc_06.displayGuiScreen(null);
            mc_06.func_6259_e();
        }
    }

    public void updateScreen()
    {
        super.updateScreen();
        updateCounter_02++;
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        boolean flag = !mc_06.world.func_650_a(updateCounter2++);
        if(flag || updateCounter_02 < 20)
        {
            float f1 = ((float)(updateCounter_02 % 10) + f) / 10F;
            f1 = MathHelper.sin(f1 * 3.141593F * 2.0F) * 0.2F + 0.8F;
            int k = (int)(255F * f1);
            drawString(field_6451_g, "Saving level..", 8, height_02 - 16, k << 16 | k << 8 | k);
        }
        drawCenteredString(field_6451_g, "Game menu", width_02 / 2, 40, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private int updateCounter2;
    private int updateCounter_02;
}
