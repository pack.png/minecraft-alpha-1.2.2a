package net.minecraft.src;

import net.minecraft.client.Minecraft;

public class GuiTextField extends Gui
{
    public int xPosition;
    public int yPosition;

    public final int width;
    public final int height;

    private String text = "";
    private int cursorPosition;
    private int cursorCounter;

    private boolean enabled = true;

    public GuiTextField(int x, int y, String text) {
        this(x, y, 200, 20, text);
    }

    public GuiTextField(int x, int y, int width, int height, String text) {
        this.xPosition = x;
        this.yPosition = y;
        this.width = width;
        this.height = height;
        this.text = text;
    }

    public void setEnabled(boolean value) {
        enabled = value;
    }

    public void updateCursorCounter() {
        cursorCounter++;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setCursorEnd() {
        cursorPosition = text.length() + 1;
    }

    public boolean keyTyped(char c, int i) {
        if(!enabled) {
            return false;
        }
        if(c == '\026')
        {
            String s;
            int j;
            s = GuiScreen.getClipboardString();
            if(s == null)
            {
                s = "";
            }
            j = 32 - text.length();
            if(j > s.length())
            {
                j = s.length();
            }
            if(j > 0)
            {
                text += s.substring(0, j);
            }
        }
        if(c == '\r')
        {
            //actionPerformed((GuiButton)controlList.get(0));
        }
        if(i == 14 && text.length() > 0)
        {
            text = text.substring(0, text.length() - 1);
        }
        if(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_'abcdefghijklmnopqrstuvwxyz{|}~\u2302\307\374\351\342\344\340\345\347\352\353\350\357\356\354\304\305\311\346\306\364\366\362\373\371\377\326\334\370\243\330\327\u0192\341\355\363\372\361\321\252\272\277\256\254\275\274\241\253\273".indexOf(c) >= 0 && text.length() < 32)
        {
            text += c;
        }
        return true;
    }

    public void drawTextField(Minecraft minecraft) {
        FontRenderer fontRenderer = minecraft.fontRenderer;

        drawRect(this.xPosition - 1, this.yPosition - 1, this.xPosition + this.width + 1, this.yPosition + this.height + 1, 0xffa0a0a0);
        drawRect(this.xPosition, this.yPosition, this.xPosition + this.width, this.yPosition + this.height, 0xff000000);

        if (!enabled) {
            drawString(fontRenderer, text, this.xPosition + 4,  this.yPosition + (this.height - 8) / 2, 0xffa0a0a0);
        } else {
            drawString(fontRenderer, (new StringBuilder()).append(text).append((cursorCounter / 6) % 2 != 0 ? "" : "_").toString(), this.xPosition + 4,  this.yPosition + (this.height - 8) / 2, 0xe0e0e0);
        }
    }
}
