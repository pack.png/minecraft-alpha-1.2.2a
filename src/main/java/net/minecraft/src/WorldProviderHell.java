package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.world.chunk.ChunkLoader;
import net.minecraft.src.world.chunk.provider.ChunkProviderHell;

import java.io.File;

public class WorldProviderHell extends WorldProvider
{

    public WorldProviderHell()
    {
    }

    public void func_4098_a()
    {
        field_4215_b = new WorldChunkManagerHell(MobSpawnerBase.field_4245_l, 1.0D, 0.0D);
        field_4220_c = true;
        field_6479_d = true;
        field_6478_e = true;
        field_4218_e = -1;
    }

    public Vec3D func_4096_a(float f, float f1)
    {
        return Vec3D.createVector(0.20000000298023224D, 0.029999999329447746D, 0.029999999329447746D);
    }

    protected void generateLightBrightnessTable()
    {
        float f = 0.1F;
        for(int i = 0; i <= 15; i++)
        {
            float f1 = 1.0F - (float)i / 15F;
            lightBrightnessTable[i] = ((1.0F - f1) / (f1 * 3F + 1.0F)) * (1.0F - f) + f;
        }

    }

    public IChunkProvider func_4094_c()
    {
        return new ChunkProviderHell(field_4216_a, field_4216_a.randomSeed);
    }

    public IChunkLoader func_4092_a(File file)
    {
        File file1 = new File(file, "DIM-1");
        file1.mkdirs();
        return new ChunkLoader(file1, true);
    }

    public boolean func_4102_a(int i, int j)
    {
        int k = field_4216_a.func_614_g(i, j);
        if(k == Block.bedrock.blockID_00)
        {
            return false;
        }
        if(k == 0)
        {
            return false;
        }
        return Block.field_343_p[k];
    }

    public float func_4100_a(long l, float f)
    {
        return 0.5F;
    }

    public boolean func_6477_d()
    {
        return false;
    }
}
