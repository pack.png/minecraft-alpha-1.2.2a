package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class Path
{

    public Path()
    {
        pathPoints = new PathPoint[1024];
        count_01 = 0;
    }

    public PathPoint addPoint(PathPoint pathpoint)
    {
        if(pathpoint.index >= 0)
        {
            throw new IllegalStateException("OW KNOWS!");
        }
        if(count_01 == pathPoints.length)
        {
            PathPoint apathpoint[] = new PathPoint[count_01 << 1];
            System.arraycopy(pathPoints, 0, apathpoint, 0, count_01);
            pathPoints = apathpoint;
        }
        pathPoints[count_01] = pathpoint;
        pathpoint.index = count_01;
        sortBack(count_01++);
        return pathpoint;
    }

    public void clearPath()
    {
        count_01 = 0;
    }

    public PathPoint dequeue()
    {
        PathPoint pathpoint = pathPoints[0];
        pathPoints[0] = pathPoints[--count_01];
        pathPoints[count_01] = null;
        if(count_01 > 0)
        {
            sortForward(0);
        }
        pathpoint.index = -1;
        return pathpoint;
    }

    public void changeDistance(PathPoint pathpoint, float f)
    {
        float f1 = pathpoint.distanceToTarget;
        pathpoint.distanceToTarget = f;
        if(f < f1)
        {
            sortBack(pathpoint.index);
        } else
        {
            sortForward(pathpoint.index);
        }
    }

    private void sortBack(int i)
    {
        PathPoint pathpoint = pathPoints[i];
        float f = pathpoint.distanceToTarget;
        do
        {
            if(i <= 0)
            {
                break;
            }
            int j = i - 1 >> 1;
            PathPoint pathpoint1 = pathPoints[j];
            if(f >= pathpoint1.distanceToTarget)
            {
                break;
            }
            pathPoints[i] = pathpoint1;
            pathpoint1.index = i;
            i = j;
        } while(true);
        pathPoints[i] = pathpoint;
        pathpoint.index = i;
    }

    private void sortForward(int i)
    {
        PathPoint pathpoint = pathPoints[i];
        float f = pathpoint.distanceToTarget;
        do
        {
            int j = 1 + (i << 1);
            int k = j + 1;
            if(j >= count_01)
            {
                break;
            }
            PathPoint pathpoint1 = pathPoints[j];
            float f1 = pathpoint1.distanceToTarget;
            PathPoint pathpoint2;
            float f2;
            if(k >= count_01)
            {
                pathpoint2 = null;
                f2 = (1.0F / 0.0F);
            } else
            {
                pathpoint2 = pathPoints[k];
                f2 = pathpoint2.distanceToTarget;
            }
            if(f1 < f2)
            {
                if(f1 >= f)
                {
                    break;
                }
                pathPoints[i] = pathpoint1;
                pathpoint1.index = i;
                i = j;
                continue;
            }
            if(f2 >= f)
            {
                break;
            }
            pathPoints[i] = pathpoint2;
            pathpoint2.index = i;
            i = k;
        } while(true);
        pathPoints[i] = pathpoint;
        pathpoint.index = i;
    }

    public boolean isPathEmpty()
    {
        return count_01 == 0;
    }

    private PathPoint pathPoints[];
    private int count_01;
}
