package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

public abstract class GuiContainer extends GuiScreen
{

    public GuiContainer()
    {
        xSize_00 = 176;
        ySize_00 = 166;
        inventorySlots = new ArrayList();
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        int k = (width_02 - xSize_00) / 2;
        int l = (height_02 - ySize_00) / 2;
        func_589_a(f);
        GL11.glPushMatrix();
        GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
        RenderHelper.func_1158_b();
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        GL11.glTranslatef(k, l, 0.0F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glEnable(32826);
        for(int i1 = 0; i1 < inventorySlots.size(); i1++)
        {
            SlotInventory slotinventory = (SlotInventory)inventorySlots.get(i1);
            func_590_a(slotinventory);
            if(slotinventory.isAtCursorPos(i, j))
            {
                GL11.glDisable(2896);
                GL11.glDisable(2929);
                int j1 = slotinventory.xPos;
                int k1 = slotinventory.yPos;
                drawGradientRect(j1, k1, j1 + 16, k1 + 16, 0x80ffffff, 0x80ffffff);
                GL11.glEnable(2896);
                GL11.glEnable(2929);
            }
        }

        InventoryPlayer inventoryplayer = mc_06.entityPlayerSP.inventory;
        if(inventoryplayer.field_846_e != null)
        {
            GL11.glTranslatef(0.0F, 0.0F, 32F);
            itemRenderer.func_161_a(field_6451_g, mc_06.renderEngine, inventoryplayer.field_846_e, i - k - 8, j - l - 8);
            itemRenderer.func_164_b(field_6451_g, mc_06.renderEngine, inventoryplayer.field_846_e, i - k - 8, j - l - 8);
        }
        GL11.glDisable(32826);
        RenderHelper.func_1159_a();
        GL11.glDisable(2896);
        GL11.glDisable(2929);
        func_587_j();
        GL11.glEnable(2896);
        GL11.glEnable(2929);
        GL11.glPopMatrix();
    }

    protected void func_587_j()
    {
    }

    protected abstract void func_589_a(float f);

    private void func_590_a(SlotInventory slotinventory)
    {
        IInventory iinventory = slotinventory.inventory_00;
        int i = slotinventory.slotIndex;
        int j = slotinventory.xPos;
        int k = slotinventory.yPos;
        ItemStack itemstack = iinventory.getStackInSlot(i);
        if(itemstack == null)
        {
            int l = slotinventory.func_775_c();
            if(l >= 0)
            {
                GL11.glDisable(2896);
                mc_06.renderEngine.bindTexture(mc_06.renderEngine.getTexture("/gui/items.png"));
                drawTexturedModalRect(j, k, (l % 16) * 16, (l / 16) * 16, 16, 16);
                GL11.glEnable(2896);
                return;
            }
        }
        itemRenderer.func_161_a(field_6451_g, mc_06.renderEngine, itemstack, j, k);
        itemRenderer.func_164_b(field_6451_g, mc_06.renderEngine, itemstack, j, k);
    }

    private Slot getSlotAtPosition(int i, int j)
    {
        for(int k = 0; k < inventorySlots.size(); k++)
        {
            SlotInventory slotinventory = (SlotInventory)inventorySlots.get(k);
            if(slotinventory.isAtCursorPos(i, j))
            {
                return slotinventory;
            }
        }

        return null;
    }

    protected void mouseClicked(int i, int j, int k)
    {
        if(k == 0 || k == 1)
        {
            Slot slot = getSlotAtPosition(i, j);
            InventoryPlayer inventoryplayer = mc_06.entityPlayerSP.inventory;
            if(slot != null)
            {
                ItemStack itemstack = slot.getStack();
                if(itemstack != null || inventoryplayer.field_846_e != null)
                {
                    if(itemstack != null && inventoryplayer.field_846_e == null)
                    {
                        int i1 = k != 0 ? (itemstack.stackSize + 1) / 2 : itemstack.stackSize;
                        inventoryplayer.field_846_e = slot.inventory_00.decrStackSize(slot.slotIndex, i1);
                        if(itemstack.stackSize == 0)
                        {
                            slot.putStack(null);
                        }
                        slot.func_4103_a();
                    } else
                    if(itemstack == null && inventoryplayer.field_846_e != null && slot.func_4105_a(inventoryplayer.field_846_e))
                    {
                        int j1 = k != 0 ? 1 : inventoryplayer.field_846_e.stackSize;
                        if(j1 > slot.func_4104_e())
                        {
                            j1 = slot.func_4104_e();
                        }
                        slot.putStack(inventoryplayer.field_846_e.splitStack(j1));
                        if(inventoryplayer.field_846_e.stackSize == 0)
                        {
                            inventoryplayer.field_846_e = null;
                        }
                    } else
                    if(itemstack != null && inventoryplayer.field_846_e != null)
                    {
                        if(slot.func_4105_a(inventoryplayer.field_846_e))
                        {
                            if(itemstack.itemID != inventoryplayer.field_846_e.itemID)
                            {
                                if(inventoryplayer.field_846_e.stackSize <= slot.func_4104_e())
                                {
                                    ItemStack itemstack1 = itemstack;
                                    slot.putStack(inventoryplayer.field_846_e);
                                    inventoryplayer.field_846_e = itemstack1;
                                }
                            } else
                            if(itemstack.itemID == inventoryplayer.field_846_e.itemID)
                            {
                                if(k == 0)
                                {
                                    int k1 = inventoryplayer.field_846_e.stackSize;
                                    if(k1 > slot.func_4104_e() - itemstack.stackSize)
                                    {
                                        k1 = slot.func_4104_e() - itemstack.stackSize;
                                    }
                                    if(k1 > inventoryplayer.field_846_e.getMaxStackSize() - itemstack.stackSize)
                                    {
                                        k1 = inventoryplayer.field_846_e.getMaxStackSize() - itemstack.stackSize;
                                    }
                                    inventoryplayer.field_846_e.splitStack(k1);
                                    if(inventoryplayer.field_846_e.stackSize == 0)
                                    {
                                        inventoryplayer.field_846_e = null;
                                    }
                                    itemstack.stackSize += k1;
                                } else
                                if(k == 1)
                                {
                                    int l1 = 1;
                                    if(l1 > slot.func_4104_e() - itemstack.stackSize)
                                    {
                                        l1 = slot.func_4104_e() - itemstack.stackSize;
                                    }
                                    if(l1 > inventoryplayer.field_846_e.getMaxStackSize() - itemstack.stackSize)
                                    {
                                        l1 = inventoryplayer.field_846_e.getMaxStackSize() - itemstack.stackSize;
                                    }
                                    inventoryplayer.field_846_e.splitStack(l1);
                                    if(inventoryplayer.field_846_e.stackSize == 0)
                                    {
                                        inventoryplayer.field_846_e = null;
                                    }
                                    itemstack.stackSize += l1;
                                }
                            }
                        } else
                        if(itemstack.itemID == inventoryplayer.field_846_e.itemID && inventoryplayer.field_846_e.getMaxStackSize() > 1)
                        {
                            int i2 = itemstack.stackSize;
                            if(i2 > 0 && i2 + inventoryplayer.field_846_e.stackSize <= inventoryplayer.field_846_e.getMaxStackSize())
                            {
                                inventoryplayer.field_846_e.stackSize += i2;
                                itemstack.splitStack(i2);
                                if(itemstack.stackSize == 0)
                                {
                                    slot.putStack(null);
                                }
                                slot.func_4103_a();
                            }
                        }
                    }
                }
                slot.func_779_d();
            } else
            if(inventoryplayer.field_846_e != null)
            {
                int l = (width_02 - xSize_00) / 2;
                int j2 = (height_02 - ySize_00) / 2;
                if(i < l || j < j2 || i >= l + xSize_00 || j >= j2 + xSize_00)
                {
                    EntityPlayerSP entityplayersp = mc_06.entityPlayerSP;
                    if(k == 0)
                    {
                        entityplayersp.func_449_a(inventoryplayer.field_846_e);
                        inventoryplayer.field_846_e = null;
                    }
                    if(k == 1)
                    {
                        entityplayersp.func_449_a(inventoryplayer.field_846_e.splitStack(1));
                        if(inventoryplayer.field_846_e.stackSize == 0)
                        {
                            inventoryplayer.field_846_e = null;
                        }
                    }
                }
            }
        }
    }

    protected void mouseMoved(int i, int j, int k)
    {
        if(k != 0);
    }

    protected void keyTyped(char c, int i)
    {
        if(i == 1 || i == mc_06.gameSettings.keyBindInventory.keyCode)
        {
            mc_06.displayGuiScreen(null);
        }
    }

    public void func_6449_h()
    {
        if(mc_06.entityPlayerSP == null)
        {
            return;
        }
        InventoryPlayer inventoryplayer = mc_06.entityPlayerSP.inventory;
        if(inventoryplayer.field_846_e != null)
        {
            mc_06.entityPlayerSP.func_449_a(inventoryplayer.field_846_e);
            inventoryplayer.field_846_e = null;
        }
    }

    public boolean func_6450_b()
    {
        return false;
    }

    private static RenderItem itemRenderer = new RenderItem();
    protected int xSize_00;
    protected int ySize_00;
    protected List inventorySlots;

}
