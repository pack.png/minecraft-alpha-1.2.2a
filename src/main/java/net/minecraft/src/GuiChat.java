package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import org.lwjgl.input.Keyboard;

public class GuiChat extends GuiScreen
{

    public GuiChat()
    {
        message_01 = "";
        updateCounter_05 = 0;
    }

    public void initGui()
    {
        Keyboard.enableRepeatEvents(true);
    }

    public void func_6449_h()
    {
        Keyboard.enableRepeatEvents(false);
    }

    public void updateScreen()
    {
        updateCounter_05++;
    }

    protected void keyTyped(char c, int i)
    {
        if(i == 1)
        {
            mc_06.displayGuiScreen(null);
            return;
        }
        if(i == 28)
        {
            String s = message_01.trim();
            if(s.length() > 0)
            {
                mc_06.entityPlayerSP.func_461_a(message_01.trim());
            }
            mc_06.displayGuiScreen(null);
            return;
        }
        if(i == 14 && message_01.length() > 0)
        {
            message_01 = message_01.substring(0, message_01.length() - 1);
        }
        if(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_'abcdefghijklmnopqrstuvwxyz{|}~\u2302\307\374\351\342\344\340\345\347\352\353\350\357\356\354\304\305\311\346\306\364\366\362\373\371\377\326\334\370\243\330\327\u0192\341\355\363\372\361\321\252\272\277\256\254\275\274\241\253\273".indexOf(c) >= 0 && message_01.length() < 100)
        {
        	message_01 += c;
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawRect(2, height_02 - 14, width_02 - 2, height_02 - 2, 0x80000000);
        drawString(field_6451_g, (new StringBuilder()).append("> ").append(message_01).append((updateCounter_05 / 6) % 2 != 0 ? "" : "_").toString(), 4, height_02 - 12, 0xe0e0e0);
    }

    protected void mouseClicked(int i, int j, int k)
    {
        if(k != 0 || mc_06.guiIngame.field_933_a == null)
        {
            return;
        }
        if(message_01.length() > 0 && !message_01.endsWith(" "))
        {
        	message_01 += " ";
        }
        message_01 += mc_06.guiIngame.field_933_a;
        byte byte0 = 100;
        if(message_01.length() > byte0)
        {
            message_01 = message_01.substring(0, byte0);
        }
    }

    private String message_01;
    private int updateCounter_05;
}
