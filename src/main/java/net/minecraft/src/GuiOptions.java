package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiOptions extends GuiScreen
{

    public GuiOptions(GuiScreen guiscreen, GameSettings gamesettings)
    {
        screenTitle = "Options";
        parentScreen = guiscreen;
        options = gamesettings;
    }

    public void initGui()
    {
        for(int i = 0; i < options.numberOfOptions; i++)
        {
            int j = options.getOptionControlType(i);
            if(j == 0)
            {
                controlList.add(new GuiSmallButton(i, (width_02 / 2 - 155) + (i % 2) * 160, height_02 / 6 + 24 * (i >> 1), options.getOptionDisplayString(i)));
            } else
            {
                controlList.add(new GuiSlider(i, (width_02 / 2 - 155) + (i % 2) * 160, height_02 / 6 + 24 * (i >> 1), i, options.getOptionDisplayString(i), options.getOptionFloatValue(i)));
            }
        }

        controlList.add(new GuiButton(100, width_02 / 2 - 100, height_02 / 6 + 120 + 12, "Controls..."));
        controlList.add(new GuiButton(200, width_02 / 2 - 100, height_02 / 6 + 168, "Done"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(!guibutton.enabled)
        {
            return;
        }
        if(guibutton.id_02 < 100)
        {
            options.setOptionValue(guibutton.id_02, 1);
            guibutton.displayString = options.getOptionDisplayString(guibutton.id_02);
        }
        if(guibutton.id_02 == 100)
        {
            mc_06.gameSettings.saveOptions();
            mc_06.displayGuiScreen(new GuiControls(this, options));
        }
        if(guibutton.id_02 == 200)
        {
            mc_06.gameSettings.saveOptions();
            mc_06.displayGuiScreen(parentScreen);
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, screenTitle, width_02 / 2, 20, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private GuiScreen parentScreen;
    protected String screenTitle;
    private GameSettings options;
}
