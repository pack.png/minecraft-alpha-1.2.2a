package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityItem;
import net.minecraft.src.world.World;

import java.util.Random;

public class BlockCrops extends BlockFlower
{

    public BlockCrops(int i, int j)
    {
        super(i, j);
        blockIndexInTexture = j;
        setTickOnLoad(true);
        float f = 0.5F;
        setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
    }

    protected boolean canThisPlantGrowOnThisBlockID(int i)
    {
        return i == Block.tilledField.blockID_00;
    }

    public void updateTick(World world, int i, int j, int k, Random random)
    {
        super.updateTick(world, i, j, k, random);
        if(world.getBlockLightValue_00(i, j + 1, k) >= 9)
        {
            int l = world.getBlockMetadata_01(i, j, k);
            if(l < 7)
            {
                float f = getGrowthRate(world, i, j, k);
                if(random.nextInt((int)(100F / f)) == 0)
                {
                    l++;
                    world.setBlockMetadataWithNotify(i, j, k, l);
                }
            }
        }
    }

    private float getGrowthRate(World world, int i, int j, int k)
    {
        float f = 1.0F;
        int l = world.getBlockId(i, j, k - 1);
        int i1 = world.getBlockId(i, j, k + 1);
        int j1 = world.getBlockId(i - 1, j, k);
        int k1 = world.getBlockId(i + 1, j, k);
        int l1 = world.getBlockId(i - 1, j, k - 1);
        int i2 = world.getBlockId(i + 1, j, k - 1);
        int j2 = world.getBlockId(i + 1, j, k + 1);
        int k2 = world.getBlockId(i - 1, j, k + 1);
        boolean flag = j1 == blockID_00 || k1 == blockID_00;
        boolean flag1 = l == blockID_00 || i1 == blockID_00;
        boolean flag2 = l1 == blockID_00 || i2 == blockID_00 || j2 == blockID_00 || k2 == blockID_00;
        for(int l2 = i - 1; l2 <= i + 1; l2++)
        {
            for(int i3 = k - 1; i3 <= k + 1; i3++)
            {
                int j3 = world.getBlockId(l2, j - 1, i3);
                float f1 = 0.0F;
                if(j3 == Block.tilledField.blockID_00)
                {
                    f1 = 1.0F;
                    if(world.getBlockMetadata_01(l2, j - 1, i3) > 0)
                    {
                        f1 = 3F;
                    }
                }
                if(l2 != i || i3 != k)
                {
                    f1 /= 4F;
                }
                f += f1;
            }

        }

        if(flag2 || flag && flag1)
        {
            f /= 2.0F;
        }
        return f;
    }

    public int getBlockTextureFromSideAndMetadata(int i, int j)
    {
        if(j < 0)
        {
            j = 7;
        }
        return blockIndexInTexture + j;
    }

    public int getRenderType()
    {
        return 6;
    }

    public void onBlockDestroyedByPlayer(World world, int i, int j, int k, int l)
    {
        super.onBlockDestroyedByPlayer(world, i, j, k, l);
        for(int i1 = 0; i1 < 3; i1++)
        {
            if(world.rand_00.nextInt(15) <= l)
            {
                float f = 0.7F;
                float f1 = world.rand_00.nextFloat() * f + (1.0F - f) * 0.5F;
                float f2 = world.rand_00.nextFloat() * f + (1.0F - f) * 0.5F;
                float f3 = world.rand_00.nextFloat() * f + (1.0F - f) * 0.5F;
                EntityItem entityitem = new EntityItem(world, (float)i + f1, (float)j + f2, (float)k + f3, new ItemStack(Item.seeds));
                entityitem.field_805_c = 10;
                world.entityJoinedWorld(entityitem);
            }
        }

    }

    public int idDropped(int i, Random random)
    {
        if(i == 7)
        {
            return Item.wheat.swiftedIndex;
        } else
        {
            return -1;
        }
    }

    public int quantityDropped(Random random)
    {
        return 1;
    }
}
