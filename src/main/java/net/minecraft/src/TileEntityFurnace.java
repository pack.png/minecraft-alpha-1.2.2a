package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class TileEntityFurnace extends TileEntity
    implements IInventory
{

    public TileEntityFurnace()
    {
        field_833_a = new ItemStack[3];
        field_832_b = 0;
        field_835_c = 0;
        field_834_d = 0;
    }

    public int getSizeInventory()
    {
        return field_833_a.length;
    }

    public ItemStack getStackInSlot(int i)
    {
        return field_833_a[i];
    }

    public ItemStack decrStackSize(int i, int j)
    {
        if(field_833_a[i] != null)
        {
            if(field_833_a[i].stackSize <= j)
            {
                ItemStack itemstack = field_833_a[i];
                field_833_a[i] = null;
                return itemstack;
            }
            ItemStack itemstack1 = field_833_a[i].splitStack(j);
            if(field_833_a[i].stackSize == 0)
            {
                field_833_a[i] = null;
            }
            return itemstack1;
        } else
        {
            return null;
        }
    }

    public void setInventorySlotContents(int i, ItemStack itemstack)
    {
        field_833_a[i] = itemstack;
        if(itemstack != null && itemstack.stackSize > getInventoryStackLimit())
        {
            itemstack.stackSize = getInventoryStackLimit();
        }
    }

    public String getInvName()
    {
        return "Chest";
    }

    public void readFromNBT_01(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT_01(nbttagcompound);
        NBTTagList nbttaglist = nbttagcompound.getTagList("Items");
        field_833_a = new ItemStack[getSizeInventory()];
        for(int i = 0; i < nbttaglist.tagCount(); i++)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte byte0 = nbttagcompound1.getByte("Slot");
            if(byte0 >= 0 && byte0 < field_833_a.length)
            {
                field_833_a[byte0] = new ItemStack(nbttagcompound1);
            }
        }

        field_832_b = nbttagcompound.getShort("BurnTime");
        field_834_d = nbttagcompound.getShort("CookTime");
        field_835_c = func_488_a(field_833_a[1]);
    }

    public void writeToNBT_01(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT_01(nbttagcompound);
        nbttagcompound.setShort("BurnTime", (short)field_832_b);
        nbttagcompound.setShort("CookTime", (short)field_834_d);
        NBTTagList nbttaglist = new NBTTagList();
        for(int i = 0; i < field_833_a.length; i++)
        {
            if(field_833_a[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                field_833_a[i].writeToNBT_02(nbttagcompound1);
                nbttaglist.setTag_00(nbttagcompound1);
            }
        }

        nbttagcompound.setTag("Items", nbttaglist);
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public int func_490_a(int i)
    {
        return (field_834_d * i) / 200;
    }

    public int func_489_b(int i)
    {
        if(field_835_c == 0)
        {
            field_835_c = 200;
        }
        return (field_832_b * i) / field_835_c;
    }

    public boolean func_485_a()
    {
        return field_832_b > 0;
    }

    public void updateEntity()
    {
        boolean flag = field_832_b > 0;
        boolean flag1 = false;
        if(field_832_b > 0)
        {
            field_832_b--;
        }
        if(!worldObj_01.multiplayerWorld)
        {
            if(field_832_b == 0 && func_491_j())
            {
                field_835_c = field_832_b = func_488_a(field_833_a[1]);
                if(field_832_b > 0)
                {
                    flag1 = true;
                    if(field_833_a[1] != null)
                    {
                        field_833_a[1].stackSize--;
                        if(field_833_a[1].stackSize == 0)
                        {
                            field_833_a[1] = null;
                        }
                    }
                }
            }
            if(func_485_a() && func_491_j())
            {
                field_834_d++;
                if(field_834_d == 200)
                {
                    field_834_d = 0;
                    func_487_i();
                    flag1 = true;
                }
            } else
            {
                field_834_d = 0;
            }
            if(flag != (field_832_b > 0))
            {
                flag1 = true;
                BlockFurnace.func_285_a(field_832_b > 0, worldObj_01, xCoord_01, yCoord_01, zCoord_01);
            }
        }
        if(flag1)
        {
            func_474_j_();
        }
    }

    private boolean func_491_j()
    {
        if(field_833_a[0] == null)
        {
            return false;
        }
        int i = func_486_d(field_833_a[0].getItem().swiftedIndex);
        if(i < 0)
        {
            return false;
        }
        if(field_833_a[2] == null)
        {
            return true;
        }
        if(field_833_a[2].itemID != i)
        {
            return false;
        }
        if(field_833_a[2].stackSize < getInventoryStackLimit() && field_833_a[2].stackSize < field_833_a[2].getMaxStackSize())
        {
            return true;
        }
        return field_833_a[2].stackSize < Item.itemsList[i].getItemStackLimit();
    }

    public void func_487_i()
    {
        if(!func_491_j())
        {
            return;
        }
        int i = func_486_d(field_833_a[0].getItem().swiftedIndex);
        if(field_833_a[2] == null)
        {
            field_833_a[2] = new ItemStack(i, 1);
        } else
        if(field_833_a[2].itemID == i)
        {
            field_833_a[2].stackSize++;
        }
        field_833_a[0].stackSize--;
        if(field_833_a[0].stackSize <= 0)
        {
            field_833_a[0] = null;
        }
    }

    private int func_486_d(int i)
    {
        if(i == Block.oreIron.blockID_00)
        {
            return Item.ingotIron.swiftedIndex;
        }
        if(i == Block.oreGold.blockID_00)
        {
            return Item.ingotGold.swiftedIndex;
        }
        if(i == Block.oreDiamond.blockID_00)
        {
            return Item.diamond.swiftedIndex;
        }
        if(i == Block.sand_00.blockID_00)
        {
            return Block.glass.blockID_00;
        }
        if(i == Item.porkRaw.swiftedIndex)
        {
            return Item.field_4017_ap.swiftedIndex;
        }
        if(i == Item.field_4021_aS.swiftedIndex)
        {
            return Item.field_4020_aT.swiftedIndex;
        }
        if(i == Block.cobblestone.blockID_00)
        {
            return Block.stone.blockID_00;
        }
        if(i == Item.clay.swiftedIndex)
        {
            return Item.field_4030_aF.swiftedIndex;
        } else
        {
            return -1;
        }
    }

    private int func_488_a(ItemStack itemstack)
    {
        if(itemstack == null)
        {
            return 0;
        }
        int i = itemstack.getItem().swiftedIndex;
        if(i < 256 && Block.blocksList[i].blockMaterial == Material.wood)
        {
            return 300;
        }
        if(i == Item.stick.swiftedIndex)
        {
            return 100;
        }
        if(i == Item.coal.swiftedIndex)
        {
            return 1600;
        }
        return i != Item.bucketLava.swiftedIndex ? 0 : 20000;
    }

    private ItemStack field_833_a[];
    private int field_832_b;
    private int field_835_c;
    private int field_834_d;
}
