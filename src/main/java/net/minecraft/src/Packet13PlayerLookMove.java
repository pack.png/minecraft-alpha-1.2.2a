package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet13PlayerLookMove extends Packet10Flying
{

    public Packet13PlayerLookMove()
    {
        rotating_00 = true;
        moving = true;
    }

    public Packet13PlayerLookMove(double d, double d1, double d2, double d3, float f, float f1, boolean flag)
    {
        xPosition_00 = d;
        yPosition = d1;
        stance = d2;
        zPosition_10 = d3;
        yaw_01 = f;
        pitch_02 = f1;
        onGround = flag;
        rotating_00 = true;
        moving = true;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        xPosition_00 = datainputstream.readDouble();
        yPosition = datainputstream.readDouble();
        stance = datainputstream.readDouble();
        zPosition_10 = datainputstream.readDouble();
        yaw_01 = datainputstream.readFloat();
        pitch_02 = datainputstream.readFloat();
        super.readPacketData(datainputstream);
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeDouble(xPosition_00);
        dataoutputstream.writeDouble(yPosition);
        dataoutputstream.writeDouble(stance);
        dataoutputstream.writeDouble(zPosition_10);
        dataoutputstream.writeFloat(yaw_01);
        dataoutputstream.writeFloat(pitch_02);
        super.writePacketData(dataoutputstream);
    }

    public int getPacketSize()
    {
        return 41;
    }
}
