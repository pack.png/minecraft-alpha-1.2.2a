package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet15Place extends Packet
{

    public Packet15Place()
    {
    }

    public Packet15Place(int i, int j, int k, int l, int i1)
    {
        id = i;
        xPosition = j;
        yPosition_10 = k;
        zPosition_11 = l;
        direction = i1;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        id = datainputstream.readShort();
        xPosition = datainputstream.readInt();
        yPosition_10 = datainputstream.read();
        zPosition_11 = datainputstream.readInt();
        direction = datainputstream.read();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeShort(id);
        dataoutputstream.writeInt(xPosition);
        dataoutputstream.write(yPosition_10);
        dataoutputstream.writeInt(zPosition_11);
        dataoutputstream.write(direction);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handlePlace(this);
    }

    public int getPacketSize()
    {
        return 12;
    }

    public int id;
    public int xPosition;
    public int yPosition_10;
    public int zPosition_11;
    public int direction;
}
