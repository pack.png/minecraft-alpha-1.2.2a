package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.Entity;

import java.io.*;

public class Packet34EntityTeleport extends Packet
{

    public Packet34EntityTeleport()
    {
    }

    public Packet34EntityTeleport(Entity entity)
    {
        entityId_06 = entity.field_620_ab;
        xPosition_14 = MathHelper.convertToBlockCoord_00(entity.posX * 32D);
        yPosition_01 = MathHelper.convertToBlockCoord_00(entity.posY * 32D);
        zPosition_00 = MathHelper.convertToBlockCoord_00(entity.posZ * 32D);
        yaw = (byte)(int)((entity.rotationYaw * 256F) / 360F);
        pitch = (byte)(int)((entity.rotationPitch * 256F) / 360F);
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_06 = datainputstream.readInt();
        xPosition_14 = datainputstream.readInt();
        yPosition_01 = datainputstream.readInt();
        zPosition_00 = datainputstream.readInt();
        yaw = (byte)datainputstream.read();
        pitch = (byte)datainputstream.read();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_06);
        dataoutputstream.writeInt(xPosition_14);
        dataoutputstream.writeInt(yPosition_01);
        dataoutputstream.writeInt(zPosition_00);
        dataoutputstream.write(yaw);
        dataoutputstream.write(pitch);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleEntityTeleport(this);
    }

    public int getPacketSize()
    {
        return 34;
    }

    public int entityId_06;
    public int xPosition_14;
    public int yPosition_01;
    public int zPosition_00;
    public byte yaw;
    public byte pitch;
}
