package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet53BlockChange extends Packet
{

    public Packet53BlockChange()
    {
        isChunkDataPacket = true;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        xPosition_04 = datainputstream.readInt();
        yPosition_04 = datainputstream.read();
        zPosition_04 = datainputstream.readInt();
        type = datainputstream.read();
        metadata = datainputstream.read();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(xPosition_04);
        dataoutputstream.write(yPosition_04);
        dataoutputstream.writeInt(zPosition_04);
        dataoutputstream.write(type);
        dataoutputstream.write(metadata);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleBlockChange(this);
    }

    public int getPacketSize()
    {
        return 11;
    }

    public int xPosition_04;
    public int yPosition_04;
    public int zPosition_04;
    public int type;
    public int metadata;
}
