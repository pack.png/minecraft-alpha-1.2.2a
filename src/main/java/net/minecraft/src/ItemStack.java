package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityLiving;
import net.minecraft.src.world.World;

public final class ItemStack
{

    public ItemStack(Block block)
    {
        this(block, 1);
    }

    public ItemStack(Block block, int i)
    {
        this(block.blockID_00, i);
    }

    public ItemStack(Item item)
    {
        this(item, 1);
    }

    public ItemStack(Item item, int i)
    {
        this(item.swiftedIndex, i);
    }

    public ItemStack(int i)
    {
        this(i, 1);
    }

    public ItemStack(int i, int j)
    {
        stackSize = 0;
        itemID = i;
        stackSize = j;
    }

    public ItemStack(int i, int j, int k)
    {
        stackSize = 0;
        itemID = i;
        stackSize = j;
        itemDamage = k;
    }

    public ItemStack(NBTTagCompound nbttagcompound)
    {
        stackSize = 0;
        readFromNBT_02(nbttagcompound);
    }

    public ItemStack splitStack(int i)
    {
        stackSize -= i;
        return new ItemStack(itemID, i, itemDamage);
    }

    public Item getItem()
    {
        return Item.itemsList[itemID];
    }

    public int getIconIndex_00()
    {
        return getItem().getIconIndex(this);
    }

    public boolean useItem(EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        return getItem().onItemUse(this, entityplayer, world, i, j, k, l);
    }

    public float getStrVsBlock_00(Block block)
    {
        return getItem().getStrVsBlock_01(this, block);
    }

    public ItemStack useItemRightClick(World world, EntityPlayer entityplayer)
    {
        return getItem().onItemRightClick(this, world, entityplayer);
    }

    public NBTTagCompound writeToNBT_02(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setShort("id", (short)itemID);
        nbttagcompound.setByte("Count", (byte)stackSize);
        nbttagcompound.setShort("Damage", (short)itemDamage);
        return nbttagcompound;
    }

    public void readFromNBT_02(NBTTagCompound nbttagcompound)
    {
        itemID = nbttagcompound.getShort("id");
        stackSize = nbttagcompound.getByte("Count");
        itemDamage = nbttagcompound.getShort("Damage");
    }

    public int getMaxStackSize()
    {
        return getItem().getItemStackLimit();
    }

    public int getMaxDamage_00()
    {
        return Item.itemsList[itemID].getMaxDamage();
    }

    public void damageItem(int i)
    {
        itemDamage += i;
        if(itemDamage > getMaxDamage_00())
        {
            stackSize--;
            if(stackSize < 0)
            {
                stackSize = 0;
            }
            itemDamage = 0;
        }
    }

    public void hitEntity(EntityLiving entityliving)
    {
        Item.itemsList[itemID].func_4021_a(this, entityliving);
    }

    public void hitBlock(int i, int j, int k, int l)
    {
        Item.itemsList[itemID].hitBlock_00(this, i, j, k, l);
    }

    public int getDamageVsEntity(Entity entity)
    {
        return Item.itemsList[itemID].func_4020_a(entity);
    }

    public boolean func_1099_b(Block block)
    {
        return Item.itemsList[itemID].func_4018_a(block);
    }

    public void func_1097_a(EntityPlayer entityplayer)
    {
    }

    public void useItemOnEntity(EntityLiving entityliving)
    {
        Item.itemsList[itemID].func_4019_b(this, entityliving);
    }

    public ItemStack copy_00()
    {
        return new ItemStack(itemID, stackSize, itemDamage);
    }

    public int stackSize;
    public int animationsToGo;
    public int itemID;
    public int itemDamage;
}
