package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class Slot
{

    public Slot(IInventory iinventory, int i)
    {
        inventory_00 = iinventory;
        slotIndex = i;
    }

    public void func_4103_a()
    {
        func_779_d();
    }

    public boolean func_4105_a(ItemStack itemstack)
    {
        return true;
    }

    public ItemStack getStack()
    {
        return inventory_00.getStackInSlot(slotIndex);
    }

    public void putStack(ItemStack itemstack)
    {
        inventory_00.setInventorySlotContents(slotIndex, itemstack);
        func_779_d();
    }

    public int func_775_c()
    {
        return -1;
    }

    public void func_779_d()
    {
        inventory_00.func_474_j_();
    }

    public int func_4104_e()
    {
        return inventory_00.getInventoryStackLimit();
    }

    public final int slotIndex;
    public final IInventory inventory_00;
}
