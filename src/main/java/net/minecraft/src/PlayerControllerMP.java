package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.world.World;

public class PlayerControllerMP extends PlayerController
{

    public PlayerControllerMP(Minecraft minecraft, NetClientHandler netclienthandler)
    {
        super(minecraft);
        field_1084_c = -1;
        field_1083_d = -1;
        field_1082_e = -1;
        field_1081_f = 0.0F;
        field_1080_g = 0.0F;
        field_1079_h = 0.0F;
        field_1078_i = 0;
        field_1077_j = false;
        field_1075_l = 0;
        field_1076_k = netclienthandler;
    }

    public void func_6476_a(EntityPlayer entityplayer)
    {
        entityplayer.rotationYaw = -180F;
    }

    public boolean func_729_b(int i, int j, int k, int l)
    {
        field_1076_k.func_847_a(new Packet14BlockDig(3, i, j, k, l));
        int i1 = mc_04.world.getBlockId(i, j, k);
        int j1 = mc_04.world.getBlockMetadata_01(i, j, k);
        boolean flag = super.func_729_b(i, j, k, l);
        ItemStack itemstack = mc_04.entityPlayerSP.func_6416_v();
        if(itemstack != null)
        {
            itemstack.hitBlock(i1, i, j, k);
            if(itemstack.stackSize == 0)
            {
                itemstack.func_1097_a(mc_04.entityPlayerSP);
                mc_04.entityPlayerSP.func_448_u();
            }
        }
        if(flag && mc_04.entityPlayerSP.func_454_b(Block.blocksList[i1]))
        {
            Block.blocksList[i1].dropBlockAsItem(mc_04.world, i, j, k, j1);
        }
        return flag;
    }

    public void func_719_a(int i, int j, int k, int l)
    {
        field_1077_j = true;
        field_1076_k.func_847_a(new Packet14BlockDig(0, i, j, k, l));
        int i1 = mc_04.world.getBlockId(i, j, k);
        if(i1 > 0 && field_1081_f == 0.0F)
        {
            Block.blocksList[i1].onBlockClicked(mc_04.world, i, j, k, mc_04.entityPlayerSP);
        }
        if(i1 > 0 && Block.blocksList[i1].func_225_a(mc_04.entityPlayerSP) >= 1.0F)
        {
            func_729_b(i, j, k, l);
        }
    }

    public void func_6468_a()
    {
        if(!field_1077_j)
        {
            return;
        } else
        {
            field_1077_j = false;
            field_1076_k.func_847_a(new Packet14BlockDig(2, 0, 0, 0, 0));
            field_1081_f = 0.0F;
            field_1078_i = 0;
            return;
        }
    }

    public void func_6470_c(int i, int j, int k, int l)
    {
        field_1077_j = true;
        func_730_e();
        field_1076_k.func_847_a(new Packet14BlockDig(1, i, j, k, l));
        if(field_1078_i > 0)
        {
            field_1078_i--;
            return;
        }
        if(i == field_1084_c && j == field_1083_d && k == field_1082_e)
        {
            int i1 = mc_04.world.getBlockId(i, j, k);
            if(i1 == 0)
            {
                return;
            }
            Block block = Block.blocksList[i1];
            field_1081_f += block.func_225_a(mc_04.entityPlayerSP);
            if(field_1079_h % 4F == 0.0F && block != null)
            {
                mc_04.soundManager.func_336_b(block.stepSound.func_1145_d(), (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, (block.stepSound.func_1147_b() + 1.0F) / 8F, block.stepSound.func_1144_c() * 0.5F);
            }
            field_1079_h++;
            if(field_1081_f >= 1.0F)
            {
                func_729_b(i, j, k, l);
                field_1081_f = 0.0F;
                field_1080_g = 0.0F;
                field_1079_h = 0.0F;
                field_1078_i = 5;
            }
        } else
        {
            field_1081_f = 0.0F;
            field_1080_g = 0.0F;
            field_1079_h = 0.0F;
            field_1084_c = i;
            field_1083_d = j;
            field_1082_e = k;
        }
    }

    public void func_6467_a(float f)
    {
        if(field_1081_f <= 0.0F)
        {
            mc_04.guiIngame.field_6446_b = 0.0F;
            mc_04.field_6323_f.field_1450_i = 0.0F;
        } else
        {
            float f1 = field_1080_g + (field_1081_f - field_1080_g) * f;
            mc_04.guiIngame.field_6446_b = f1;
            mc_04.field_6323_f.field_1450_i = f1;
        }
    }

    public float func_727_b()
    {
        return 4F;
    }

    public void func_717_a(World world)
    {
        super.func_717_a(world);
    }

    public void func_6474_c()
    {
        func_730_e();
        field_1080_g = field_1081_f;
        mc_04.soundManager.func_4033_c();
    }

    private void func_730_e()
    {
        ItemStack itemstack = mc_04.entityPlayerSP.inventory.getCurrentItem();
        int i = 0;
        if(itemstack != null)
        {
            i = itemstack.itemID;
        }
        if(i != field_1075_l)
        {
            field_1075_l = i;
            field_1076_k.func_847_a(new Packet16BlockItemSwitch(0, field_1075_l));
        }
    }

    public boolean func_722_a(EntityPlayer entityplayer, World world, ItemStack itemstack, int i, int j, int k, int l)
    {
        func_730_e();
        field_1076_k.func_847_a(new Packet15Place(itemstack == null ? -1 : itemstack.itemID, i, j, k, l));
        return super.func_722_a(entityplayer, world, itemstack, i, j, k, l);
    }

    public boolean func_6471_a(EntityPlayer entityplayer, World world, ItemStack itemstack)
    {
        func_730_e();
        field_1076_k.func_847_a(new Packet15Place(itemstack == null ? -1 : itemstack.itemID, -1, -1, -1, 255));
        return super.func_6471_a(entityplayer, world, itemstack);
    }

    public EntityPlayer func_4087_b(World world)
    {
        return new EntityClientPlayerMP(mc_04, world, mc_04.field_6320_i, field_1076_k);
    }

    public void func_6475_a(EntityPlayer entityplayer, Entity entity)
    {
        field_1076_k.func_847_a(new Packet7(entityplayer.field_620_ab, entity.field_620_ab));
        entityplayer.func_6415_a_(entity);
    }

    private int field_1084_c;
    private int field_1083_d;
    private int field_1082_e;
    private float field_1081_f;
    private float field_1080_g;
    private float field_1079_h;
    private int field_1078_i;
    private boolean field_1077_j;
    private NetClientHandler field_1076_k;
    private int field_1075_l;
}
