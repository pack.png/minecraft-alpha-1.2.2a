package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class GuiMainMenu extends GuiScreen
{

    public GuiMainMenu()
    {
        updateCounter_06 = 0.0F;
        field_6462_l = "missingno";
        try
        {
            ArrayList arraylist = new ArrayList();
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader((GuiMainMenu.class).getResourceAsStream("/title/splashes.txt")));
            String s = "";
            do
            {
                String s1;
                if((s1 = bufferedreader.readLine()) == null)
                {
                    break;
                }
                s1 = s1.trim();
                if(s1.length() > 0)
                {
                    arraylist.add(s1);
                }
            } while(true);
            field_6462_l = (String)arraylist.get(field_6463_h.nextInt(arraylist.size()));
        }
        catch(Exception exception) { }
    }

    public void updateScreen()
    {
        updateCounter_06++;
        if(logoEffects != null)
        {
            for(int i = 0; i < logoEffects.length; i++)
            {
                for(int j = 0; j < logoEffects[i].length; j++)
                {
                    logoEffects[i][j].func_875_a();
                }

            }

        }
    }

    protected void keyTyped(char c, int i)
    {
    }

    public void initGui()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        if(calendar.get(2) + 1 == 11 && calendar.get(5) == 9)
        {
            field_6462_l = "Happy birthday, ez!";
        } else
        if(calendar.get(2) + 1 == 6 && calendar.get(5) == 1)
        {
            field_6462_l = "Happy birthday, Notch!";
        } else
        if(calendar.get(2) + 1 == 12 && calendar.get(5) == 24)
        {
            field_6462_l = "Merry X-mas!";
        } else
        if(calendar.get(2) + 1 == 1 && calendar.get(5) == 1)
        {
            field_6462_l = "Happy new year!";
        }
        controlList.add(new GuiButton(1, width_02 / 2 - 100, height_02 / 4 + 48, "Singleplayer"));
        controlList.add(new GuiButton(2, width_02 / 2 - 100, height_02 / 4 + 72, "Multiplayer"));
        controlList.add(new GuiButton(3, width_02 / 2 - 100, height_02 / 4 + 96, "Mods and Texture Packs"));
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Options..."));
        if(mc_06.field_6320_i == null)
        {
            ((GuiButton)controlList.get(1)).enabled = false;
        }
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(guibutton.id_02 == 0)
        {
            mc_06.displayGuiScreen(new GuiOptions(this, mc_06.gameSettings));
        }
        if(guibutton.id_02 == 1)
        {
            mc_06.displayGuiScreen(new GuiSelectWorld(this));
        }
        if(guibutton.id_02 == 2)
        {
            mc_06.displayGuiScreen(new GuiMultiplayer(this));
        }
        if(guibutton.id_02 == 3)
        {
            mc_06.displayGuiScreen(new GuiTexturePacks(this));
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        Tessellator tessellator = Tessellator.instance;
        func_4068_a(f);
        GL11.glBindTexture(3553, mc_06.renderEngine.getTexture("/gui/logo.png"));
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        tessellator.setColorOpaque_I(0xffffff);
        GL11.glPushMatrix();
        GL11.glTranslatef(width_02 / 2 + 90, 70F, 0.0F);
        GL11.glRotatef(-20F, 0.0F, 0.0F, 1.0F);
        float f1 = 1.8F - MathHelper.abs(MathHelper.sin(((float)(System.currentTimeMillis() % 1000L) / 1000F) * 3.141593F * 2.0F) * 0.1F);
        f1 = (f1 * 100F) / (float)(field_6451_g.getStringWidth(field_6462_l) + 32);
        GL11.glScalef(f1, f1, f1);
        drawCenteredString(field_6451_g, field_6462_l, 0, -8, 0xffff00);
        GL11.glPopMatrix();
        drawString(field_6451_g, "Minecraft Alpha v1.2.2", 2, 2, 0x505050);
        String s = "Copyright Mojang Specifications. Do not distribute.";
        drawString(field_6451_g, s, width_02 - field_6451_g.getStringWidth(s) - 2, height_02 - 10, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private void func_4068_a(float f)
    {
        if(logoEffects == null)
        {
            logoEffects = new LogoEffectRandomizer[field_4187_a[0].length()][field_4187_a.length];
            for(int i = 0; i < logoEffects.length; i++)
            {
                for(int j = 0; j < logoEffects[i].length; j++)
                {
                    logoEffects[i][j] = new LogoEffectRandomizer(this, i, j);
                }

            }

        }
        GL11.glMatrixMode(5889);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        ScaledResolution scaledresolution = new ScaledResolution(mc_06.field_6326_c, mc_06.field_6325_d);
        int k = 120 * scaledresolution.scaleFactor;
        GLU.gluPerspective(70F, (float)mc_06.field_6326_c / (float)k, 0.05F, 100F);
        GL11.glViewport(0, mc_06.field_6325_d - k, mc_06.field_6326_c, k);
        GL11.glMatrixMode(5888);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glDisable(2884);
        GL11.glCullFace(1029);
        GL11.glDepthMask(true);
        RenderBlocks renderblocks = new RenderBlocks();
        for(int l = 0; l < 3; l++)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef(0.4F, 0.6F, -13F);
            if(l == 0)
            {
                GL11.glClear(256);
                GL11.glTranslatef(0.0F, -0.4F, 0.0F);
                GL11.glScalef(0.98F, 1.0F, 1.0F);
                GL11.glEnable(3042);
                GL11.glBlendFunc(770, 771);
            }
            if(l == 1)
            {
                GL11.glDisable(3042);
                GL11.glClear(256);
            }
            if(l == 2)
            {
                GL11.glEnable(3042);
                GL11.glBlendFunc(768, 1);
            }
            GL11.glScalef(1.0F, -1F, 1.0F);
            GL11.glRotatef(15F, 1.0F, 0.0F, 0.0F);
            GL11.glScalef(0.89F, 1.0F, 0.4F);
            GL11.glTranslatef((float)(-field_4187_a[0].length()) * 0.5F, (float)(-field_4187_a.length) * 0.5F, 0.0F);
            GL11.glBindTexture(3553, mc_06.renderEngine.getTexture("/terrain.png"));
            if(l == 0)
            {
                GL11.glBindTexture(3553, mc_06.renderEngine.getTexture("/title/black.png"));
            }
            for(int i1 = 0; i1 < field_4187_a.length; i1++)
            {
                for(int j1 = 0; j1 < field_4187_a[i1].length(); j1++)
                {
                    char c = field_4187_a[i1].charAt(j1);
                    if(c == ' ')
                    {
                        continue;
                    }
                    GL11.glPushMatrix();
                    LogoEffectRandomizer logoeffectrandomizer = logoEffects[j1][i1];
                    float f1 = (float)(logoeffectrandomizer.field_1311_b + (logoeffectrandomizer.field_1312_a - logoeffectrandomizer.field_1311_b) * (double)f);
                    float f2 = 1.0F;
                    float f3 = 1.0F;
                    float f4 = 0.0F;
                    if(l == 0)
                    {
                        f2 = f1 * 0.04F + 1.0F;
                        f3 = 1.0F / f2;
                        f1 = 0.0F;
                    }
                    GL11.glTranslatef(j1, i1, f1);
                    GL11.glScalef(f2, f2, f2);
                    GL11.glRotatef(f4, 0.0F, 1.0F, 0.0F);
                    renderblocks.func_1238_a(Block.stone, f3);
                    GL11.glPopMatrix();
                }

            }

            GL11.glPopMatrix();
        }

        GL11.glDisable(3042);
        GL11.glMatrixMode(5889);
        GL11.glPopMatrix();
        GL11.glMatrixMode(5888);
        GL11.glPopMatrix();
        GL11.glViewport(0, 0, mc_06.field_6326_c, mc_06.field_6325_d);
        GL11.glEnable(2884);
    }

    static Random getRand()
    {
        return field_6463_h;
    }

    private static final Random field_6463_h = new Random();
    String field_4187_a[] = {
        " *   * * *   * *** *** *** *** *** ***", " ** ** * **  * *   *   * * * * *    * ", " * * * * * * * **  *   **  *** **   * ", " *   * * *  ** *   *   * * * * *    * ", " *   * * *   * *** *** * * * * *    * "
    };
    private LogoEffectRandomizer logoEffects[][];
    private float updateCounter_06;
    private String field_6462_l;

}
