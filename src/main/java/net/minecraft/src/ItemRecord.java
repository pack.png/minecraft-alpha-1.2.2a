package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.world.World;

public class ItemRecord extends Item
{

    protected ItemRecord(int i, String s)
    {
        super(i);
        recordName = s;
        maxStackSize = 1;
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(world.getBlockId(i, j, k) == Block.jukebox.blockID_00 && world.getBlockMetadata_01(i, j, k) == 0)
        {
            world.setBlockMetadataWithNotify(i, j, k, (swiftedIndex - Item.record13.swiftedIndex) + 1);
            world.playRecord_00(recordName, i, j, k);
            itemstack.stackSize--;
            return true;
        } else
        {
            return false;
        }
    }

    private String recordName;
}
