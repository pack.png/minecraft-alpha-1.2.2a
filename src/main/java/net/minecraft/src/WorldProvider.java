package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.world.World;
import net.minecraft.src.world.chunk.ChunkLoader;
import net.minecraft.src.world.chunk.provider.ChunkProviderGenerate;

import java.io.File;

public class WorldProvider
{

    public WorldProvider()
    {
        field_4220_c = false;
        field_6479_d = false;
        field_6478_e = false;
        lightBrightnessTable = new float[16];
        field_4218_e = 0;
        field_4217_f = new float[4];
    }

    public final void func_4095_a(World world)
    {
        field_4216_a = world;
        func_4098_a();
        generateLightBrightnessTable();
    }

    protected void generateLightBrightnessTable()
    {
        float f = 0.05F;
        for(int i = 0; i <= 15; i++)
        {
            float f1 = 1.0F - (float)i / 15F;
            lightBrightnessTable[i] = ((1.0F - f1) / (f1 * 3F + 1.0F)) * (1.0F - f) + f;
        }

    }

    protected void func_4098_a()
    {
        field_4215_b = new WorldChunkManager(field_4216_a);
    }

    public IChunkProvider func_4094_c()
    {
        return new ChunkProviderGenerate(field_4216_a, field_4216_a.randomSeed);
    }

    public IChunkLoader func_4092_a(File file)
    {
        return new ChunkLoader(file, true);
    }

    public boolean func_4102_a(int i, int j)
    {
        int k = field_4216_a.func_614_g(i, j);
        return k == Block.sand_00.blockID_00;
    }

    public float func_4100_a(long l, float f)
    {
        int i = (int)(l % 24000L);
        float f1 = ((float)i + f) / 24000F - 0.25F;
        if(f1 < 0.0F)
        {
            f1++;
        }
        if(f1 > 1.0F)
        {
            f1--;
        }
        float f2 = f1;
        f1 = 1.0F - (float)((Math.cos((double)f1 * 3.1415926535897931D) + 1.0D) / 2D);
        f1 = f2 + (f1 - f2) / 3F;
        return f1;
    }

    public float[] func_4097_b(float f, float f1)
    {
        float f2 = 0.4F;
        float f3 = MathHelper.cos(f * 3.141593F * 2.0F) - 0.0F;
        float f4 = -0F;
        if(f3 >= f4 - f2 && f3 <= f4 + f2)
        {
            float f5 = ((f3 - f4) / f2) * 0.5F + 0.5F;
            float f6 = 1.0F - (1.0F - MathHelper.sin(f5 * 3.141593F)) * 0.99F;
            f6 *= f6;
            field_4217_f[0] = f5 * 0.3F + 0.7F;
            field_4217_f[1] = f5 * f5 * 0.7F + 0.2F;
            field_4217_f[2] = f5 * f5 * 0.0F + 0.2F;
            field_4217_f[3] = f6;
            return field_4217_f;
        } else
        {
            return null;
        }
    }

    public Vec3D func_4096_a(float f, float f1)
    {
        float f2 = MathHelper.cos(f * 3.141593F * 2.0F) * 2.0F + 0.5F;
        if(f2 < 0.0F)
        {
            f2 = 0.0F;
        }
        if(f2 > 1.0F)
        {
            f2 = 1.0F;
        }
        float f3 = 0.7529412F;
        float f4 = 0.8470588F;
        float f5 = 1.0F;
        f3 *= f2 * 0.94F + 0.06F;
        f4 *= f2 * 0.94F + 0.06F;
        f5 *= f2 * 0.91F + 0.09F;
        return Vec3D.createVector(f3, f4, f5);
    }

    public boolean func_6477_d()
    {
        return true;
    }

    public static WorldProvider func_4101_a(int i)
    {
        if(i == 0)
        {
            return new WorldProvider();
        }
        if(i == -1)
        {
            return new WorldProviderHell();
        } else
        {
            return null;
        }
    }

    public World field_4216_a;
    public WorldChunkManager field_4215_b;
    public boolean field_4220_c;
    public boolean field_6479_d;
    public boolean field_6478_e;
    public float lightBrightnessTable[];
    public int field_4218_e;
    private float field_4217_f[];
}
