package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class LoadingScreenRenderer
    implements IProgressUpdate
{

    public LoadingScreenRenderer(Minecraft minecraft)
    {
        subtitle = "";
        title = "";
        field_1006_d = System.currentTimeMillis();
        field_1005_e = false;
        this.minecraft = minecraft;
    }

    public void func_596_a(String s)
    {
        field_1005_e = false;
        func_597_c(s);
    }

    public void func_594_b(String s)
    {
        field_1005_e = true;
        func_597_c(title);
    }

    public void func_597_c(String s)
    {
        if(!minecraft.running)
        {
            if(field_1005_e)
            {
                return;
            } else
            {
                throw new MinecraftError();
            }
        } else
        {
            title = s;
            ScaledResolution scaledresolution = new ScaledResolution(minecraft.field_6326_c, minecraft.field_6325_d);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            GL11.glClear(256);
            GL11.glMatrixMode(5889);
            GL11.glLoadIdentity();
            GL11.glOrtho(0.0D, i, j, 0.0D, 100D, 300D);
            GL11.glMatrixMode(5888);
            GL11.glLoadIdentity();
            GL11.glTranslatef(0.0F, 0.0F, -200F);
            return;
        }
    }

    public void func_595_d(String s)
    {
        if(!minecraft.running)
        {
            if(field_1005_e)
            {
                return;
            } else
            {
                throw new MinecraftError();
            }
        } else
        {
            field_1006_d = 0L;
            subtitle = s;
            func_593_a(-1);
            field_1006_d = 0L;
            return;
        }
    }

    public void func_593_a(int i)
    {
        if(!minecraft.running)
        {
            if(field_1005_e)
            {
                return;
            } else
            {
                throw new MinecraftError();
            }
        }
        long l = System.currentTimeMillis();
        if(l - field_1006_d < 20L)
        {
            return;
        }
        field_1006_d = l;
        ScaledResolution scaledresolution = new ScaledResolution(minecraft.field_6326_c, minecraft.field_6325_d);
        int j = scaledresolution.getScaledWidth();
        int k = scaledresolution.getScaledHeight();
        GL11.glClear(256);
        GL11.glMatrixMode(5889);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0D, j, k, 0.0D, 100D, 300D);
        GL11.glMatrixMode(5888);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0.0F, 0.0F, -200F);
        GL11.glClear(16640);
        Tessellator tessellator = Tessellator.instance;
        int i1 = minecraft.renderEngine.getTexture("/gui/background.png");
        GL11.glBindTexture(3553, i1);
        float f = 32F;
        tessellator.startDrawingQuads();
        tessellator.setColorOpaque_I(0x404040);
        tessellator.addVertexWithUV(0.0D, k, 0.0D, 0.0D, (float)k / f);
        tessellator.addVertexWithUV(j, k, 0.0D, (float)j / f, (float)k / f);
        tessellator.addVertexWithUV(j, 0.0D, 0.0D, (float)j / f, 0.0D);
        tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
        tessellator.draw();
        if(i >= 0)
        {
            byte byte0 = 100;
            byte byte1 = 2;
            int j1 = j / 2 - byte0 / 2;
            int k1 = k / 2 + 16;
            GL11.glDisable(3553);
            tessellator.startDrawingQuads();
            tessellator.setColorOpaque_I(0x808080);
            tessellator.addVertex(j1, k1, 0.0D);
            tessellator.addVertex(j1, k1 + byte1, 0.0D);
            tessellator.addVertex(j1 + byte0, k1 + byte1, 0.0D);
            tessellator.addVertex(j1 + byte0, k1, 0.0D);
            tessellator.setColorOpaque_I(0x80ff80);
            tessellator.addVertex(j1, k1, 0.0D);
            tessellator.addVertex(j1, k1 + byte1, 0.0D);
            tessellator.addVertex(j1 + i, k1 + byte1, 0.0D);
            tessellator.addVertex(j1 + i, k1, 0.0D);
            tessellator.draw();
            GL11.glEnable(3553);
        }
        minecraft.fontRenderer.drawStringWithShadow(title, (j - minecraft.fontRenderer.getStringWidth(title)) / 2, k / 2 - 4 - 16, 0xffffff);
        minecraft.fontRenderer.drawStringWithShadow(subtitle, (j - minecraft.fontRenderer.getStringWidth(subtitle)) / 2, (k / 2 - 4) + 8, 0xffffff);
        Display.update();
        try
        {
            Thread.yield();
        }
        catch(Exception exception) { }
    }

    private String subtitle;
    private Minecraft minecraft;
    private String title;
    private long field_1006_d;
    private boolean field_1005_e;
}
