package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiDownloadTerrain extends GuiScreen
{

    public GuiDownloadTerrain(NetClientHandler netclienthandler)
    {
        updateCounter_04 = 0;
        netHandler_00 = netclienthandler;
    }

    protected void keyTyped(char c, int i)
    {
    }

    public void initGui()
    {
        controlList.clear();
    }

    public void updateScreen()
    {
        updateCounter_04++;
        if(updateCounter_04 % 20 == 0)
        {
            netHandler_00.func_847_a(new Packet0KeepAlive());
        }
        if(netHandler_00 != null)
        {
            netHandler_00.func_848_a();
        }
    }

    protected void actionPerformed(GuiButton guibutton)
    {
    }

    public void drawScreen(int i, int j, float f)
    {
        drawDirtBackground(0);
        drawCenteredString(field_6451_g, "Downloading terrain", width_02 / 2, height_02 / 2 - 50, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private NetClientHandler netHandler_00;
    private int updateCounter_04;
}
