package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import net.minecraft.src.world.World;

public class GuiSelectWorld extends GuiScreen
{

    public GuiSelectWorld(GuiScreen guiscreen)
    {
        screenTitle_02 = "Select world";
        selected = false;
        parentScreen_01 = guiscreen;
    }

    public void initGui()
    {
        java.io.File file = Minecraft.func_6240_b();
        for(int i = 0; i < 5; i++)
        {
            NBTTagCompound nbttagcompound = World.func_629_a(file, (new StringBuilder()).append("World").append(i + 1).toString());
            if(nbttagcompound == null)
            {
                controlList.add(new GuiButton(i, width_02 / 2 - 100, height_02 / 6 + 24 * i, "- empty -"));
            } else
            {
                String s = (new StringBuilder()).append("World ").append(i + 1).toString();
                long l = nbttagcompound.getLong("SizeOnDisk");
                s = (new StringBuilder()).append(s).append(" (").append((float)(((l / 1024L) * 100L) / 1024L) / 100F).append(" MB)").toString();
                controlList.add(new GuiButton(i, width_02 / 2 - 100, height_02 / 6 + 24 * i, s));
            }
        }

        initGui2();
    }

    protected String getWorldName(int i)
    {
        java.io.File file = Minecraft.func_6240_b();
        return World.func_629_a(file, (new StringBuilder()).append("World").append(i).toString()) == null ? null : (new StringBuilder()).append("World").append(i).toString();
    }

    public void initGui2()
    {
        controlList.add(new GuiButton(6, width_02 / 2 - 100, height_02 / 6 + 168, "Cancel"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(!guibutton.enabled)
        {
            return;
        }
        if(guibutton.id_02 < 5)
        {
            selectWorld(guibutton.id_02 + 1);
        } else
        if(guibutton.id_02 == 6)
        {
            mc_06.displayGuiScreen(parentScreen_01);
        }
    }

    public void selectWorld(int i)
    {
        //mc_06.func_6272_a(null);

        mc_06.displayGuiScreen(new GuiSelectWorldSettings(this, i));
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, screenTitle_02, width_02 / 2, 20, 0xffffff);
        super.drawScreen(i, j, f);
    }

    protected GuiScreen parentScreen_01;
    protected String screenTitle_02;
    private boolean selected;
}
