package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.*;

import java.awt.Color;

public class MobSpawnerBase
{

    public MobSpawnerBase()
    {
        field_4242_o = (byte) Block.grass.blockID_00;
        field_4241_p = (byte)Block.dirt.blockID_00;
        field_6502_q = 0x4ee031;
        field_4239_r = (new Class[] {
            EntitySpider.class, EntityZombie.class, EntitySkeleton.class, EntityCreeper.class
        });
        field_4238_s = (new Class[] {
            EntitySheep.class, EntityPig.class, EntityChicken.class, EntityCow.class
        });
    }

    public static void func_4120_a()
    {
        for(int i = 0; i < 64; i++)
        {
            for(int j = 0; j < 64; j++)
            {
                field_4237_t[i + j * 64] = func_4119_a((float)i / 63F, (float)j / 63F);
            }

        }

        field_4249_h.field_4242_o = field_4249_h.field_4241_p = (byte)Block.sand_00.blockID_00;
        field_4247_j.field_4242_o = field_4247_j.field_4241_p = (byte)Block.sand_00.blockID_00;
    }

    protected MobSpawnerBase func_4122_b()
    {
        return this;
    }

    protected MobSpawnerBase func_4125_a(String s)
    {
        field_6504_m = s;
        return this;
    }

    protected MobSpawnerBase func_4124_a(int i)
    {
        field_6502_q = i;
        return this;
    }

    protected MobSpawnerBase func_4123_b(int i)
    {
        field_6503_n = i;
        return this;
    }

    public static MobSpawnerBase func_4121_a(double d, double d1)
    {
        int i = (int)(d * 63D);
        int j = (int)(d1 * 63D);
        return field_4237_t[i + j * 64];
    }

    public static MobSpawnerBase func_4119_a(float f, float f1)
    {
        f1 *= f;
        if(f < 0.1F)
        {
            return field_4246_k;
        }
        if(f1 < 0.2F)
        {
            if(f < 0.5F)
            {
                return field_4246_k;
            }
            if(f < 0.95F)
            {
                return field_4252_e;
            } else
            {
                return field_4249_h;
            }
        }
        if(f1 > 0.5F && f < 0.7F)
        {
            return field_4255_b;
        }
        if(f < 0.5F)
        {
            return field_4250_g;
        }
        if(f < 0.97F)
        {
            if(f1 < 0.35F)
            {
                return field_4251_f;
            } else
            {
                return field_4253_d;
            }
        }
        if(f1 < 0.45F)
        {
            return field_4248_i;
        }
        if(f1 < 0.9F)
        {
            return field_4254_c;
        } else
        {
            return field_4256_a;
        }
    }

    public int func_4126_a(float f)
    {
        f /= 3F;
        if(f < -1F)
        {
            f = -1F;
        }
        if(f > 1.0F)
        {
            f = 1.0F;
        }
        return Color.getHSBColor(0.6222222F - f * 0.05F, 0.5F + f * 0.1F, 1.0F).getRGB();
    }

    public Class[] func_4118_a(EnumCreatureType enumcreaturetype)
    {
        if(enumcreaturetype == EnumCreatureType.monster)
        {
            return field_4239_r;
        }
        if(enumcreaturetype == EnumCreatureType.creature)
        {
            return field_4238_s;
        } else
        {
            return null;
        }
    }

    public static final MobSpawnerBase field_4256_a = (new MobSpawnerBase()).func_4123_b(0x8fa36).func_4125_a("Rainforest").func_4124_a(0x1ff458);
    public static final MobSpawnerBase field_4255_b = (new MobSpawnerSwamp()).func_4123_b(0x7f9b2).func_4125_a("Swampland").func_4124_a(0x8baf48);
    public static final MobSpawnerBase field_4254_c = (new MobSpawnerBase()).func_4123_b(0x9be023).func_4125_a("Seasonal Forest");
    public static final MobSpawnerBase field_4253_d = (new MobSpawnerBase()).func_4123_b(0x56621).func_4125_a("Forest").func_4124_a(0x4eba31);
    public static final MobSpawnerBase field_4252_e = (new MobSpawnerDesert()).func_4123_b(0xd9e023).func_4125_a("Savanna");
    public static final MobSpawnerBase field_4251_f = (new MobSpawnerBase()).func_4123_b(0xa1ad20).func_4125_a("Shrubland");
    public static final MobSpawnerBase field_4250_g = (new MobSpawnerBase()).func_4123_b(0x2eb153).func_4125_a("Taiga").func_4122_b().func_4124_a(0x7bb731);
    public static final MobSpawnerBase field_4249_h = (new MobSpawnerDesert()).func_4123_b(0xfa9418).func_4125_a("Desert");
    public static final MobSpawnerBase field_4248_i = (new MobSpawnerDesert()).func_4123_b(0xffd910).func_4125_a("Plains");
    public static final MobSpawnerBase field_4247_j = (new MobSpawnerDesert()).func_4123_b(0xffed93).func_4125_a("Ice Desert").func_4122_b().func_4124_a(0xc4d339);
    public static final MobSpawnerBase field_4246_k = (new MobSpawnerBase()).func_4123_b(0x57ebf9).func_4125_a("Tundra").func_4122_b().func_4124_a(0xc4d339);
    public static final MobSpawnerBase field_4245_l = (new MobSpawnerHell()).func_4123_b(0xff0000).func_4125_a("Hell");
    public String field_6504_m;
    public int field_6503_n;
    public byte field_4242_o;
    public byte field_4241_p;
    public int field_6502_q;
    protected Class field_4239_r[];
    protected Class field_4238_s[];
    private static MobSpawnerBase field_4237_t[] = new MobSpawnerBase[4096];

    static 
    {
        func_4120_a();
    }
}
