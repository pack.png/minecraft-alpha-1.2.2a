package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet50PreChunk extends Packet
{

    public Packet50PreChunk()
    {
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        xPosition_13 = datainputstream.readInt();
        yPosition_12 = datainputstream.readInt();
        mode = datainputstream.read() != 0;
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(xPosition_13);
        dataoutputstream.writeInt(yPosition_12);
        dataoutputstream.write(mode ? 1 : 0);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handlePreChunk(this);
    }

    public int getPacketSize()
    {
        return 9;
    }

    public int xPosition_13;
    public int yPosition_12;
    public boolean mode;
}
