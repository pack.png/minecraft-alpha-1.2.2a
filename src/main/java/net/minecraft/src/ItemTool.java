package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityLiving;

public class ItemTool extends Item
{

    public ItemTool(int i, int j, int k, Block ablock[])
    {
        super(i);
        field_324_aY = 4F;
        field_322_a = k;
        field_325_aX = ablock;
        maxStackSize = 1;
        maxDamage = 32 << k;
        if(k == 3)
        {
            maxDamage *= 4;
        }
        field_324_aY = (k + 1) * 2;
        field_323_aZ = j + k;
    }

    public float getStrVsBlock_01(ItemStack itemstack, Block block)
    {
        for(int i = 0; i < field_325_aX.length; i++)
        {
            if(field_325_aX[i] == block)
            {
                return field_324_aY;
            }
        }

        return 1.0F;
    }

    public void func_4021_a(ItemStack itemstack, EntityLiving entityliving)
    {
        itemstack.damageItem(2);
    }

    public void hitBlock_00(ItemStack itemstack, int i, int j, int k, int l)
    {
        itemstack.damageItem(1);
    }

    public int func_4020_a(Entity entity)
    {
        return field_323_aZ;
    }

    public boolean func_4017_a()
    {
        return true;
    }

    private Block field_325_aX[];
    private float field_324_aY;
    private int field_323_aZ;
    protected int field_322_a;
}
