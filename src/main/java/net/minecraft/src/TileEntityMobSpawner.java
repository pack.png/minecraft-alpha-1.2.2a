package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityLiving;

public class TileEntityMobSpawner extends TileEntity
{

    public TileEntityMobSpawner()
    {
        delay = -1;
        field_830_d = 0.0D;
        entityID = "Pig";
        delay = 20;
    }

    public boolean func_484_a()
    {
        return worldObj_01.getClosestPlayer((double)xCoord_01 + 0.5D, (double)yCoord_01 + 0.5D, (double)zCoord_01 + 0.5D, 16D) != null;
    }

    public void updateEntity()
    {
        field_830_d = field_831_c;
        if(!func_484_a())
        {
            return;
        }
        double d = (float)xCoord_01 + worldObj_01.rand_00.nextFloat();
        double d2 = (float)yCoord_01 + worldObj_01.rand_00.nextFloat();
        double d4 = (float)zCoord_01 + worldObj_01.rand_00.nextFloat();
        worldObj_01.spawnParticle_00("smoke", d, d2, d4, 0.0D, 0.0D, 0.0D);
        worldObj_01.spawnParticle_00("flame", d, d2, d4, 0.0D, 0.0D, 0.0D);
        for(field_831_c += 1000F / ((float)delay + 200F); field_831_c > 360D;)
        {
            field_831_c -= 360D;
            field_830_d -= 360D;
        }

        if(delay == -1)
        {
            updateDelay();
        }
        if(delay > 0)
        {
            delay--;
            return;
        }
        byte byte0 = 4;
        for(int i = 0; i < byte0; i++)
        {
            EntityLiving entityliving = (EntityLiving)EntityList.func_1079_a(entityID, worldObj_01);
            if(entityliving == null)
            {
                return;
            }
            int j = worldObj_01.getEntitiesWithinAABB(entityliving.getClass(), AxisAlignedBB.getBoundingBoxFromPool(xCoord_01, yCoord_01, zCoord_01, xCoord_01 + 1, yCoord_01 + 1, zCoord_01 + 1).expands(8D, 4D, 8D)).size();
            if(j >= 6)
            {
                updateDelay();
                return;
            }
            if(entityliving == null)
            {
                continue;
            }
            double d6 = (double)xCoord_01 + (worldObj_01.rand_00.nextDouble() - worldObj_01.rand_00.nextDouble()) * 4D;
            double d7 = (yCoord_01 + worldObj_01.rand_00.nextInt(3)) - 1;
            double d8 = (double)zCoord_01 + (worldObj_01.rand_00.nextDouble() - worldObj_01.rand_00.nextDouble()) * 4D;
            entityliving.func_365_c(d6, d7, d8, worldObj_01.rand_00.nextFloat() * 360F, 0.0F);
            if(!entityliving.getCanSpawnHere())
            {
                continue;
            }
            worldObj_01.entityJoinedWorld(entityliving);
            for(int k = 0; k < 20; k++)
            {
                double d1 = (double)xCoord_01 + 0.5D + ((double)worldObj_01.rand_00.nextFloat() - 0.5D) * 2D;
                double d3 = (double)yCoord_01 + 0.5D + ((double)worldObj_01.rand_00.nextFloat() - 0.5D) * 2D;
                double d5 = (double)zCoord_01 + 0.5D + ((double)worldObj_01.rand_00.nextFloat() - 0.5D) * 2D;
                worldObj_01.spawnParticle_00("smoke", d1, d3, d5, 0.0D, 0.0D, 0.0D);
                worldObj_01.spawnParticle_00("flame", d1, d3, d5, 0.0D, 0.0D, 0.0D);
            }

            entityliving.func_415_z();
            updateDelay();
        }

        super.updateEntity();
    }

    private void updateDelay()
    {
        delay = 200 + worldObj_01.rand_00.nextInt(600);
    }

    public void readFromNBT_01(NBTTagCompound nbttagcompound)
    {
        super.readFromNBT_01(nbttagcompound);
        entityID = nbttagcompound.getString("EntityId");
        delay = nbttagcompound.getShort("Delay");
    }

    public void writeToNBT_01(NBTTagCompound nbttagcompound)
    {
        super.writeToNBT_01(nbttagcompound);
        nbttagcompound.setString("EntityId", entityID);
        nbttagcompound.setShort("Delay", (short)delay);
    }

    public int delay;
    public String entityID;
    public double field_831_c;
    public double field_830_d;
}
