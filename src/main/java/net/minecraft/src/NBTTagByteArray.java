package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class NBTTagByteArray extends NBTBase
{

    public NBTTagByteArray()
    {
    }

    public NBTTagByteArray(byte abyte0[])
    {
        byteArray = abyte0;
    }

    void writeTag_00(DataOutput dataoutput) throws IOException
    {
        dataoutput.writeInt(byteArray.length);
        dataoutput.write(byteArray);
    }

    void readTag_00(DataInput datainput) throws IOException
    {
        int i = datainput.readInt();
        byteArray = new byte[i];
        datainput.readFully(byteArray);
    }

    public byte getType()
    {
        return 7;
    }

    public String toString()
    {
        return (new StringBuilder()).append("[").append(byteArray.length).append(" bytes]").toString();
    }

    public byte byteArray[];
}
