package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.world.World;

import java.util.Random;

public class BlockPortal extends BlockBreakable
{

    public BlockPortal(int i, int j)
    {
        super(i, j, Material.field_4260_x, false);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
    {
        return null;
    }

    public void setBlockBoundsBasedOnState(IBlockAccess iblockaccess, int i, int j, int k)
    {
        if(iblockaccess.getBlockId(i - 1, j, k) == blockID_00 || iblockaccess.getBlockId(i + 1, j, k) == blockID_00)
        {
            float f = 0.5F;
            float f2 = 0.125F;
            setBlockBounds(0.5F - f, 0.0F, 0.5F - f2, 0.5F + f, 1.0F, 0.5F + f2);
        } else
        {
            float f1 = 0.125F;
            float f3 = 0.5F;
            setBlockBounds(0.5F - f1, 0.0F, 0.5F - f3, 0.5F + f1, 1.0F, 0.5F + f3);
        }
    }

    public boolean allowsAttachment()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public boolean tryToCreatePortal(World world, int i, int j, int k)
    {
        int l = 0;
        int i1 = 0;
        if(world.getBlockId(i - 1, j, k) == Block.obsidian.blockID_00 || world.getBlockId(i + 1, j, k) == Block.obsidian.blockID_00)
        {
            l = 1;
        }
        if(world.getBlockId(i, j, k - 1) == Block.obsidian.blockID_00 || world.getBlockId(i, j, k + 1) == Block.obsidian.blockID_00)
        {
            i1 = 1;
        }
        System.out.println((new StringBuilder()).append(l).append(", ").append(i1).toString());
        if(l == i1)
        {
            return false;
        }
        if(world.getBlockId(i - l, j, k - i1) == 0)
        {
            i -= l;
            k -= i1;
        }
        for(int j1 = -1; j1 <= 2; j1++)
        {
            for(int l1 = -1; l1 <= 3; l1++)
            {
                boolean flag = j1 == -1 || j1 == 2 || l1 == -1 || l1 == 3;
                if((j1 == -1 || j1 == 2) && (l1 == -1 || l1 == 3))
                {
                    continue;
                }
                int j2 = world.getBlockId(i + l * j1, j + l1, k + i1 * j1);
                if(flag)
                {
                    if(j2 != Block.obsidian.blockID_00)
                    {
                        return false;
                    }
                    continue;
                }
                if(j2 != 0 && j2 != Block.fire_01.blockID_00)
                {
                    return false;
                }
            }

        }

        world.field_1043_h = true;
        for(int k1 = 0; k1 < 2; k1++)
        {
            for(int i2 = 0; i2 < 3; i2++)
            {
                world.setBlockWithNotify(i + l * k1, j + i2, k + i1 * k1, Block.field_4047_bf.blockID_00);
            }

        }

        world.field_1043_h = false;
        return true;
    }

    public void onNeighborBlockChange(World world, int i, int j, int k, int l)
    {
        int i1 = 0;
        int j1 = 1;
        if(world.getBlockId(i - 1, j, k) == blockID_00 || world.getBlockId(i + 1, j, k) == blockID_00)
        {
            i1 = 1;
            j1 = 0;
        }
        int k1;
        for(k1 = j; world.getBlockId(i, k1 - 1, k) == blockID_00; k1--) { }
        if(world.getBlockId(i, k1 - 1, k) != Block.obsidian.blockID_00)
        {
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        int l1;
        for(l1 = 1; l1 < 4 && world.getBlockId(i, k1 + l1, k) == blockID_00; l1++) { }
        if(l1 != 3 || world.getBlockId(i, k1 + l1, k) != Block.obsidian.blockID_00)
        {
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        boolean flag = world.getBlockId(i - 1, j, k) == blockID_00 || world.getBlockId(i + 1, j, k) == blockID_00;
        boolean flag1 = world.getBlockId(i, j, k - 1) == blockID_00 || world.getBlockId(i, j, k + 1) == blockID_00;
        if(flag && flag1)
        {
            world.setBlockWithNotify(i, j, k, 0);
            return;
        }
        if((world.getBlockId(i + i1, j, k + j1) != Block.obsidian.blockID_00 || world.getBlockId(i - i1, j, k - j1) != blockID_00) && (world.getBlockId(i - i1, j, k - j1) != Block.obsidian.blockID_00 || world.getBlockId(i + i1, j, k + j1) != blockID_00))
        {
            world.setBlockWithNotify(i, j, k, 0);
            return;
        } else
        {
            return;
        }
    }

    public boolean isSideInsideCoordinate(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        return true;
    }

    public int quantityDropped(Random random)
    {
        return 0;
    }

    public int func_234_g()
    {
        return 1;
    }

    public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
    {
        if(world.multiplayerWorld)
        {
            return;
        } else
        {
            entity.func_4039_q();
            return;
        }
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        if(random.nextInt(100) == 0)
        {
            world.playSoundEffect((double)i + 0.5D, (double)j + 0.5D, (double)k + 0.5D, "portal.portal", 1.0F, random.nextFloat() * 0.4F + 0.8F);
        }
        for(int l = 0; l < 4; l++)
        {
            double d = (float)i + random.nextFloat();
            double d1 = (float)j + random.nextFloat();
            double d2 = (float)k + random.nextFloat();
            double d3 = 0.0D;
            double d4 = 0.0D;
            double d5 = 0.0D;
            int i1 = random.nextInt(2) * 2 - 1;
            d3 = ((double)random.nextFloat() - 0.5D) * 0.5D;
            d4 = ((double)random.nextFloat() - 0.5D) * 0.5D;
            d5 = ((double)random.nextFloat() - 0.5D) * 0.5D;
            if(world.getBlockId(i - 1, j, k) == blockID_00 || world.getBlockId(i + 1, j, k) == blockID_00)
            {
                d2 = (double)k + 0.5D + 0.25D * (double)i1;
                d5 = random.nextFloat() * 2.0F * (float)i1;
            } else
            {
                d = (double)i + 0.5D + 0.25D * (double)i1;
                d3 = random.nextFloat() * 2.0F * (float)i1;
            }
            world.spawnParticle_00("portal", d, d1, d2, d3, d4, d5);
        }

    }
}
