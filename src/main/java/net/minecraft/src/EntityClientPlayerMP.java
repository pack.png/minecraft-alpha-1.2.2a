package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityItem;
import net.minecraft.src.world.World;

public class EntityClientPlayerMP extends EntityPlayerSP
{

    public EntityClientPlayerMP(Minecraft minecraft, World world, Session session, NetClientHandler netclienthandler)
    {
        super(minecraft, world, session, 0);
        field_4137_bn = 0;
        field_4140_bu = new InventoryPlayer(null);
        field_4139_bv = false;
        field_4138_bw = 0;
        field_797_bg = netclienthandler;
    }

    public boolean attackEntity(Entity entity, int i)
    {
        return false;
    }

    public void heal(int i)
    {
    }

    public void onUpdate()
    {
        if(!worldObj_09.func_630_d(MathHelper.convertToBlockCoord_00(posX), 64, MathHelper.convertToBlockCoord_00(posZ)))
        {
            return;
        } else
        {
            super.onUpdate();
            func_4056_N();
            return;
        }
    }

    public void func_6420_o()
    {
    }

    public void func_4056_N()
    {
        if(field_4137_bn++ == 20)
        {
            if(!inventory.func_500_a(field_4140_bu))
            {
                field_797_bg.func_847_a(new Packet5PlayerInventory(-1, inventory.mainInventory));
                field_797_bg.func_847_a(new Packet5PlayerInventory(-2, inventory.craftingInventory));
                field_797_bg.func_847_a(new Packet5PlayerInventory(-3, inventory.armorInventory));
                field_4140_bu = inventory.func_512_i();
            }
            field_4137_bn = 0;
        }
        double d = posX - field_6426_bv;
        double d1 = boundingBox.minY - field_6425_bw;
        double d2 = posY - field_6424_bx;
        double d3 = posZ - field_6423_by;
        double d4 = rotationYaw - field_6422_bz;
        double d5 = rotationPitch - field_6421_bA;
        boolean flag = d1 != 0.0D || d2 != 0.0D || d != 0.0D || d3 != 0.0D;
        boolean flag1 = d4 != 0.0D || d5 != 0.0D;
        if(field_616_af != null)
        {
            if(flag1)
            {
                field_797_bg.func_847_a(new Packet11PlayerPosition(motionX, -999D, -999D, motionZ, onGround_00));
            } else
            {
                field_797_bg.func_847_a(new Packet13PlayerLookMove(motionX, -999D, -999D, motionZ, rotationYaw, rotationPitch, onGround_00));
            }
            flag = false;
        }
        if(flag && flag1)
        {
            field_797_bg.func_847_a(new Packet13PlayerLookMove(posX, boundingBox.minY, posY, posZ, rotationYaw, rotationPitch, onGround_00));
            field_4138_bw = 0;
        } else
        if(flag)
        {
            field_797_bg.func_847_a(new Packet11PlayerPosition(posX, boundingBox.minY, posY, posZ, onGround_00));
            field_4138_bw = 0;
        } else
        if(flag1)
        {
            field_797_bg.func_847_a(new Packet12PlayerLook(rotationYaw, rotationPitch, onGround_00));
            field_4138_bw = 0;
        } else
        if(field_4139_bv != onGround_00 || field_4138_bw > 20)
        {
            field_797_bg.func_847_a(new Packet10Flying(onGround_00));
            field_4138_bw = 0;
        } else
        {
            field_4138_bw++;
        }
        field_4139_bv = onGround_00;
        if(flag)
        {
            field_6426_bv = posX;
            field_6425_bw = boundingBox.minY;
            field_6424_bx = posY;
            field_6423_by = posZ;
        }
        if(flag1)
        {
            field_6422_bz = rotationYaw;
            field_6421_bA = rotationPitch;
        }
    }

    protected void func_446_a(EntityItem entityitem)
    {
        Packet21PickupSpawn packet21pickupspawn = new Packet21PickupSpawn(entityitem);
        field_797_bg.func_847_a(packet21pickupspawn);
        entityitem.posX = (double)packet21pickupspawn.xPosition_02 / 32D;
        entityitem.posY = (double)packet21pickupspawn.yPosition_02 / 32D;
        entityitem.posZ = (double)packet21pickupspawn.zPosition_01 / 32D;
        entityitem.motionX = (double)packet21pickupspawn.rotation / 128D;
        entityitem.motionY = (double)packet21pickupspawn.pitch_03 / 128D;
        entityitem.motionZ = (double)packet21pickupspawn.roll / 128D;
    }

    public void func_461_a(String s)
    {
        field_797_bg.func_847_a(new Packet3Chat(s));
    }

    public void func_457_w()
    {
        super.func_457_w();
        field_797_bg.func_847_a(new Packet18ArmAnimation(this, 1));
    }

    private NetClientHandler field_797_bg;
    private int field_4137_bn;
    private double field_6426_bv;
    private double field_6425_bw;
    private double field_6424_bx;
    private double field_6423_by;
    private float field_6422_bz;
    private float field_6421_bA;
    private InventoryPlayer field_4140_bu;
    private boolean field_4139_bv;
    private int field_4138_bw;
}
