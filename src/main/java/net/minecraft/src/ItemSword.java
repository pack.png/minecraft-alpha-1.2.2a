package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityLiving;

public class ItemSword extends Item
{

    public ItemSword(int i, int j)
    {
        super(i);
        maxStackSize = 1;
        maxDamage = 32 << j;
        if(j == 3)
        {
            maxDamage *= 4;
        }
        field_319_a = 4 + j * 2;
    }

    public float getStrVsBlock_01(ItemStack itemstack, Block block)
    {
        return 1.5F;
    }

    public void func_4021_a(ItemStack itemstack, EntityLiving entityliving)
    {
        itemstack.damageItem(1);
    }

    public void hitBlock_00(ItemStack itemstack, int i, int j, int k, int l)
    {
        itemstack.damageItem(2);
    }

    public int func_4020_a(Entity entity)
    {
        return field_319_a;
    }

    public boolean func_4017_a()
    {
        return true;
    }

    private int field_319_a;
}
