package net.minecraft.src;

import net.minecraft.client.Minecraft;
import net.minecraft.src.world.World;

import java.util.Random;

public class GuiSelectWorldSettings extends GuiScreen {
    private GuiScreen parentScreen;
    private String screenTitle;
    private NBTTagCompound compoundTag;
    private int worldNumber;

    private GuiTextField textField;

    public GuiSelectWorldSettings(GuiScreen parentScreen, int worldNumber) {
        this.parentScreen = parentScreen;
        screenTitle = "Edit World";

        java.io.File file = Minecraft.func_6240_b();
        NBTTagCompound nbttagcompound = World.func_629_a(file, "World" + worldNumber);
        if(nbttagcompound == null) {
            screenTitle = "Create World";
        }

        this.worldNumber = worldNumber;
        this.compoundTag = nbttagcompound;
    }

    public void updateScreen() {
        textField.updateCursorCounter();
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 96 + 12, "Play"));
        controlList.add(new GuiButton(1, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Cancel"));

        if(compoundTag != null) {
            controlList.add(new GuiButton(2, width_02 / 2 - 100, height_02 / 4 + 72 + 12, "Delete World"));
        }

        textField = new GuiTextField(width_02 / 2 - 100, 60, "");

        if(compoundTag != null) {
            textField.setEnabled(false);
            textField.setText(String.valueOf(compoundTag.getLong("RandomSeed")));
        }
    }

    public void actionPerformed(GuiButton guibutton) {
        if(guibutton.id_02 == 0) {
            playWorld();
        }
        else if(guibutton.id_02 == 1) {
            mc_06.displayGuiScreen(parentScreen);
        } else if(guibutton.id_02 == 2) {
            mc_06.displayGuiScreen(new GuiYesNo(this, "Are you sure you want to delete this world?", (new StringBuilder()).append("'").append("World" + worldNumber).append("' will be lost forever!").toString(), worldNumber));
        }
    }

    public void deleteWorld_00(boolean flag, int i)
    {
        if(flag)
        {
            java.io.File file = Minecraft.func_6240_b();
            World.deleteWorld(file, "World" + i);
        }
        mc_06.displayGuiScreen(parentScreen);
    }

    protected void keyTyped(char c, int keyCode) {
        textField.keyTyped(c, keyCode);
    }

    public void drawScreen(int i, int j, float f) {
        drawBackground();
        drawCenteredString(field_6451_g, screenTitle, width_02 / 2, 20, 0xffffff);

        drawString(field_6451_g, "World Seed", width_02 / 2 - 100, 47, 0xffa0a0a0);

        if(compoundTag == null) {
            drawString(field_6451_g, "Leave blank for a random seed", width_02 / 2 - 100, 85, 0xffa0a0a0);
        }

        textField.drawTextField(mc_06);
        super.drawScreen(i, j, f);
    }

    public void playWorld()
    {
        mc_06.displayGuiScreen(null);

        long seed = new Random().nextLong();
        String text = textField.getText();

        try {
            long parsedSeed = Long.parseLong(text);

            if(parsedSeed != 0L) {
                seed = parsedSeed;
            }
        } catch (NumberFormatException ex) {
            seed = text.hashCode();
        }

        mc_06.playerController = new PlayerControllerSP(mc_06);
        mc_06.loadWorld("World" + worldNumber, seed);
        mc_06.displayGuiScreen(null);
    }
}
