package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


import net.minecraft.src.world.World;

public class ItemSign extends Item
{

    public ItemSign(int i)
    {
        super(i);
        maxDamage = 64;
        maxStackSize = 1;
    }

    public boolean onItemUse(ItemStack itemstack, EntityPlayer entityplayer, World world, int i, int j, int k, int l)
    {
        if(l == 0)
        {
            return false;
        }
        if(!world.getBlockMaterial(i, j, k).func_878_a())
        {
            return false;
        }
        if(l == 1)
        {
            j++;
        }
        if(l == 2)
        {
            k--;
        }
        if(l == 3)
        {
            k++;
        }
        if(l == 4)
        {
            i--;
        }
        if(l == 5)
        {
            i++;
        }
        if(!Block.pressurePlateWoodActive.canPlaceBlockAt(world, i, j, k))
        {
            return false;
        }
        if(l == 1)
        {
            world.func_688_b(i, j, k, Block.pressurePlateWoodActive.blockID_00, MathHelper.convertToBlockCoord_00((double)(((entityplayer.rotationYaw + 180F) * 16F) / 360F) + 0.5D) & 0xf);
        } else
        {
            world.func_688_b(i, j, k, Block.field_4068_aJ.blockID_00, l);
        }
        itemstack.stackSize--;
        TileEntitySign tileentitysign = (TileEntitySign)world.func_603_b(i, j, k);
        if(tileentitysign != null)
        {
            entityplayer.func_4052_a(tileentitysign);
        }
        return true;
    }
}
