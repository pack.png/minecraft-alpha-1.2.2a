package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityFish;
import net.minecraft.src.world.World;

public class ItemFishingRod extends Item
{

    public ItemFishingRod(int i)
    {
        super(i);
        maxDamage = 64;
    }

    public boolean func_4017_a()
    {
        return true;
    }

    public boolean func_4023_b()
    {
        return true;
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer)
    {
        if(entityplayer.field_4128_n != null)
        {
            int i = entityplayer.field_4128_n.func_4043_i();
            itemstack.damageItem(i);
            entityplayer.func_457_w();
        } else
        {
            world.playSoundAtEntity(entityplayer, "random.bow", 0.5F, 0.4F / (field_4015_b.nextFloat() * 0.4F + 0.8F));
            if(!world.multiplayerWorld)
            {
                world.entityJoinedWorld(new EntityFish(world, entityplayer));
            }
            entityplayer.func_457_w();
        }
        return itemstack;
    }
}
