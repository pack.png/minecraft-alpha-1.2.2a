package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet6SpawnPosition extends Packet
{

    public Packet6SpawnPosition()
    {
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        xPosition_01 = datainputstream.readInt();
        yPosition_00 = datainputstream.readInt();
        zPosition = datainputstream.readInt();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(xPosition_01);
        dataoutputstream.writeInt(yPosition_00);
        dataoutputstream.writeInt(zPosition);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleSpawnPosition(this);
    }

    public int getPacketSize()
    {
        return 12;
    }

    public int xPosition_01;
    public int yPosition_00;
    public int zPosition;
}
