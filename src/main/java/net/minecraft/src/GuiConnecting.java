package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;

public class GuiConnecting extends GuiScreen
{

    public GuiConnecting(Minecraft minecraft, String s, int i)
    {
        cancelled = false;
        minecraft.setWorld(null);
        (new ThreadConnectToServer(this, minecraft, s, i)).start();
    }

    public void updateScreen()
    {
        if(clientHandler != null)
        {
            clientHandler.func_848_a();
        }
    }

    protected void keyTyped(char c, int i)
    {
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Cancel"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(guibutton.id_02 == 0)
        {
            cancelled = true;
            if(clientHandler != null)
            {
                clientHandler.disconnect();
            }
            mc_06.displayGuiScreen(new GuiMainMenu());
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        if(clientHandler == null)
        {
            drawCenteredString(field_6451_g, "Connecting to the server...", width_02 / 2, height_02 / 2 - 50, 0xffffff);
            drawCenteredString(field_6451_g, "", width_02 / 2, height_02 / 2 - 10, 0xffffff);
        } else
        {
            drawCenteredString(field_6451_g, "Logging in...", width_02 / 2, height_02 / 2 - 50, 0xffffff);
            drawCenteredString(field_6451_g, clientHandler.field_1209_a, width_02 / 2, height_02 / 2 - 10, 0xffffff);
        }
        super.drawScreen(i, j, f);
    }

    static NetClientHandler setNetClientHandler(GuiConnecting guiconnecting, NetClientHandler netclienthandler)
    {
        return guiconnecting.clientHandler = netclienthandler;
    }

    static boolean isCancelled(GuiConnecting guiconnecting)
    {
        return guiconnecting.cancelled;
    }

    static NetClientHandler getNetClientHandler(GuiConnecting guiconnecting)
    {
        return guiconnecting.clientHandler;
    }

    private NetClientHandler clientHandler;
    private boolean cancelled;
}
