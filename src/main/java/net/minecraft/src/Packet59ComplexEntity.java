package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet59ComplexEntity extends Packet
{

    public Packet59ComplexEntity()
    {
        isChunkDataPacket = true;
    }

    public Packet59ComplexEntity(int i, int j, int k, TileEntity tileentity)
    {
        isChunkDataPacket = true;
        xPosition_07 = i;
        yPosition_06 = j;
        zPosition_06 = k;
        entityNBT = new NBTTagCompound();
        tileentity.writeToNBT_01(entityNBT);
        try
        {
            entityData = CompressedStreamTools.func_1142_a(entityNBT);
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        xPosition_07 = datainputstream.readInt();
        yPosition_06 = datainputstream.readShort();
        zPosition_06 = datainputstream.readInt();
        int i = datainputstream.readShort() & 0xffff;
        entityData = new byte[i];
        datainputstream.readFully(entityData);
        entityNBT = CompressedStreamTools.func_1140_a(entityData);
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(xPosition_07);
        dataoutputstream.writeShort(yPosition_06);
        dataoutputstream.writeInt(zPosition_06);
        dataoutputstream.writeShort((short)entityData.length);
        dataoutputstream.write(entityData);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleComplexEntity(this);
    }

    public int getPacketSize()
    {
        return entityData.length + 2 + 10;
    }

    public int xPosition_07;
    public int yPosition_06;
    public int zPosition_06;
    public byte entityData[];
    public NBTTagCompound entityNBT;
}
