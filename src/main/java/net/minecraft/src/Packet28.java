package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet28 extends Packet
{

    public Packet28()
    {
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        field_6367_a = datainputstream.readInt();
        field_6366_b = datainputstream.readShort();
        field_6369_c = datainputstream.readShort();
        field_6368_d = datainputstream.readShort();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(field_6367_a);
        dataoutputstream.writeShort(field_6366_b);
        dataoutputstream.writeShort(field_6369_c);
        dataoutputstream.writeShort(field_6368_d);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.func_6498_a(this);
    }

    public int getPacketSize()
    {
        return 10;
    }

    public int field_6367_a;
    public int field_6366_b;
    public int field_6369_c;
    public int field_6368_d;
}
