package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

class SlotArmor extends SlotInventory
{

    SlotArmor(GuiInventory guiinventory, GuiContainer guicontainer, IInventory iinventory, int i, int j, int k, int l)
    {
        super(guicontainer, iinventory, i, j, k);
        field_1123_d = guiinventory;
        field_1124_c = l;
    }

    public int func_4104_e()
    {
        return 1;
    }

    public boolean func_4105_a(ItemStack itemstack)
    {
        if(itemstack.getItem() instanceof ItemArmor)
        {
            return ((ItemArmor)itemstack.getItem()).field_313_aX == field_1124_c;
        }
        System.out.println((new StringBuilder()).append(itemstack.getItem().swiftedIndex).append(", ").append(field_1124_c).toString());
        if(itemstack.getItem().swiftedIndex == Block.field_4055_bb.blockID_00)
        {
            return field_1124_c == 0;
        } else
        {
            return false;
        }
    }

    public int func_775_c()
    {
        return 15 + field_1124_c * 16;
    }

    final int field_1124_c; /* synthetic field */
    final GuiInventory field_1123_d; /* synthetic field */
}
