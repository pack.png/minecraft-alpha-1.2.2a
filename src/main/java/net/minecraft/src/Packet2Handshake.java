package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet2Handshake extends Packet
{

    public Packet2Handshake()
    {
    }

    public Packet2Handshake(String s)
    {
        username_00 = s;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        username_00 = datainputstream.readUTF();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeUTF(username_00);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleHandshake(this);
    }

    public int getPacketSize()
    {
        return 4 + username_00.length() + 4;
    }

    public String username_00;
}
