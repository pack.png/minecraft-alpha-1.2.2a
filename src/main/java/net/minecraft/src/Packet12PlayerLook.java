package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet12PlayerLook extends Packet10Flying
{

    public Packet12PlayerLook()
    {
        rotating_00 = true;
    }

    public Packet12PlayerLook(float f, float f1, boolean flag)
    {
        yaw_01 = f;
        pitch_02 = f1;
        onGround = flag;
        rotating_00 = true;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        yaw_01 = datainputstream.readFloat();
        pitch_02 = datainputstream.readFloat();
        super.readPacketData(datainputstream);
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeFloat(yaw_01);
        dataoutputstream.writeFloat(pitch_02);
        super.writePacketData(dataoutputstream);
    }

    public int getPacketSize()
    {
        return 9;
    }
}
