package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet14BlockDig extends Packet
{

    public Packet14BlockDig()
    {
    }

    public Packet14BlockDig(int i, int j, int k, int l, int i1)
    {
        status = i;
        xPosition_10 = j;
        yPosition_08 = k;
        zPosition_09 = l;
        face = i1;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        status = datainputstream.read();
        xPosition_10 = datainputstream.readInt();
        yPosition_08 = datainputstream.read();
        zPosition_09 = datainputstream.readInt();
        face = datainputstream.read();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.write(status);
        dataoutputstream.writeInt(xPosition_10);
        dataoutputstream.write(yPosition_08);
        dataoutputstream.writeInt(zPosition_09);
        dataoutputstream.write(face);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleBlockDig(this);
    }

    public int getPacketSize()
    {
        return 11;
    }

    public int xPosition_10;
    public int yPosition_08;
    public int zPosition_09;
    public int face;
    public int status;
}
