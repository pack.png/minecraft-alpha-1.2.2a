package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityItem;

import java.io.*;

public class Packet21PickupSpawn extends Packet
{

    public Packet21PickupSpawn()
    {
    }

    public Packet21PickupSpawn(EntityItem entityitem)
    {
        entityId_01 = entityitem.field_620_ab;
        itemId = entityitem.item.itemID;
        count = entityitem.item.stackSize;
        xPosition_02 = MathHelper.convertToBlockCoord_00(entityitem.posX * 32D);
        yPosition_02 = MathHelper.convertToBlockCoord_00(entityitem.posY * 32D);
        zPosition_01 = MathHelper.convertToBlockCoord_00(entityitem.posZ * 32D);
        rotation = (byte)(int)(entityitem.motionX * 128D);
        pitch_03 = (byte)(int)(entityitem.motionY * 128D);
        roll = (byte)(int)(entityitem.motionZ * 128D);
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_01 = datainputstream.readInt();
        itemId = datainputstream.readShort();
        count = datainputstream.readByte();
        xPosition_02 = datainputstream.readInt();
        yPosition_02 = datainputstream.readInt();
        zPosition_01 = datainputstream.readInt();
        rotation = datainputstream.readByte();
        pitch_03 = datainputstream.readByte();
        roll = datainputstream.readByte();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_01);
        dataoutputstream.writeShort(itemId);
        dataoutputstream.writeByte(count);
        dataoutputstream.writeInt(xPosition_02);
        dataoutputstream.writeInt(yPosition_02);
        dataoutputstream.writeInt(zPosition_01);
        dataoutputstream.writeByte(rotation);
        dataoutputstream.writeByte(pitch_03);
        dataoutputstream.writeByte(roll);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handlePickupSpawn(this);
    }

    public int getPacketSize()
    {
        return 22;
    }

    public int entityId_01;
    public int xPosition_02;
    public int yPosition_02;
    public int zPosition_01;
    public byte rotation;
    public byte pitch_03;
    public byte roll;
    public int itemId;
    public int count;
}
