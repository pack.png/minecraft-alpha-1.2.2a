package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import net.minecraft.src.entity.Entity;
import net.minecraft.src.world.World;

public class PlayerController
{

    public PlayerController(Minecraft minecraft)
    {
        field_1064_b = false;
        mc_04 = minecraft;
    }

    public void func_717_a(World world)
    {
    }

    public void func_719_a(int i, int j, int k, int l)
    {
        func_729_b(i, j, k, l);
    }

    public boolean func_729_b(int i, int j, int k, int l)
    {
        mc_04.effectRenderer.func_1186_a(i, j, k);
        World world = mc_04.world;
        Block block = Block.blocksList[world.getBlockId(i, j, k)];
        int i1 = world.getBlockMetadata_01(i, j, k);
        boolean flag = world.setBlockWithNotify(i, j, k, 0);
        if(block != null && flag)
        {
            mc_04.soundManager.func_336_b(block.stepSound.func_1146_a(), (float)i + 0.5F, (float)j + 0.5F, (float)k + 0.5F, (block.stepSound.func_1147_b() + 1.0F) / 2.0F, block.stepSound.func_1144_c() * 0.8F);
            block.onBlockDestroyedByPlayer(world, i, j, k, i1);
        }
        return flag;
    }

    public void func_6470_c(int i, int j, int k, int l)
    {
    }

    public void func_6468_a()
    {
    }

    public void func_6467_a(float f)
    {
    }

    public float func_727_b()
    {
        return 5F;
    }

    public boolean func_6471_a(EntityPlayer entityplayer, World world, ItemStack itemstack)
    {
        int i = itemstack.stackSize;
        ItemStack itemstack1 = itemstack.useItemRightClick(world, entityplayer);
        if(itemstack1 != itemstack || itemstack1 != null && itemstack1.stackSize != i)
        {
            entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem_00] = itemstack1;
            if(itemstack1.stackSize == 0)
            {
                entityplayer.inventory.mainInventory[entityplayer.inventory.currentItem_00] = null;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public void func_6476_a(EntityPlayer entityplayer)
    {
    }

    public void func_6474_c()
    {
    }

    public boolean func_6469_d()
    {
        return true;
    }

    public void func_6473_b(EntityPlayer entityplayer)
    {
    }

    public boolean func_722_a(EntityPlayer entityplayer, World world, ItemStack itemstack, int i, int j, int k, int l)
    {
        int i1 = world.getBlockId(i, j, k);
        if(i1 > 0 && Block.blocksList[i1].blockActivated(world, i, j, k, entityplayer))
        {
            return true;
        }
        if(itemstack == null)
        {
            return false;
        } else
        {
            return itemstack.useItem(entityplayer, world, i, j, k, l);
        }
    }

    public EntityPlayer func_4087_b(World world)
    {
        return new EntityPlayerSP(mc_04, world, mc_04.field_6320_i, world.field_4209_q.field_4218_e);
    }

    public void func_6475_a(EntityPlayer entityplayer, Entity entity)
    {
        entityplayer.func_6415_a_(entity);
    }

    public void func_6472_b(EntityPlayer entityplayer, Entity entity)
    {
        entityplayer.func_463_a(entity);
    }

    protected final Minecraft mc_04;
    public boolean field_1064_b;
}
