package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.world.World;

import java.util.HashMap;
import java.util.Map;

public class TileEntity
{

    public TileEntity()
    {
    }

    private static void addMapping(Class class1, String s)
    {
        if(classToNameMap.containsKey(s))
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Duplicate id: ").append(s).toString());
        } else
        {
            nameToClassMap.put(s, class1);
            classToNameMap.put(class1, s);
            return;
        }
    }

    public void readFromNBT_01(NBTTagCompound nbttagcompound)
    {
        xCoord_01 = nbttagcompound.getInteger("x");
        yCoord_01 = nbttagcompound.getInteger("y");
        zCoord_01 = nbttagcompound.getInteger("z");
    }

    public void writeToNBT_01(NBTTagCompound nbttagcompound)
    {
        String s = (String)classToNameMap.get(getClass());
        if(s == null)
        {
            throw new RuntimeException((new StringBuilder()).append(getClass()).append(" is missing a mapping! This is a bug!").toString());
        } else
        {
            nbttagcompound.setString("id", s);
            nbttagcompound.setInteger("x", xCoord_01);
            nbttagcompound.setInteger("y", yCoord_01);
            nbttagcompound.setInteger("z", zCoord_01);
            return;
        }
    }

    public void updateEntity()
    {
    }

    public static TileEntity createAndLoadEntity(NBTTagCompound nbttagcompound)
    {
        TileEntity tileentity = null;
        try
        {
            Class class1 = (Class)nameToClassMap.get(nbttagcompound.getString("id"));
            if(class1 != null)
            {
                tileentity = (TileEntity)class1.newInstance();
            }
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        if(tileentity != null)
        {
            tileentity.readFromNBT_01(nbttagcompound);
        } else
        {
            System.out.println((new StringBuilder()).append("Skipping TileEntity with id ").append(nbttagcompound.getString("id")).toString());
        }
        return tileentity;
    }

    public int getBlockMetadata()
    {
        return worldObj_01.getBlockMetadata_01(xCoord_01, yCoord_01, zCoord_01);
    }

    public void func_474_j_()
    {
        worldObj_01.func_698_b(xCoord_01, yCoord_01, zCoord_01, this);
    }

    public double getDistanceFrom(double d, double d1, double d2)
    {
        double d3 = ((double)xCoord_01 + 0.5D) - d;
        double d4 = ((double)yCoord_01 + 0.5D) - d1;
        double d5 = ((double)zCoord_01 + 0.5D) - d2;
        return d3 * d3 + d4 * d4 + d5 * d5;
    }

    public Block getBlockType()
    {
        return Block.blocksList[worldObj_01.getBlockId(xCoord_01, yCoord_01, zCoord_01)];
    }

    static Class _mthclass$(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch(ClassNotFoundException classnotfoundexception)
        {
            throw new NoClassDefFoundError(classnotfoundexception.getMessage());
        }
    }

    private static Map nameToClassMap = new HashMap();
    private static Map classToNameMap = new HashMap();
    public World worldObj_01;
    public int xCoord_01;
    public int yCoord_01;
    public int zCoord_01;

    static 
    {
        addMapping(TileEntityFurnace.class, "Furnace");
        addMapping(TileEntityChest.class, "Chest");
        addMapping(TileEntitySign.class, "Sign");
        addMapping(TileEntityMobSpawner.class, "MobSpawner");
    }
}
