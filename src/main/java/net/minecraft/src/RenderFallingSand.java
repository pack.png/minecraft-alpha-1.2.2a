package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.Entity;
import net.minecraft.src.entity.EntityFallingSand;
import net.minecraft.src.world.World;
import org.lwjgl.opengl.GL11;

public class RenderFallingSand extends Render
{

    public RenderFallingSand()
    {
        field_197_d = new RenderBlocks();
        field_190_b = 0.5F;
    }

    public void func_156_a(EntityFallingSand entityfallingsand, double d, double d1, double d2,
                           float f, float f1)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef((float)d, (float)d1, (float)d2);
        func_151_a("/terrain.png");
        Block block = Block.blocksList[entityfallingsand.field_799_a];
        World world = entityfallingsand.func_465_i();
        GL11.glDisable(2896);
        field_197_d.func_1243_a(block, world, MathHelper.convertToBlockCoord_00(entityfallingsand.posX), MathHelper.convertToBlockCoord_00(entityfallingsand.posY), MathHelper.convertToBlockCoord_00(entityfallingsand.posZ));
        GL11.glEnable(2896);
        GL11.glPopMatrix();
    }

    public void func_147_a(Entity entity, double d, double d1, double d2,
                           float f, float f1)
    {
        func_156_a((EntityFallingSand)entity, d, d1, d2, f, f1);
    }

    private RenderBlocks field_197_d;
}
