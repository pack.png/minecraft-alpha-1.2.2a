package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityLiving;

import java.io.*;

public class Packet24MobSpawn extends Packet
{

    public Packet24MobSpawn()
    {
    }

    public Packet24MobSpawn(EntityLiving entityliving)
    {
        entityId_03 = entityliving.field_620_ab;
        type_00 = (byte)EntityList.func_1082_a(entityliving);
        xPosition_09 = MathHelper.convertToBlockCoord_00(entityliving.posX * 32D);
        yPosition_07 = MathHelper.convertToBlockCoord_00(entityliving.posY * 32D);
        zPosition_08 = MathHelper.convertToBlockCoord_00(entityliving.posZ * 32D);
        yaw_02 = (byte)(int)((entityliving.rotationYaw * 256F) / 360F);
        pitch_04 = (byte)(int)((entityliving.rotationPitch * 256F) / 360F);
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_03 = datainputstream.readInt();
        type_00 = datainputstream.readByte();
        xPosition_09 = datainputstream.readInt();
        yPosition_07 = datainputstream.readInt();
        zPosition_08 = datainputstream.readInt();
        yaw_02 = datainputstream.readByte();
        pitch_04 = datainputstream.readByte();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_03);
        dataoutputstream.writeByte(type_00);
        dataoutputstream.writeInt(xPosition_09);
        dataoutputstream.writeInt(yPosition_07);
        dataoutputstream.writeInt(zPosition_08);
        dataoutputstream.writeByte(yaw_02);
        dataoutputstream.writeByte(pitch_04);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleMobSpawn(this);
    }

    public int getPacketSize()
    {
        return 19;
    }

    public int entityId_03;
    public byte type_00;
    public int xPosition_09;
    public int yPosition_07;
    public int zPosition_08;
    public byte yaw_02;
    public byte pitch_04;
}
