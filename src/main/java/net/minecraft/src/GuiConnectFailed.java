package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiConnectFailed extends GuiScreen
{

    public GuiConnectFailed(String s, String s1)
    {
        errorMessage = s;
        errorDetail = s1;
    }

    public void updateScreen()
    {
    }

    protected void keyTyped(char c, int i)
    {
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Back to title screen"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(guibutton.id_02 == 0)
        {
            mc_06.displayGuiScreen(new GuiMainMenu());
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, errorMessage, width_02 / 2, height_02 / 2 - 50, 0xffffff);
        drawCenteredString(field_6451_g, errorDetail, width_02 / 2, height_02 / 2 - 10, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private String errorMessage;
    private String errorDetail;
}
