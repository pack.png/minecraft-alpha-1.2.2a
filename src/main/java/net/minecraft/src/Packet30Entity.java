package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet30Entity extends Packet
{

    public Packet30Entity()
    {
        rotating = false;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_02 = datainputstream.readInt();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_02);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleEntity(this);
    }

    public int getPacketSize()
    {
        return 4;
    }

    public int entityId_02;
    public byte xPosition_08;
    public byte yPosition_05;
    public byte zPosition_07;
    public byte yaw_00;
    public byte pitch_01;
    public boolean rotating;
}
