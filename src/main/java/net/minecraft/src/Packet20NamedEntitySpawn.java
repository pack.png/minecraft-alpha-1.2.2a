package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet20NamedEntitySpawn extends Packet
{

    public Packet20NamedEntitySpawn()
    {
    }

    public Packet20NamedEntitySpawn(EntityPlayer entityplayer)
    {
        entityId_00 = entityplayer.field_620_ab;
        name_00 = entityplayer.field_771_i;
        xPosition_15 = MathHelper.convertToBlockCoord_00(entityplayer.posX * 32D);
        yPosition_03 = MathHelper.convertToBlockCoord_00(entityplayer.posY * 32D);
        zPosition_02 = MathHelper.convertToBlockCoord_00(entityplayer.posZ * 32D);
        rotation_00 = (byte)(int)((entityplayer.rotationYaw * 256F) / 360F);
        pitch_00 = (byte)(int)((entityplayer.rotationPitch * 256F) / 360F);
        ItemStack itemstack = entityplayer.inventory.getCurrentItem();
        currentItem = itemstack != null ? itemstack.itemID : 0;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_00 = datainputstream.readInt();
        name_00 = datainputstream.readUTF();
        xPosition_15 = datainputstream.readInt();
        yPosition_03 = datainputstream.readInt();
        zPosition_02 = datainputstream.readInt();
        rotation_00 = datainputstream.readByte();
        pitch_00 = datainputstream.readByte();
        currentItem = datainputstream.readShort();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_00);
        dataoutputstream.writeUTF(name_00);
        dataoutputstream.writeInt(xPosition_15);
        dataoutputstream.writeInt(yPosition_03);
        dataoutputstream.writeInt(zPosition_02);
        dataoutputstream.writeByte(rotation_00);
        dataoutputstream.writeByte(pitch_00);
        dataoutputstream.writeShort(currentItem);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleNamedEntitySpawn(this);
    }

    public int getPacketSize()
    {
        return 28;
    }

    public int entityId_00;
    public String name_00;
    public int xPosition_15;
    public int yPosition_03;
    public int zPosition_02;
    public byte rotation_00;
    public byte pitch_00;
    public int currentItem;
}
