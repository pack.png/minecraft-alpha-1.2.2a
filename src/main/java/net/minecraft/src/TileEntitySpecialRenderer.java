package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public abstract class TileEntitySpecialRenderer
{

    public TileEntitySpecialRenderer()
    {
    }

    public abstract void func_930_a(TileEntity tileentity, double d, double d1, double d2, 
            float f);

    protected void func_6507_a(String s)
    {
        RenderEngine renderengine = field_6509_a.field_1550_e;
        renderengine.bindTexture(renderengine.getTexture(s));
    }

    public void func_928_a(TileEntityRenderer tileentityrenderer)
    {
        field_6509_a = tileentityrenderer;
    }

    public FontRenderer func_6508_a()
    {
        return field_6509_a.func_6516_a();
    }

    protected TileEntityRenderer field_6509_a;
}
