package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.entity.EntityLiving;
import net.minecraft.src.world.World;

import java.util.Random;

public class BlockFurnace extends BlockContainer
{

    protected BlockFurnace(int i, boolean flag)
    {
        super(i, Material.rock);
        field_456_a = flag;
        blockIndexInTexture = 45;
    }

    public int idDropped(int i, Random random)
    {
        return Block.stoneOvenIdle.blockID_00;
    }

    public void onBlockAdded(World world, int i, int j, int k)
    {
        super.onBlockAdded(world, i, j, k);
        func_284_h(world, i, j, k);
    }

    private void func_284_h(World world, int i, int j, int k)
    {
        int l = world.getBlockId(i, j, k - 1);
        int i1 = world.getBlockId(i, j, k + 1);
        int j1 = world.getBlockId(i - 1, j, k);
        int k1 = world.getBlockId(i + 1, j, k);
        byte byte0 = 3;
        if(Block.field_343_p[l] && !Block.field_343_p[i1])
        {
            byte0 = 3;
        }
        if(Block.field_343_p[i1] && !Block.field_343_p[l])
        {
            byte0 = 2;
        }
        if(Block.field_343_p[j1] && !Block.field_343_p[k1])
        {
            byte0 = 5;
        }
        if(Block.field_343_p[k1] && !Block.field_343_p[j1])
        {
            byte0 = 4;
        }
        world.setBlockMetadataWithNotify(i, j, k, byte0);
    }

    public int getBlockTexture(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        if(l == 1)
        {
            return Block.stone.blockIndexInTexture;
        }
        if(l == 0)
        {
            return Block.stone.blockIndexInTexture;
        }
        int i1 = iblockaccess.getBlockMetadata_01(i, j, k);
        if(l != i1)
        {
            return blockIndexInTexture;
        }
        if(field_456_a)
        {
            return blockIndexInTexture + 16;
        } else
        {
            return blockIndexInTexture - 1;
        }
    }

    public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
        if(!field_456_a)
        {
            return;
        }
        int l = world.getBlockMetadata_01(i, j, k);
        float f = (float)i + 0.5F;
        float f1 = (float)j + 0.0F + (random.nextFloat() * 6F) / 16F;
        float f2 = (float)k + 0.5F;
        float f3 = 0.52F;
        float f4 = random.nextFloat() * 0.6F - 0.3F;
        if(l == 4)
        {
            world.spawnParticle_00("smoke", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.spawnParticle_00("flame", f - f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
        } else
        if(l == 5)
        {
            world.spawnParticle_00("smoke", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
            world.spawnParticle_00("flame", f + f3, f1, f2 + f4, 0.0D, 0.0D, 0.0D);
        } else
        if(l == 2)
        {
            world.spawnParticle_00("smoke", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
            world.spawnParticle_00("flame", f + f4, f1, f2 - f3, 0.0D, 0.0D, 0.0D);
        } else
        if(l == 3)
        {
            world.spawnParticle_00("smoke", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
            world.spawnParticle_00("flame", f + f4, f1, f2 + f3, 0.0D, 0.0D, 0.0D);
        }
    }

    public int getBlockTextureFromSide(int i)
    {
        if(i == 1)
        {
            return Block.stone.blockID_00;
        }
        if(i == 0)
        {
            return Block.stone.blockID_00;
        }
        if(i == 3)
        {
            return blockIndexInTexture - 1;
        } else
        {
            return blockIndexInTexture;
        }
    }

    public boolean blockActivated(World world, int i, int j, int k, EntityPlayer entityplayer)
    {
        TileEntityFurnace tileentityfurnace = (TileEntityFurnace)world.func_603_b(i, j, k);
        entityplayer.func_453_a(tileentityfurnace);
        return true;
    }

    public static void func_285_a(boolean flag, World world, int i, int j, int k)
    {
        int l = world.getBlockMetadata_01(i, j, k);
        TileEntity tileentity = world.func_603_b(i, j, k);
        if(flag)
        {
            world.setBlockWithNotify(i, j, k, Block.stoneOvenActive.blockID_00);
        } else
        {
            world.setBlockWithNotify(i, j, k, Block.stoneOvenIdle.blockID_00);
        }
        world.setBlockMetadataWithNotify(i, j, k, l);
        world.func_654_a(i, j, k, tileentity);
    }

    protected TileEntity func_283_a_()
    {
        return new TileEntityFurnace();
    }

    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLiving entityliving)
    {
        int l = MathHelper.convertToBlockCoord_00((double)((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
        if(l == 0)
        {
            world.setBlockMetadataWithNotify(i, j, k, 2);
        }
        if(l == 1)
        {
            world.setBlockMetadataWithNotify(i, j, k, 5);
        }
        if(l == 2)
        {
            world.setBlockMetadataWithNotify(i, j, k, 3);
        }
        if(l == 3)
        {
            world.setBlockMetadataWithNotify(i, j, k, 4);
        }
    }

    private final boolean field_456_a;
}
