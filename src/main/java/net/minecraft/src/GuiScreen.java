package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class    GuiScreen extends Gui
{

    public GuiScreen()
    {
        controlList = new ArrayList();
        field_948_f = false;
        field_946_a = null;
    }

    public void drawScreen(int i, int j, float f)
    {
        for(int k = 0; k < controlList.size(); k++)
        {
            GuiButton guibutton = (GuiButton)controlList.get(k);
            guibutton.drawButton(mc_06, i, j);
        }

    }

    protected void keyTyped(char c, int i)
    {
        if(i == 1)
        {
            mc_06.displayGuiScreen(null);
            mc_06.func_6259_e();
        }
    }

    public static String getClipboardString()
    {
        try
        {
            Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
            if(transferable != null && transferable.isDataFlavorSupported(DataFlavor.stringFlavor))
            {
                String s = (String)transferable.getTransferData(DataFlavor.stringFlavor);
                return s;
            }
        }
        catch(Exception exception) { }
        return null;
    }

    protected void mouseClicked(int i, int j, int k)
    {
        if(k == 0)
        {
            for(int l = 0; l < controlList.size(); l++)
            {
                GuiButton guibutton = (GuiButton)controlList.get(l);
                if(guibutton.mousePressed(mc_06, i, j))
                {
                    field_946_a = guibutton;
                    mc_06.soundManager.func_337_a("random.click", 1.0F, 1.0F);
                    actionPerformed(guibutton);
                }
            }

        }
    }

    protected void mouseMoved(int i, int j, int k)
    {
        if(field_946_a != null && k == 0)
        {
            field_946_a.mouseReleased(i, j);
            field_946_a = null;
        }
    }

    protected void actionPerformed(GuiButton guibutton)
    {
    }

    public void func_6447_a(Minecraft minecraft, int i, int j)
    {
        mc_06 = minecraft;
        field_6451_g = minecraft.fontRenderer;
        width_02 = i;
        height_02 = j;
        controlList.clear();
        initGui();
    }

    public void initGui()
    {
    }

    public void handleInput()
    {
        for(; Mouse.next(); handleMouseInput()) { }
        for(; Keyboard.next(); handleKeyboardInput()) { }
    }

    public void handleMouseInput()
    {
        if(Mouse.getEventButtonState())
        {
            int i = (Mouse.getEventX() * width_02) / mc_06.field_6326_c;
            int k = height_02 - (Mouse.getEventY() * height_02) / mc_06.field_6325_d - 1;
            mouseClicked(i, k, Mouse.getEventButton());
        } else
        {
            int j = (Mouse.getEventX() * width_02) / mc_06.field_6326_c;
            int l = height_02 - (Mouse.getEventY() * height_02) / mc_06.field_6325_d - 1;
            mouseMoved(j, l, Mouse.getEventButton());
        }
    }

    public void handleKeyboardInput()
    {
        if(Keyboard.getEventKeyState())
        {
            if(Keyboard.getEventKey() == 87)
            {
                mc_06.func_6270_h();
                return;
            }
            keyTyped(Keyboard.getEventCharacter(), Keyboard.getEventKey());
        }
    }

    public void updateScreen()
    {
    }

    public void func_6449_h()
    {
    }

    public void drawBackground()
    {
        drawBackground(0);
    }

    public void drawBackground(int i)
    {
        if(mc_06.world != null)
        {
            drawGradientRect(0, 0, width_02, height_02, 0xc0101010, 0xd0101010);
        } else
        {
            drawDirtBackground(i);
        }
    }

    public void drawDirtBackground(int i)
    {
        GL11.glDisable(2896);
        GL11.glDisable(2912);
        Tessellator tessellator = Tessellator.instance;
        GL11.glBindTexture(3553, mc_06.renderEngine.getTexture("/gui/background.png"));
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        float f = 32F;
        tessellator.startDrawingQuads();
        tessellator.setColorOpaque_I(0x404040);
        tessellator.addVertexWithUV(0.0D, height_02, 0.0D, 0.0D, (float)height_02 / f + (float)i);
        tessellator.addVertexWithUV(width_02, height_02, 0.0D, (float)width_02 / f, (float)height_02 / f + (float)i);
        tessellator.addVertexWithUV(width_02, 0.0D, 0.0D, (float)width_02 / f, 0 + i);
        tessellator.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0 + i);
        tessellator.draw();
    }

    public boolean func_6450_b()
    {
        return true;
    }

    public void deleteWorld_00(boolean flag, int i)
    {
    }

    protected Minecraft mc_06;
    public int width_02;
    public int height_02;
    protected java.util.List controlList;
    public boolean field_948_f;
    protected FontRenderer field_6451_g;
    private GuiButton field_946_a;
}
