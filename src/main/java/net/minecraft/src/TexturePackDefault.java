package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

public class TexturePackDefault extends TexturePackBase
{

    public TexturePackDefault()
    {
        field_6491_e = -1;
        field_6487_a = "Default";
        field_6486_b = "The default look of Minecraft";
        try
        {
            field_6490_f = ImageIO.read((TexturePackDefault.class).getResource("/pack.png"));
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
    }

    public void func_6484_b(Minecraft minecraft)
    {
        if(field_6490_f != null)
        {
            minecraft.renderEngine.func_1078_a(field_6491_e);
        }
    }

    public void func_6483_c(Minecraft minecraft)
    {
        if(field_6490_f != null && field_6491_e < 0)
        {
            field_6491_e = minecraft.renderEngine.func_1074_a(field_6490_f);
        }
        if(field_6490_f != null)
        {
            minecraft.renderEngine.bindTexture(field_6491_e);
        } else
        {
            GL11.glBindTexture(3553, minecraft.renderEngine.getTexture("/gui/unknown_pack.png"));
        }
    }

    private int field_6491_e;
    private BufferedImage field_6490_f;
}
