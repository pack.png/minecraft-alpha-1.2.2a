package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


class NetworkMasterThread extends Thread
{

    NetworkMasterThread(NetworkManager networkmanager)
    {
        netManager_00 = networkmanager;
    }

    public void run()
    {
        try
        {
            Thread.sleep(5000L);
            if(NetworkManager.getReadThread(netManager_00).isAlive())
            {
                try
                {
                    NetworkManager.getReadThread(netManager_00).stop();
                }
                catch(Throwable throwable) { }
            }
            if(NetworkManager.getWriteThread(netManager_00).isAlive())
            {
                try
                {
                    NetworkManager.getWriteThread(netManager_00).stop();
                }
                catch(Throwable throwable1) { }
            }
        }
        catch(InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
    }

    final NetworkManager netManager_00; /* synthetic field */
}
