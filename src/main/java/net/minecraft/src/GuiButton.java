package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

public class GuiButton extends Gui
{

    public GuiButton(int id, int x, int y, String text)
    {
        this(id, x, y, 200, 20, text);
    }

    protected GuiButton(int id, int x, int y, int width, int height, String text)
    {
        width_01 = 200;
        height_00 = 20;
        enabled = true;
        enabled2 = true;
        id_02 = id;
        xPosition_11 = x;
        yPosition_09 = y;
        width_01 = width;
        height_00 = height;
        displayString = text;
    }

    protected int getHoverState(boolean flag)
    {
        byte byte0 = 1;
        if(!enabled)
        {
            byte0 = 0;
        } else
        if(flag)
        {
            byte0 = 2;
        }
        return byte0;
    }

    public void drawButton(Minecraft minecraft, int i, int j)
    {
        if(!enabled2)
        {
            return;
        }
        FontRenderer fontrenderer = minecraft.fontRenderer;
        GL11.glBindTexture(3553, minecraft.renderEngine.getTexture("/gui/gui.png"));
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        boolean flag = i >= xPosition_11 && j >= yPosition_09 && i < xPosition_11 + width_01 && j < yPosition_09 + height_00;
        int k = getHoverState(flag);
        drawTexturedModalRect(xPosition_11, yPosition_09, 0, 46 + k * 20, width_01 / 2, height_00);
        drawTexturedModalRect(xPosition_11 + width_01 / 2, yPosition_09, 200 - width_01 / 2, 46 + k * 20, width_01 / 2, height_00);
        mouseDragged(minecraft, i, j);
        if(!enabled)
        {
            drawCenteredString(fontrenderer, displayString, xPosition_11 + width_01 / 2, yPosition_09 + (height_00 - 8) / 2, 0xffa0a0a0);
        } else
        if(flag)
        {
            drawCenteredString(fontrenderer, displayString, xPosition_11 + width_01 / 2, yPosition_09 + (height_00 - 8) / 2, 0xffffa0);
        } else
        {
            drawCenteredString(fontrenderer, displayString, xPosition_11 + width_01 / 2, yPosition_09 + (height_00 - 8) / 2, 0xe0e0e0);
        }
    }

    protected void mouseDragged(Minecraft minecraft, int i, int j)
    {
    }

    public void mouseReleased(int i, int j)
    {
    }

    public boolean mousePressed(Minecraft minecraft, int i, int j)
    {
        return enabled && i >= xPosition_11 && j >= yPosition_09 && i < xPosition_11 + width_01 && j < yPosition_09 + height_00;
    }

    protected int width_01;
    protected int height_00;
    public int xPosition_11;
    public int yPosition_09;
    public String displayString;
    public int id_02;
    public boolean enabled;
    public boolean enabled2;
}
