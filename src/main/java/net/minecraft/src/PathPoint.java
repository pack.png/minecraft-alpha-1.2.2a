package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class PathPoint
{

    public PathPoint(int i, int j, int k)
    {
        index = -1;
        isFirst = false;
        xCoord_02 = i;
        yCoord_02 = j;
        zCoord_02 = k;
        hash = i | j << 10 | k << 20;
    }

    public float distanceTo(PathPoint pathpoint)
    {
        float f = pathpoint.xCoord_02 - xCoord_02;
        float f1 = pathpoint.yCoord_02 - yCoord_02;
        float f2 = pathpoint.zCoord_02 - zCoord_02;
        return MathHelper.sqrt_float(f * f + f1 * f1 + f2 * f2);
    }

    public boolean equals(Object obj)
    {
        return ((PathPoint)obj).hash == hash;
    }

    public int hashCode()
    {
        return hash;
    }

    public boolean isAssigned()
    {
        return index >= 0;
    }

    public String toString()
    {
        return (new StringBuilder()).append(xCoord_02).append(", ").append(yCoord_02).append(", ").append(zCoord_02).toString();
    }

    public final int xCoord_02;
    public final int yCoord_02;
    public final int zCoord_02;
    public final int hash;
    int index;
    float totalPathDistance;
    float distanceToNext;
    float distanceToTarget;
    PathPoint previous;
    public boolean isFirst;
}
