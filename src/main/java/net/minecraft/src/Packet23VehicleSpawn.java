package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet23VehicleSpawn extends Packet
{

    public Packet23VehicleSpawn()
    {
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        entityId_05 = datainputstream.readInt();
        type_02 = datainputstream.readByte();
        xPosition_05 = datainputstream.readInt();
        yPosition_13 = datainputstream.readInt();
        zPosition_13 = datainputstream.readInt();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(entityId_05);
        dataoutputstream.writeByte(type_02);
        dataoutputstream.writeInt(xPosition_05);
        dataoutputstream.writeInt(yPosition_13);
        dataoutputstream.writeInt(zPosition_13);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.handleVehicleSpawn(this);
    }

    public int getPacketSize()
    {
        return 17;
    }

    public int entityId_05;
    public int xPosition_05;
    public int yPosition_13;
    public int zPosition_13;
    public int type_02;
}
