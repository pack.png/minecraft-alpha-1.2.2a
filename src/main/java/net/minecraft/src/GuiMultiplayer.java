package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiMultiplayer extends GuiScreen
{

    public GuiMultiplayer(GuiScreen guiscreen)
    {
        parentScreen_03 = 0;
        serverAddress = "";
        updateCounter_01 = guiscreen;
    }

    public void updateScreen()
    {
        parentScreen_03++;
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(0, width_02 / 2 - 100, height_02 / 4 + 96 + 12, "Connect"));
        controlList.add(new GuiButton(1, width_02 / 2 - 100, height_02 / 4 + 120 + 12, "Cancel"));
        ((GuiButton)controlList.get(0)).enabled = false;
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(!guibutton.enabled)
        {
            return;
        }
        if(guibutton.id_02 == 1)
        {
            mc_06.displayGuiScreen(updateCounter_01);
        } else
        if(guibutton.id_02 == 0)
        {
            String as[] = serverAddress.split(":");
            mc_06.displayGuiScreen(new GuiConnecting(mc_06, as[0], as.length <= 1 ? 25565 : func_4067_a(as[1], 25565)));
        }
    }

    private int func_4067_a(String s, int i)
    {
        try
        {
            return Integer.parseInt(s.trim());
        }
        catch(Exception exception)
        {
            return i;
        }
    }

    protected void keyTyped(char c, int i)
    {
        if(c == '\026')
        {
            String s;
            int j;
            s = GuiScreen.getClipboardString();
            if(s == null)
            {
                s = "";
            }
            j = 32 - serverAddress.length();
            if(j > s.length())
            {
                j = s.length();
            }
            if(j > 0)
            {
            	serverAddress += s.substring(0, j);
            }
        }
        if(c == '\r')
        {
            actionPerformed((GuiButton)controlList.get(0));
        }
        if(i == 14 && serverAddress.length() > 0)
        {
            serverAddress = serverAddress.substring(0, serverAddress.length() - 1);
        }
        if(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_'abcdefghijklmnopqrstuvwxyz{|}~\u2302\307\374\351\342\344\340\345\347\352\353\350\357\356\354\304\305\311\346\306\364\366\362\373\371\377\326\334\370\243\330\327\u0192\341\355\363\372\361\321\252\272\277\256\254\275\274\241\253\273".indexOf(c) >= 0 && serverAddress.length() < 32)
        {
        	serverAddress += c;
        }
        ((GuiButton)controlList.get(0)).enabled = serverAddress.length() > 0;
        return;
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, "Play Multiplayer", width_02 / 2, (height_02 / 4 - 60) + 20, 0xffffff);
        drawString(field_6451_g, "Minecraft Multiplayer is currently not finished, but there", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 0, 0xa0a0a0);
        drawString(field_6451_g, "is some buggy early testing going on.", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 9, 0xa0a0a0);
        drawString(field_6451_g, "Enter the IP of a server to connect to it:", width_02 / 2 - 140, (height_02 / 4 - 60) + 60 + 36, 0xa0a0a0);
        int k = width_02 / 2 - 100;
        int l = (height_02 / 4 - 10) + 50 + 18;
        char c = '\310';
        byte byte0 = 20;
        drawRect(k - 1, l - 1, k + c + 1, l + byte0 + 1, 0xffa0a0a0);
        drawRect(k, l, k + c, l + byte0, 0xff000000);
        drawString(field_6451_g, (new StringBuilder()).append(serverAddress).append((parentScreen_03 / 6) % 2 != 0 ? "" : "_").toString(), k + 4, l + (byte0 - 8) / 2, 0xe0e0e0);
        super.drawScreen(i, j, f);
    }

    private GuiScreen updateCounter_01;
    private int parentScreen_03;
    private String serverAddress;
}
