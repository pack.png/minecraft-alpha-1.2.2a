package net.minecraft.src.world.chunk.provider;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.IChunkLoader;
import net.minecraft.src.IChunkProvider;
import net.minecraft.src.IProgressUpdate;
import net.minecraft.src.world.World;
import net.minecraft.src.world.chunk.Chunk;

import java.io.IOException;

public class ChunkProviderIso
    implements IChunkProvider
{

    public ChunkProviderIso(World world, IChunkLoader ichunkloader)
    {
        chunks_00 = new Chunk[256];
        field_899_a = new byte[32768];
        worldObj_05 = world;
        field_900_d = ichunkloader;
    }

    public boolean chunkExists(int i, int j)
    {
        int k = i & 0xf | (j & 0xf) * 16;
        return chunks_00[k] != null && chunks_00[k].func_1017_a(i, j);
    }

    public Chunk func_533_b(int i, int j)
    {
        int k = i & 0xf | (j & 0xf) * 16;
        try
        {
            if(!chunkExists(i, j))
            {
                Chunk chunk = func_543_c(i, j);
                if(chunk == null)
                {
                    chunk = new Chunk(worldObj_05, field_899_a, i, j);
                    chunk.field_1524_q = true;
                    chunk.field_1525_p = true;
                }
                chunks_00[k] = chunk;
            }
            return chunks_00[k];
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        return null;
    }

    private synchronized Chunk func_543_c(int i, int j)
    {
        try
        {
            return field_900_d.func_813_a(worldObj_05, i, j);
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        return null;
    }

    public void populate(IChunkProvider ichunkprovider, int i, int j)
    {
    }

    public boolean func_535_a(boolean flag, IProgressUpdate iprogressupdate)
    {
        return true;
    }

    public boolean func_532_a()
    {
        return false;
    }

    public boolean func_536_b()
    {
        return false;
    }

    private Chunk chunks_00[];
    private World worldObj_05;
    private IChunkLoader field_900_d;
    byte field_899_a[];
}
