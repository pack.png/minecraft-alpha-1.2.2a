package net.minecraft.src.world.chunk.provider;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.*;
import net.minecraft.src.Block;
import net.minecraft.src.BlockSand;
import net.minecraft.src.world.World;
import net.minecraft.src.world.chunk.Chunk;

import java.util.Random;

public class ChunkProviderGenerate
    implements IChunkProvider
{

    public ChunkProviderGenerate(World world, long l)
    {
        field_905_r = new double[256];
        field_904_s = new double[256];
        field_903_t = new double[256];
        field_902_u = new MapGenCaves();
        field_914_i = new int[32][32];
        worldObj_06 = world;
        rand_02 = new Random(l);
        field_912_k = new NoiseGeneratorOctaves(rand_02, 16);
        field_911_l = new NoiseGeneratorOctaves(rand_02, 16);
        field_910_m = new NoiseGeneratorOctaves(rand_02, 8);
        field_909_n = new NoiseGeneratorOctaves(rand_02, 4);
        field_908_o = new NoiseGeneratorOctaves(rand_02, 4);
        field_922_a = new NoiseGeneratorOctaves(rand_02, 10);
        field_921_b = new NoiseGeneratorOctaves(rand_02, 16);
        field_920_c = new NoiseGeneratorOctaves(rand_02, 8);
    }

    public void func_4060_a(int i, int j, byte abyte0[], MobSpawnerBase amobspawnerbase[], double ad[])
    {
        byte byte0 = 4;
        byte byte1 = 64;
        int k = byte0 + 1;
        byte byte2 = 17;
        int l = byte0 + 1;
        field_4180_q = func_4061_a(field_4180_q, i * byte0, 0, j * byte0, k, byte2, l);
        for(int i1 = 0; i1 < byte0; i1++)
        {
            for(int j1 = 0; j1 < byte0; j1++)
            {
                for(int k1 = 0; k1 < 16; k1++)
                {
                    double d = 0.125D;
                    double d1 = field_4180_q[((i1 + 0) * l + (j1 + 0)) * byte2 + (k1 + 0)];
                    double d2 = field_4180_q[((i1 + 0) * l + (j1 + 1)) * byte2 + (k1 + 0)];
                    double d3 = field_4180_q[((i1 + 1) * l + (j1 + 0)) * byte2 + (k1 + 0)];
                    double d4 = field_4180_q[((i1 + 1) * l + (j1 + 1)) * byte2 + (k1 + 0)];
                    double d5 = (field_4180_q[((i1 + 0) * l + (j1 + 0)) * byte2 + (k1 + 1)] - d1) * d;
                    double d6 = (field_4180_q[((i1 + 0) * l + (j1 + 1)) * byte2 + (k1 + 1)] - d2) * d;
                    double d7 = (field_4180_q[((i1 + 1) * l + (j1 + 0)) * byte2 + (k1 + 1)] - d3) * d;
                    double d8 = (field_4180_q[((i1 + 1) * l + (j1 + 1)) * byte2 + (k1 + 1)] - d4) * d;
                    for(int l1 = 0; l1 < 8; l1++)
                    {
                        double d9 = 0.25D;
                        double d10 = d1;
                        double d11 = d2;
                        double d12 = (d3 - d1) * d9;
                        double d13 = (d4 - d2) * d9;
                        for(int i2 = 0; i2 < 4; i2++)
                        {
                            int j2 = i2 + i1 * 4 << 11 | 0 + j1 * 4 << 7 | k1 * 8 + l1;
                            char c = '\200';
                            double d14 = 0.25D;
                            double d15 = d10;
                            double d16 = (d11 - d10) * d14;
                            for(int k2 = 0; k2 < 4; k2++)
                            {
                                double d17 = ad[(i1 * 4 + i2) * 16 + (j1 * 4 + k2)];
                                int l2 = 0;
                                if(k1 * 8 + l1 < byte1)
                                {
                                    if(d17 < 0.5D && k1 * 8 + l1 >= byte1 - 1)
                                    {
                                        l2 = Block.ice.blockID_00;
                                    } else
                                    {
                                        l2 = Block.waterMoving.blockID_00;
                                    }
                                }
                                if(d15 > 0.0D)
                                {
                                    l2 = Block.stone.blockID_00;
                                }
                                abyte0[j2] = (byte)l2;
                                j2 += c;
                                d15 += d16;
                            }

                            d10 += d12;
                            d11 += d13;
                        }

                        d1 += d5;
                        d2 += d6;
                        d3 += d7;
                        d4 += d8;
                    }

                }

            }

        }

    }

    public void func_4062_a(int i, int j, byte abyte0[], MobSpawnerBase amobspawnerbase[])
    {
        byte byte0 = 64;
        double d = 0.03125D;
        field_905_r = field_909_n.func_807_a(field_905_r, i * 16, j * 16, 0.0D, 16, 16, 1, d, d, 1.0D);
        field_904_s = field_909_n.func_807_a(field_904_s, j * 16, 109.0134D, i * 16, 16, 1, 16, d, 1.0D, d);
        field_903_t = field_908_o.func_807_a(field_903_t, i * 16, j * 16, 0.0D, 16, 16, 1, d * 2D, d * 2D, d * 2D);
        for(int k = 0; k < 16; k++)
        {
            for(int l = 0; l < 16; l++)
            {
                MobSpawnerBase mobspawnerbase = amobspawnerbase[k * 16 + l];
                boolean flag = field_905_r[k + l * 16] + rand_02.nextDouble() * 0.20000000000000001D > 0.0D;
                boolean flag1 = field_904_s[k + l * 16] + rand_02.nextDouble() * 0.20000000000000001D > 3D;
                int i1 = (int)(field_903_t[k + l * 16] / 3D + 3D + rand_02.nextDouble() * 0.25D);
                int j1 = -1;
                byte byte1 = mobspawnerbase.field_4242_o;
                byte byte2 = mobspawnerbase.field_4241_p;
                for(int k1 = 127; k1 >= 0; k1--)
                {
                    int l1 = (k * 16 + l) * 128 + k1;
                    if(k1 <= 0 + rand_02.nextInt(5))
                    {
                        abyte0[l1] = (byte)Block.bedrock.blockID_00;
                        continue;
                    }
                    byte byte3 = abyte0[l1];
                    if(byte3 == 0)
                    {
                        j1 = -1;
                        continue;
                    }
                    if(byte3 != Block.stone.blockID_00)
                    {
                        continue;
                    }
                    if(j1 == -1)
                    {
                        if(i1 <= 0)
                        {
                            byte1 = 0;
                            byte2 = (byte)Block.stone.blockID_00;
                        } else
                        if(k1 >= byte0 - 4 && k1 <= byte0 + 1)
                        {
                            byte1 = mobspawnerbase.field_4242_o;
                            byte2 = mobspawnerbase.field_4241_p;
                            if(flag1)
                            {
                                byte1 = 0;
                            }
                            if(flag1)
                            {
                                byte2 = (byte)Block.gravel.blockID_00;
                            }
                            if(flag)
                            {
                                byte1 = (byte)Block.sand_00.blockID_00;
                            }
                            if(flag)
                            {
                                byte2 = (byte)Block.sand_00.blockID_00;
                            }
                        }
                        if(k1 < byte0 && byte1 == 0)
                        {
                            byte1 = (byte)Block.waterMoving.blockID_00;
                        }
                        j1 = i1;
                        if(k1 >= byte0 - 1)
                        {
                            abyte0[l1] = byte1;
                        } else
                        {
                            abyte0[l1] = byte2;
                        }
                        continue;
                    }
                    if(j1 > 0)
                    {
                        j1--;
                        abyte0[l1] = byte2;
                    }
                }

            }

        }

    }

    public Chunk func_533_b(int i, int j)
    {
        rand_02.setSeed((long)i * 0x4f9939f508L + (long)j * 0x1ef1565bd5L);
        byte abyte0[] = new byte[32768];
        Chunk chunk = new Chunk(worldObj_06, abyte0, i, j);
        field_4179_v = worldObj_06.func_4075_a().func_4070_a(field_4179_v, i * 16, j * 16, 16, 16);
        double ad[] = worldObj_06.func_4075_a().field_4198_a;
        func_4060_a(i, j, abyte0, field_4179_v, ad);
        func_4062_a(i, j, abyte0, field_4179_v);
        field_902_u.func_867_a(this, worldObj_06, i, j, abyte0);
        chunk.func_1024_c();
        return chunk;
    }

    private double[] func_4061_a(double ad[], int i, int j, int k, int l, int i1, int j1)
    {
        if(ad == null)
        {
            ad = new double[l * i1 * j1];
        }
        double d = 684.41200000000003D;
        double d1 = 684.41200000000003D;
        double ad1[] = worldObj_06.func_4075_a().field_4198_a;
        double ad2[] = worldObj_06.func_4075_a().field_4197_b;
        field_4182_g = field_922_a.func_4109_a(field_4182_g, i, k, l, j1, 1.121D, 1.121D, 0.5D);
        field_4181_h = field_921_b.func_4109_a(field_4181_h, i, k, l, j1, 200D, 200D, 0.5D);
        field_4185_d = field_910_m.func_807_a(field_4185_d, i, j, k, l, i1, j1, d / 80D, d1 / 160D, d / 80D);
        field_4184_e = field_912_k.func_807_a(field_4184_e, i, j, k, l, i1, j1, d, d1, d);
        field_4183_f = field_911_l.func_807_a(field_4183_f, i, j, k, l, i1, j1, d, d1, d);
        int k1 = 0;
        int l1 = 0;
        int i2 = 16 / l;
        for(int j2 = 0; j2 < l; j2++)
        {
            int k2 = j2 * i2 + i2 / 2;
            for(int l2 = 0; l2 < j1; l2++)
            {
                int i3 = l2 * i2 + i2 / 2;
                double d2 = ad1[k2 * 16 + i3];
                double d3 = ad2[k2 * 16 + i3] * d2;
                double d4 = 1.0D - d3;
                d4 *= d4;
                d4 *= d4;
                d4 = 1.0D - d4;
                double d5 = (field_4182_g[l1] + 256D) / 512D;
                d5 *= d4;
                if(d5 > 1.0D)
                {
                    d5 = 1.0D;
                }
                double d6 = field_4181_h[l1] / 8000D;
                if(d6 < 0.0D)
                {
                    d6 = -d6 * 0.29999999999999999D;
                }
                d6 = d6 * 3D - 2D;
                if(d6 < 0.0D)
                {
                    d6 /= 2D;
                    if(d6 < -1D)
                    {
                        d6 = -1D;
                    }
                    d6 /= 1.3999999999999999D;
                    d6 /= 2D;
                    d5 = 0.0D;
                } else
                {
                    if(d6 > 1.0D)
                    {
                        d6 = 1.0D;
                    }
                    d6 /= 8D;
                }
                if(d5 < 0.0D)
                {
                    d5 = 0.0D;
                }
                d5 += 0.5D;
                d6 = (d6 * (double)i1) / 16D;
                double d7 = (double)i1 / 2D + d6 * 4D;
                l1++;
                for(int j3 = 0; j3 < i1; j3++)
                {
                    double d8 = 0.0D;
                    double d9 = (((double)j3 - d7) * 12D) / d5;
                    if(d9 < 0.0D)
                    {
                        d9 *= 4D;
                    }
                    double d10 = field_4184_e[k1] / 512D;
                    double d11 = field_4183_f[k1] / 512D;
                    double d12 = (field_4185_d[k1] / 10D + 1.0D) / 2D;
                    if(d12 < 0.0D)
                    {
                        d8 = d10;
                    } else
                    if(d12 > 1.0D)
                    {
                        d8 = d11;
                    } else
                    {
                        d8 = d10 + (d11 - d10) * d12;
                    }
                    d8 -= d9;
                    if(j3 > i1 - 4)
                    {
                        double d13 = (float)(j3 - (i1 - 4)) / 3F;
                        d8 = d8 * (1.0D - d13) + -10D * d13;
                    }
                    ad[k1] = d8;
                    k1++;
                }

            }

        }

        return ad;
    }

    public boolean chunkExists(int i, int j)
    {
        return true;
    }

    public void populate(IChunkProvider ichunkprovider, int i, int j)
    {
        BlockSand.fallInstantly = true;
        int k = i * 16;
        int l = j * 16;
        MobSpawnerBase mobspawnerbase = worldObj_06.func_4075_a().func_4073_a(k + 16, l + 16);
        rand_02.setSeed(worldObj_06.randomSeed);
        long l1 = (rand_02.nextLong() / 2L) * 2L + 1L;
        long l2 = (rand_02.nextLong() / 2L) * 2L + 1L;
        rand_02.setSeed((long)i * l1 + (long)j * l2 ^ worldObj_06.randomSeed);
        double d = 0.25D;
        for(int i1 = 0; i1 < 8; i1++)
        {
            int i4 = k + rand_02.nextInt(16) + 8;
            int k6 = rand_02.nextInt(128);
            int l8 = l + rand_02.nextInt(16) + 8;
            (new WorldGenDungeons()).generate(worldObj_06, rand_02, i4, k6, l8);
        }

        for(int j1 = 0; j1 < 10; j1++)
        {
            int j4 = k + rand_02.nextInt(16);
            int l6 = rand_02.nextInt(128);
            int i9 = l + rand_02.nextInt(16);
            (new WorldGenClay(32)).generate(worldObj_06, rand_02, j4, l6, i9);
        }

        for(int k1 = 0; k1 < 20; k1++)
        {
            int k4 = k + rand_02.nextInt(16);
            int i7 = rand_02.nextInt(128);
            int j9 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.dirt.blockID_00, 32)).generate(worldObj_06, rand_02, k4, i7, j9);
        }

        for(int i2 = 0; i2 < 10; i2++)
        {
            int l4 = k + rand_02.nextInt(16);
            int j7 = rand_02.nextInt(128);
            int k9 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.gravel.blockID_00, 32)).generate(worldObj_06, rand_02, l4, j7, k9);
        }

        for(int j2 = 0; j2 < 20; j2++)
        {
            int i5 = k + rand_02.nextInt(16);
            int k7 = rand_02.nextInt(128);
            int l9 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.oreCoal.blockID_00, 16)).generate(worldObj_06, rand_02, i5, k7, l9);
        }

        for(int k2 = 0; k2 < 20; k2++)
        {
            int j5 = k + rand_02.nextInt(16);
            int l7 = rand_02.nextInt(64);
            int i10 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.oreIron.blockID_00, 8)).generate(worldObj_06, rand_02, j5, l7, i10);
        }

        for(int i3 = 0; i3 < 2; i3++)
        {
            int k5 = k + rand_02.nextInt(16);
            int i8 = rand_02.nextInt(32);
            int j10 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.oreGold.blockID_00, 8)).generate(worldObj_06, rand_02, k5, i8, j10);
        }

        for(int j3 = 0; j3 < 8; j3++)
        {
            int l5 = k + rand_02.nextInt(16);
            int j8 = rand_02.nextInt(16);
            int k10 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.oreRedstone.blockID_00, 7)).generate(worldObj_06, rand_02, l5, j8, k10);
        }

        for(int k3 = 0; k3 < 1; k3++)
        {
            int i6 = k + rand_02.nextInt(16);
            int k8 = rand_02.nextInt(16);
            int l10 = l + rand_02.nextInt(16);
            (new WorldGenMinable(Block.oreDiamond.blockID_00, 7)).generate(worldObj_06, rand_02, i6, k8, l10);
        }

        d = 0.5D;
        int l3 = (int)((field_920_c.func_806_a((double)k * d, (double)l * d) / 8D + rand_02.nextDouble() * 4D + 4D) / 3D);
        int j6 = 0;
        if(rand_02.nextInt(10) == 0)
        {
            j6++;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4253_d)
        {
            j6 += l3 + 5;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4256_a)
        {
            j6 += l3 + 5;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4254_c)
        {
            j6 += l3 + 2;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4250_g)
        {
            j6 += l3 + 5;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4249_h)
        {
            j6 -= 20;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4246_k)
        {
            j6 -= 20;
        }
        if(mobspawnerbase == MobSpawnerBase.field_4248_i)
        {
            j6 -= 20;
        }
        Object obj = new WorldGenTrees();
        if(rand_02.nextInt(10) == 0)
        {
            obj = new WorldGenBigTree();
        }
        if(mobspawnerbase == MobSpawnerBase.field_4256_a && rand_02.nextInt(3) == 0)
        {
            obj = new WorldGenBigTree();
        }
        for(int i11 = 0; i11 < j6; i11++)
        {
            int i13 = k + rand_02.nextInt(16) + 8;
            int l15 = l + rand_02.nextInt(16) + 8;
            ((WorldGenerator) (obj)).func_517_a(1.0D, 1.0D, 1.0D);
            ((WorldGenerator) (obj)).generate(worldObj_06, rand_02, i13, worldObj_06.getHeightValue_00(i13, l15), l15);
        }

        for(int j11 = 0; j11 < 2; j11++)
        {
            int j13 = k + rand_02.nextInt(16) + 8;
            int i16 = rand_02.nextInt(128);
            int k18 = l + rand_02.nextInt(16) + 8;
            (new WorldGenFlowers(Block.plantYellow.blockID_00)).generate(worldObj_06, rand_02, j13, i16, k18);
        }

        if(rand_02.nextInt(2) == 0)
        {
            int k11 = k + rand_02.nextInt(16) + 8;
            int k13 = rand_02.nextInt(128);
            int j16 = l + rand_02.nextInt(16) + 8;
            (new WorldGenFlowers(Block.plantRed.blockID_00)).generate(worldObj_06, rand_02, k11, k13, j16);
        }
        if(rand_02.nextInt(4) == 0)
        {
            int l11 = k + rand_02.nextInt(16) + 8;
            int l13 = rand_02.nextInt(128);
            int k16 = l + rand_02.nextInt(16) + 8;
            (new WorldGenFlowers(Block.mushroomBrown.blockID_00)).generate(worldObj_06, rand_02, l11, l13, k16);
        }
        if(rand_02.nextInt(8) == 0)
        {
            int i12 = k + rand_02.nextInt(16) + 8;
            int i14 = rand_02.nextInt(128);
            int l16 = l + rand_02.nextInt(16) + 8;
            (new WorldGenFlowers(Block.mushroomRed.blockID_00)).generate(worldObj_06, rand_02, i12, i14, l16);
        }
        for(int j12 = 0; j12 < 10; j12++)
        {
            int j14 = k + rand_02.nextInt(16) + 8;
            int i17 = rand_02.nextInt(128);
            int l18 = l + rand_02.nextInt(16) + 8;
            (new WorldGenReed()).generate(worldObj_06, rand_02, j14, i17, l18);
        }

        if(rand_02.nextInt(32) == 0)
        {
            int k12 = k + rand_02.nextInt(16) + 8;
            int k14 = rand_02.nextInt(128);
            int j17 = l + rand_02.nextInt(16) + 8;
            (new WorldGenPumpkin()).generate(worldObj_06, rand_02, k12, k14, j17);
        }
        int l12 = 0;
        if(mobspawnerbase == MobSpawnerBase.field_4249_h)
        {
            l12 += 10;
        }
        for(int l14 = 0; l14 < l12; l14++)
        {
            int k17 = k + rand_02.nextInt(16) + 8;
            int i19 = rand_02.nextInt(128);
            int i20 = l + rand_02.nextInt(16) + 8;
            (new WorldGenCactus()).generate(worldObj_06, rand_02, k17, i19, i20);
        }

        for(int i15 = 0; i15 < 50; i15++)
        {
            int l17 = k + rand_02.nextInt(16) + 8;
            int j19 = rand_02.nextInt(rand_02.nextInt(120) + 8);
            int j20 = l + rand_02.nextInt(16) + 8;
            (new WorldGenLiquids(Block.waterStill.blockID_00)).generate(worldObj_06, rand_02, l17, j19, j20);
        }

        for(int j15 = 0; j15 < 20; j15++)
        {
            int i18 = k + rand_02.nextInt(16) + 8;
            int k19 = rand_02.nextInt(rand_02.nextInt(rand_02.nextInt(112) + 8) + 8);
            int k20 = l + rand_02.nextInt(16) + 8;
            (new WorldGenLiquids(Block.lavaStill.blockID_00)).generate(worldObj_06, rand_02, i18, k19, k20);
        }

        field_4178_w = worldObj_06.func_4075_a().func_4071_a(field_4178_w, k + 8, l + 8, 16, 16);
        for(int k15 = k + 8; k15 < k + 8 + 16; k15++)
        {
            for(int j18 = l + 8; j18 < l + 8 + 16; j18++)
            {
                int l19 = k15 - (k + 8);
                int l20 = j18 - (l + 8);
                int i21 = worldObj_06.func_4083_e(k15, j18);
                double d1 = field_4178_w[l19 * 16 + l20] - ((double)(i21 - 64) / 64D) * 0.29999999999999999D;
                if(d1 < 0.5D && i21 > 0 && i21 < 128 && worldObj_06.getBlockId(k15, i21, j18) == 0 && worldObj_06.getBlockMaterial(k15, i21 - 1, j18).func_880_c() && worldObj_06.getBlockMaterial(k15, i21 - 1, j18) != Material.field_1320_r)
                {
                    worldObj_06.setBlockWithNotify(k15, i21, j18, Block.snow_00.blockID_00);
                }
            }

        }

        BlockSand.fallInstantly = false;
    }

    public boolean func_535_a(boolean flag, IProgressUpdate iprogressupdate)
    {
        return true;
    }

    public boolean func_532_a()
    {
        return false;
    }

    public boolean func_536_b()
    {
        return true;
    }

    private Random rand_02;
    private NoiseGeneratorOctaves field_912_k;
    private NoiseGeneratorOctaves field_911_l;
    private NoiseGeneratorOctaves field_910_m;
    private NoiseGeneratorOctaves field_909_n;
    private NoiseGeneratorOctaves field_908_o;
    public NoiseGeneratorOctaves field_922_a;
    public NoiseGeneratorOctaves field_921_b;
    public NoiseGeneratorOctaves field_920_c;
    private World worldObj_06;
    private double field_4180_q[];
    private double field_905_r[];
    private double field_904_s[];
    private double field_903_t[];
    private MapGenBase field_902_u;
    private MobSpawnerBase field_4179_v[];
    double field_4185_d[];
    double field_4184_e[];
    double field_4183_f[];
    double field_4182_g[];
    double field_4181_h[];
    int field_914_i[][];
    private double field_4178_w[];
}
