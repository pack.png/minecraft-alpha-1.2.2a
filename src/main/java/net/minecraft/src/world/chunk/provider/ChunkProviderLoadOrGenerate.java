package net.minecraft.src.world.chunk.provider;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import net.minecraft.src.IChunkLoader;
import net.minecraft.src.IChunkProvider;
import net.minecraft.src.IProgressUpdate;
import net.minecraft.src.world.World;
import net.minecraft.src.world.chunk.Chunk;

import java.io.IOException;

public class ChunkProviderLoadOrGenerate
    implements IChunkProvider
{

    public ChunkProviderLoadOrGenerate(World world, IChunkLoader ichunkloader, IChunkProvider ichunkprovider)
    {
        chunks = new Chunk[1024];
        field_891_a = 0xc4653601;
        field_890_b = 0xc4653601;
        field_897_c = new Chunk(world, new byte[32768], 0, 0);
        field_897_c.field_1524_q = true;
        field_897_c.field_1525_p = true;
        worldObj_12 = world;
        field_895_e = ichunkloader;
        field_896_d = ichunkprovider;
    }

    public boolean chunkExists(int i, int j)
    {
        if(i == field_891_a && j == field_890_b && field_892_h != null)
        {
            return true;
        } else
        {
            int k = i & 0x1f;
            int l = j & 0x1f;
            int i1 = k + l * 32;
            return chunks[i1] != null && (chunks[i1] == field_897_c || chunks[i1].func_1017_a(i, j));
        }
    }

    public Chunk func_533_b(int i, int j)
    {
        if(i == field_891_a && j == field_890_b && field_892_h != null)
        {
            return field_892_h;
        }
        int k = i & 0x1f;
        int l = j & 0x1f;
        int i1 = k + l * 32;
        if(!chunkExists(i, j))
        {
            if(chunks[i1] != null)
            {
                chunks[i1].func_998_e();
                func_540_b(chunks[i1]);
                func_541_a(chunks[i1]);
            }
            Chunk chunk = func_542_c(i, j);
            if(chunk == null)
            {
                if(field_896_d == null)
                {
                    chunk = field_897_c;
                } else
                {
                    chunk = field_896_d.func_533_b(i, j);
                }
            }
            chunks[i1] = chunk;
            chunk.func_4143_d();
            if(chunks[i1] != null)
            {
                chunks[i1].func_995_d();
            }
            if(!chunks[i1].isTerrainPopulated && chunkExists(i + 1, j + 1) && chunkExists(i, j + 1) && chunkExists(i + 1, j))
            {
                populate(this, i, j);
            }
            if(chunkExists(i - 1, j) && !func_533_b(i - 1, j).isTerrainPopulated && chunkExists(i - 1, j + 1) && chunkExists(i, j + 1) && chunkExists(i - 1, j))
            {
                populate(this, i - 1, j);
            }
            if(chunkExists(i, j - 1) && !func_533_b(i, j - 1).isTerrainPopulated && chunkExists(i + 1, j - 1) && chunkExists(i, j - 1) && chunkExists(i + 1, j))
            {
                populate(this, i, j - 1);
            }
            if(chunkExists(i - 1, j - 1) && !func_533_b(i - 1, j - 1).isTerrainPopulated && chunkExists(i - 1, j - 1) && chunkExists(i, j - 1) && chunkExists(i - 1, j))
            {
                populate(this, i - 1, j - 1);
            }
        }
        field_891_a = i;
        field_890_b = j;
        field_892_h = chunks[i1];
        return chunks[i1];
    }

    private Chunk func_542_c(int i, int j)
    {
        if(field_895_e == null)
        {
            return null;
        }
        try
        {
            Chunk chunk = field_895_e.func_813_a(worldObj_12, i, j);
            if(chunk != null)
            {
                chunk.field_1522_s = worldObj_12.worldTime;
            }
            return chunk;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        return null;
    }

    private void func_541_a(Chunk chunk)
    {
        if(field_895_e == null)
        {
            return;
        }
        try
        {
            field_895_e.func_815_b(worldObj_12, chunk);
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void func_540_b(Chunk chunk)
    {
        if(field_895_e == null)
        {
            return;
        }
        try
        {
            chunk.field_1522_s = worldObj_12.worldTime;
            field_895_e.func_812_a(worldObj_12, chunk);
        }
        catch(IOException ioexception)
        {
            ioexception.printStackTrace();
        }
    }

    public void populate(IChunkProvider ichunkprovider, int i, int j)
    {
        Chunk chunk = func_533_b(i, j);
        if(!chunk.isTerrainPopulated)
        {
            chunk.isTerrainPopulated = true;
            if(field_896_d != null)
            {
                field_896_d.populate(ichunkprovider, i, j);
                chunk.func_1006_f();
            }
        }
    }

    public boolean func_535_a(boolean flag, IProgressUpdate iprogressupdate)
    {
        int i = 0;
        int j = 0;
        if(iprogressupdate != null)
        {
            for(int k = 0; k < chunks.length; k++)
            {
                if(chunks[k] != null && chunks[k].func_1012_a(flag))
                {
                    j++;
                }
            }

        }
        int l = 0;
        for(int i1 = 0; i1 < chunks.length; i1++)
        {
            if(chunks[i1] == null)
            {
                continue;
            }
            if(flag && !chunks[i1].field_1525_p)
            {
                func_541_a(chunks[i1]);
            }
            if(!chunks[i1].func_1012_a(flag))
            {
                continue;
            }
            func_540_b(chunks[i1]);
            chunks[i1].isModified = false;
            if(++i == 2 && !flag)
            {
                return false;
            }
            if(iprogressupdate != null && ++l % 10 == 0)
            {
                iprogressupdate.func_593_a((l * 100) / j);
            }
        }

        if(flag)
        {
            if(field_895_e == null)
            {
                return true;
            }
            field_895_e.func_811_b();
        }
        return true;
    }

    public boolean func_532_a()
    {
        if(field_895_e != null)
        {
            field_895_e.func_814_a();
        }
        return field_896_d.func_532_a();
    }

    public boolean func_536_b()
    {
        return true;
    }

    private Chunk field_897_c;
    private IChunkProvider field_896_d;
    private IChunkLoader field_895_e;
    private Chunk chunks[];
    private World worldObj_12;
    int field_891_a;
    int field_890_b;
    private Chunk field_892_h;
}
