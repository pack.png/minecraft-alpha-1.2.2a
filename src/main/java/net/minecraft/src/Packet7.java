package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import java.io.*;

public class Packet7 extends Packet
{

    public Packet7()
    {
    }

    public Packet7(int i, int j)
    {
        field_6371_a = i;
        field_6370_b = j;
    }

    public void readPacketData(DataInputStream datainputstream) throws IOException
    {
        field_6371_a = datainputstream.readInt();
        field_6370_b = datainputstream.readInt();
    }

    public void writePacketData(DataOutputStream dataoutputstream) throws IOException
    {
        dataoutputstream.writeInt(field_6371_a);
        dataoutputstream.writeInt(field_6370_b);
    }

    public void processPacket(NetHandler nethandler)
    {
        nethandler.func_6499_a(this);
    }

    public int getPacketSize()
    {
        return 8;
    }

    public int field_6371_a;
    public int field_6370_b;
}
