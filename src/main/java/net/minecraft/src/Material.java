package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 


public class Material
{

    public Material()
    {
    }

    public boolean getIsLiquid()
    {
        return false;
    }

    public boolean func_878_a()
    {
        return true;
    }

    public boolean getCanBlockGrass()
    {
        return true;
    }

    public boolean func_880_c()
    {
        return true;
    }

    private Material func_4130_f()
    {
        field_4259_y = true;
        return this;
    }

    public boolean getBurning()
    {
        return field_4259_y;
    }

    public static final Material air = new MaterialTransparent();
    public static final Material ground = new Material();
    public static final Material wood = (new Material()).func_4130_f();
    public static final Material rock = new Material();
    public static final Material iron = new Material();
    public static final Material water = new MaterialLiquid();
    public static final Material lava = new MaterialLiquid();
    public static final Material field_4265_h = (new Material()).func_4130_f();
    public static final Material plants = new MaterialLogic();
    public static final Material sponge = new Material();
    public static final Material field_4264_k = (new Material()).func_4130_f();
    public static final Material fire = new MaterialTransparent();
    public static final Material sand = new Material();
    public static final Material circuits = new MaterialLogic();
    public static final Material field_4263_o = new Material();
    public static final Material tnt = (new Material()).func_4130_f();
    public static final Material field_4262_q = new Material();
    public static final Material field_1320_r = new Material();
    public static final Material snow = new MaterialLogic();
    public static final Material builtSnow = new Material();
    public static final Material cactus = new Material();
    public static final Material clay_00 = new Material();
    public static final Material field_4261_w = new Material();
    public static final Material field_4260_x = new Material();
    private boolean field_4259_y;

}
