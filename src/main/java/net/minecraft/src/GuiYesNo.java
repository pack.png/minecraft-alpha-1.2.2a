package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

public class GuiYesNo extends GuiScreen
{

    public GuiYesNo(GuiScreen guiscreen, String s, String s1, int i)
    {
        parentScreen_02 = guiscreen;
        message1_00 = s;
        message2_00 = s1;
        worldNumber = i;
    }

    public void initGui()
    {
        controlList.add(new GuiSmallButton(0, (width_02 / 2 - 155) + 0, height_02 / 6 + 96, "Yes"));
        controlList.add(new GuiSmallButton(1, (width_02 / 2 - 155) + 160, height_02 / 6 + 96, "No"));
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        parentScreen_02.deleteWorld_00(guibutton.id_02 == 0, worldNumber);
    }

    public void drawScreen(int i, int j, float f)
    {
        drawBackground();
        drawCenteredString(field_6451_g, message1_00, width_02 / 2, 70, 0xffffff);
        drawCenteredString(field_6451_g, message2_00, width_02 / 2, 90, 0xffffff);
        super.drawScreen(i, j, f);
    }

    private GuiScreen parentScreen_02;
    private String message1_00;
    private String message2_00;
    private int worldNumber;
}
