package net.minecraft.src;
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) braces deadcode 

import org.lwjgl.opengl.GL11;

public class GuiGameOver extends GuiScreen
{

    public GuiGameOver()
    {
    }

    public void initGui()
    {
        controlList.clear();
        controlList.add(new GuiButton(1, width_02 / 2 - 100, height_02 / 4 + 72, "Respawn"));
        controlList.add(new GuiButton(2, width_02 / 2 - 100, height_02 / 4 + 96, "Title menu"));
        if(mc_06.field_6320_i == null)
        {
            ((GuiButton)controlList.get(1)).enabled = false;
        }
    }

    protected void keyTyped(char c, int i)
    {
    }

    protected void actionPerformed(GuiButton guibutton)
    {
        if(guibutton.id_02 != 0);
        if(guibutton.id_02 == 1)
        {
            mc_06.func_6239_p();
            mc_06.displayGuiScreen(null);
        }
        if(guibutton.id_02 == 2)
        {
            mc_06.setWorld(null);
            mc_06.displayGuiScreen(new GuiMainMenu());
        }
    }

    public void drawScreen(int i, int j, float f)
    {
        drawGradientRect(0, 0, width_02, height_02, 0x60500000, 0xa0803030);
        GL11.glPushMatrix();
        GL11.glScalef(2.0F, 2.0F, 2.0F);
        drawCenteredString(field_6451_g, "Game over!", width_02 / 2 / 2, 30, 0xffffff);
        GL11.glPopMatrix();
        drawCenteredString(field_6451_g, (new StringBuilder()).append("Score: &e").append(mc_06.entityPlayerSP.func_6417_t()).toString(), width_02 / 2, 100, 0xffffff);
        super.drawScreen(i, j, f);
    }

    public boolean func_6450_b()
    {
        return false;
    }
}
