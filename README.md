# Minecraft Alpha 1.2.2a - Source Code
This repo contains the decompiled source code for Minecraft Alpha 1.2.2a. 
This is the first version of the game that the pack.png file exists in.  

## Running  
**Maven compilation does not work yet**  

1. Clone the repo
2. Open IntelliJ and add the `libs/libraries` and `libs/native` as libraries in the Project Structure window, as shown below.  
  
![image](https://i.imgur.com/PiSe8tf.png)  

3. Create a run configuration for an `Application` and set the main class to `net.minecraft.client.Minecraft`  
  
![image](https://i.imgur.com/sP1Ejff.png)  


### Compiling
Back in the project stucture window, click on `Artiacts` in 
the left hand menu, and then click the plus sign and select
`JAR > From modules with dependencies`  
  
Finally, select the main class `net.minecraft.client.Minecraft`
and then you can build by clicking `Build > Build Artifacts` in
the toolbar of the main IDE window.  
  
## Custom Features
* Coordinates displayed in debug screen  
* Added a world create / edit screen which also allows editing the world seed  
* Added `GuiTextField` class to allow easily text field creation
  
## TODO
* Rename all functions and variables to make sense  
* Add more useful things for finding pack.png, or just messing around  
* **GET MAVEN WORKING**  
  
